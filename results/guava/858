Maps created via LoadingCache.asMap() always return null upon calling get(key) _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=858) created by **robert.barbey** on 2012-01-10 at 09:26 AM_ --- We upgraded guava in our project to version 11 and refactored the code make use of MapMaker to CacheBuilder. In one instance, we used MapMaker.makeComputingMap(). We followed the advice given in the Javadoc to use CacheBuilder.build(CacheLoader).asMap(). Unfortunately. the map does not load values upon calling get. Here's a small unit test that illustrates the behaviour. . Maps created with MapMaker.makeComputingMap(Function) were returning results correctly.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=858#c1) posted by **stephan202** on 2012-01-10 at 10:30 AM_

---

This is not a bug per se, since this behavior is documented by Cache#asMap. It cached."
.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=858#c2) posted by **robert.barbey** on 2012-01-10 at 10:38 AM_

---

I see. But in that case there is no replacement for MapMaker.makeComputingMap() currently and this should be reflected in the documentation.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=858#c3) posted by **cgdecker** on 2012-01-10 at 02:17 PM_

---

There is a replacement for MapMaker.makeComputingMap(): a LoadingCache. There just isn't one that implements the Map interface. Of course, you could write your own Map wrapper around a LoadingCache if you wanted, but the idea is that you use the cache directly.

---

**Status:** `WorkingAsIntended`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=858#c4) posted by **wasserman.louis** on 2012-01-10 at 03:43 PM_

---

I will note that the behavior you're looking for is an abuse of the Map interface, which is why we made Cache its own interface. Normal Maps don't load their own values on demand.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=858#c5) posted by **robert.barbey** on 2012-01-11 at 12:38 PM_

---

Thanks everyone for clarifying this so quickly!
 
Implement #toString for the Iterable returned by Splitter#split _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=999) created by **j...@nwsnet.de** on 2012-05-10 at 02:20 PM_ --- I often find myself creation an immutable set or list from a splitted string, so getting back an instance of `FluentIterable`&nbsp;and being able to call the respective, already available methods on it would make sense.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c1) posted by **j...@nwsnet.de** on 2012-05-10 at 02:20 PM_

---

s/creation/creating/
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c2) posted by **wasserman.louis** on 2012-05-10 at 02:22 PM_

---

I'm not sure this can happen until FluentIterable leaves @﻿Beta.?

---

**Status:** `New`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c3) posted by **kurt.kluever** on 2012-05-10 at 02:35 PM_

---

This can't happen at all unfortunately due to the dependency tree (collect -> base).

We've always wanted to return ImmutableList from split(), but it's impossible :-(

---

**Status:** `WontFix`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c4) posted by **SeanPFloyd** on 2012-05-10 at 02:41 PM_

---

Fair enough, but can the returned iterable be made to implement toString() in a sensible way?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c5) posted by **wasserman.louis** on 2012-05-10 at 02:46 PM_

---

I'm sure that much can be arranged, yes.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c6) posted by **kurt.kluever** on 2012-05-10 at 02:51 PM_

---

What do you want #toString to return? The backing iterator is lazy, so it can't contain the full list of tokens.

---

**Status:** `New`
**Labels:** `Type-Enhancement`, `Package-Base`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c7) posted by **wasserman.louis** on 2012-05-10 at 02:52 PM_

---

I think it makes sense to go ahead and force the backing iterator, actually, and provide the full list of tokens.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c8) posted by **SeanPFloyd** on 2012-05-10 at 03:27 PM_

---

@﻿louis yes, obviously.

I'd expect the implementation to call Iterables.toString(this);
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c9) posted by **SeanPFloyd** on 2012-05-10 at 03:28 PM_

---

After all, toString() on an iterable is only likely to be used for logging and debugging purposes
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c10) posted by **j...@nwsnet.de** on 2012-05-10 at 03:34 PM_

---

Ah, too bad, I forgot about that.

But I'm surprised to see this ticket getting hijacked. ;)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c11) posted by **kurt.kluever** on 2012-05-10 at 05:26 PM_

---

Can't call Iterables#toString from c.g.c.base either ;-) We'll have to copy the implementation into Splitter. Kinda yucky :-/
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c12) posted by **wasserman.louis** on 2012-05-10 at 05:28 PM_

---

Well, let's be fair -- we can still use Joiner!
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c13) posted by **kurt.kluever** on 2012-05-10 at 05:50 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Accepted`
**Owner:** kurt.kluever
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c14) posted by **SeanPFloyd** on 2012-05-10 at 06:57 PM_

---

Yeah, that was the next thing I was going to suggest. Something along the lines of

Joiner.on(", ").appendTo(new StringBuilder().append('['), this).append(']');
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c15) posted by **kurt.kluever** on 2012-05-10 at 08:18 PM_

---

Will go out to HEAD tomorrow morning.

---

**Status:** `Fixed`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=999#c16) posted by **wasserman.louis** on 2012-06-28 at 09:14 AM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Release13`
 
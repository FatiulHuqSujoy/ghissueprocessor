Enhanced format(String format, Object... args) in Strings _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1389) created by **jamie.spencer.86** on 2013-04-29 at 04:20 AM_ --- This issue derives from https://github.com/google/guava/issues/1382 . Could Preconditions.format(String, Object...) become public as a member of the Strings helper class? That method doesn't seem particularly dependant on Preconditions and seems well suited for any set of (String, Object...) arguments. I would hope that Strings.format only allowing %s flags, would not misleading any users, but documentation handles that well enough. Other than that, I feel that it would be useful outside of Preconditions.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1389#c1) posted by **wasserman.louis** on 2013-04-29 at 04:38 AM_

---

This seems really, really weird to add a method with essentially the same API but strictly weaker capabilities than a JDK method.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1389#c2) posted by **jamie.spencer.86** on 2013-04-29 at 04:52 AM_

---

Was Preconditions.format needed for something specifically Preconditions-related, or was there something that String.format wasn't giving that Preconditions needed at that moment? Would anything else (in guava) be able to benefit from that extra bit of functionality?

And overall, it really doesn't have to be public at Strings, if it only makes sense for it to be public at Preconditions (if it makes sense at all to be public).

I wouldn't ask anyone to implement Strings.format with all of the function of String.format + Preconditions.format; if that is what it takes to have it public at Strings, then I'd rather not put anyone through the trouble. It only seemed convenient to have during the discussion in the alluded issue.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1389#c3) posted by **wasserman.louis** on 2013-04-29 at 04:59 AM_

---

The one and only advantage of Preconditions.format over Strings.format is GWT compatibility.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1389#c4) posted by **Maaartinus** on 2013-04-29 at 07:57 PM_

---

There's one more thing (good for Preconditions and bad in most other places):
https://github.com/google/guava/issues/1382
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1389#c5) posted by **j...@nwsnet.de** on 2013-05-15 at 02:44 PM_

---

What about performance? I remember `String.format`&nbsp;to be quite slow.
 It seems there is a significant demand for this, see e.g. most currently http://stackoverflow.com/questions/29747912/, but also the older, but massively popular http://stackoverflow.com/questions/513600/ and http://stackoverflow.com/questions/12786902/.

The standard answer is to use `String.format()`/`MessageFormat` if you care about readability and flexibility, then to use a `StringBuilder` if you need performance.
In the 90% of cases, this is good enough.
In the rest, under the assumption we don't need the flexibility of `String.format()`, a simple concatenation/`StringBuilder` can be hideous.

As of now (2015/05) and to my best knowledge, the current best is slf4j `MessageFormatter`, but its API could definitely be improved.
If the `Strings.format()` method were published, it would immediatelly be the nicest and clearest option out there.
 Fixed by 7fe1702a :smiley: 
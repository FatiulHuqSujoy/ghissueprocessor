Converter<A, B>, which combines a function and its sort-of-inverse _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=177) created by **ejwinter** on 2009-05-29 at 06:48 PM_ --- In several projects I have added the following interface. I think it would go very well in Google Collections: public interface InvertableFunction&lt;T,K> extends Function&lt;T,K> { &nbsp;&nbsp;&nbsp;&nbsp;InvertableFunction&lt;K,T> reverse(); } With an example implementation of: public class BasicEncryptor implements InvertableFunction&lt;String,String>{ &nbsp;&nbsp;&nbsp;&nbsp;private final BasicTextEncryptor encryptor; . }  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c1) posted by **kevinb9n** on 2009-05-29 at 06:54 PM_

---

I can see this happening eventually. It's a little problematic because so many
functions that have an inverse() function are not actually invertible in the
mathmetical sense (for example, you can convert "1.00" to a double and get 1.0, then
back to a String and get 1.0), and as a result, the kinds of things you expect to be
able to do with an invertible function (like Sets.transform(Set)) aren't really ironclad.

I think we can work out how to warn about the minor risks though.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c2) posted by **kevinb9n** on 2009-09-17 at 05:33 PM_

---

I just realized that we have actually implemented this already internally, but we 
have named it "Converter."

How would users feel if our InvertibleFunction class were named Converter instead?

Also, it's an abstract class instead of an interface so that it can have useful 
methods on it like convertAll() (aka transform()). That's inconsistent with our 
other types, but it is quite nice...

---

**Status:** `Accepted`
**Labels:** `post-1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c3) posted by **kevinb9n** on 2009-09-17 at 05:34 PM_

---

_(No comment entered for this change.)_
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c4) posted by **kevinb9n** on 2009-09-17 at 05:45 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`post-1.0`, `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c5) posted by **kevinb9n** on 2009-09-17 at 05:57 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c6) posted by **ejwinter** on 2009-09-17 at 06:16 PM_

---

Could Converter&lt;T,K> be made to implement InvertibleFunction&lt;T,K>. Then you can
implement an interface as an option. The naming would be fine.
Thanks,
Eric
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c7) posted by **stephen.kestle** on 2009-12-03 at 01:51 AM_

---

Definitely make Converter implements InvertibleFunction.

IdentityFunction is one implementation :).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c8) posted by **kevinb@google.com** on 2010-07-30 at 03:53 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c9) posted by **kevinb@google.com** on 2011-01-27 at 01:33 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Release09`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c10) posted by **jim.andreou** on 2011-03-16 at 10:46 PM_

---

Just a comment from my recent experience implementing something like a Sets#transform: the tricky part is to specify the bijection contract. I mean the exact semantics of the methods, not just the signatures.

For example:
public interface InvertibleFunction&lt;A,B> extends Function&lt;A,B> {
&nbsp;&nbsp;&nbsp;&nbsp;InvertableFunction&lt;B,A> reverse();
}

This, without any other specification, implies a bijection A <--> B, i.e. from _every_ A to _every_ B (oh, yes, A and B (infinite) sets should have the same cardinality too). This specification makes it the easiest to implement Sets#transform, but also the hardest to implement InvertibleFunction correctly. 

At least from the perspective of Sets#transform, what is really needed is something far less, and far easier to implement: a bijection not from _all_ A to all B, but one that needs only to be defined for those A's _in a specific Set&lt;A>_. The image of that would be a Set&lt;B>, and similarly, the inverse function would only be required to map _only_ the B's in that Set&lt;B> back to A's of Set&lt;A>. 

InvertibleFunction as given above doesn't make it easy to restrict ourselves to a smaller domain than the type parameter itself (Set&lt;A> is just a subset of A). 

Aside: scala functions have an "isDefinedAt(x: A)" method, which is precisely what I'm talking about. They don't define functions over all A, but only a subset of it (a predicate on elements of A, such as isDefinedAt, defines a set of A just as Set&lt;A> does, modulo not providing an iterator). 

But we have no isDefinedAt method, and we can't define one, and even if we could, interfaces with two methods are going to become much more costly than those with a single method when closures arrive, so I think we are pretty much stuck with the clumsier approach. The good news is that the clumsier approach is workable, but the specification of a potential Sets#transform would be forced to explain this subtlety, that not a complete bijection is required, but only one defined for the given Set&lt;A> and its image Set&lt;B>, for other inputs it would be free to return whatever. 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c11) posted by **andreou@google.com** on 2011-03-18 at 10:22 PM_

---

I cross-posted the above in https://github.com/google/guava/issues/219 sorry for the mess.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c12) posted by **fry@google.com** on 2011-03-22 at 06:28 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Release09`, `Milestone-Release10`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c13) posted by **fry@google.com** on 2011-03-23 at 01:49 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Release10`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c14) posted by **kevinb@google.com** on 2011-03-26 at 05:40 PM_

---

Just an idea we're kicking around (not a promise):

public abstract class Converter&lt;A, B> extends Function&lt;A, B> {
&nbsp;&nbsp;protected Converter() {}

&nbsp;&nbsp;// I don't love these names. The requirement is that converting
&nbsp;&nbsp;// forward and backward should get you back to a "similar" value,
&nbsp;&nbsp;// but strict invertibility is too much to require.
&nbsp;&nbsp;protected abstract B convertForward(A a);
&nbsp;&nbsp;protected abstract A convertBackward(B b);

&nbsp;&nbsp;@﻿Nullable public final B apply(@﻿Nullable A a) {
&nbsp;&nbsp;&nbsp;&nbsp;return (a == null) ? null : convertForward(a);
&nbsp;&nbsp;}

&nbsp;&nbsp;public final Converter&lt;B, A> inverse() { ... }

&nbsp;&nbsp;public final Converter&lt;A, C> compose(Converter&lt;B, C> next) { ... }

&nbsp;&nbsp;public final Iterable&lt;B> convertAll(Iterable<? extends A> inputs) { ... }

&nbsp;&nbsp;// also suggested, but... these are just slight conveniences for e.g.
&nbsp;&nbsp;// Lists.transform() anyway...
&nbsp;&nbsp;public final Collection&lt;B> convertAll(Collection<? extends A> inputs) { ... }
&nbsp;&nbsp;public final List&lt;B> convertAll(List<? extends A> inputs) { ... }
}

public final class Converters {
&nbsp;public static &lt;T> Converter&lt;T, T> identity() { ... }
}

Note that we always convert null to null. We believe this is easier for everyone than having it always be up to individual converter interpretation. And after all, "I don't know how many degrees F it is" is certainly equivalent to "I don't know how many degrees C it is".

We could add these to, e.g., common.primitives.Ints:

&nbsp;&nbsp;public static Converter&lt;String, Integer> stringConverter() { ... }

We could consider an InvertibleConverter (doesn't that name just roll off the tongue?) subtype that has the much stricter requirement that whenever 'B b = c.convertForward(a)' does not throw an exception, 'c.convertBackward(b).equals(a)' _must_ be true. This doesn't necessarily expose any more functionality, it just helps to caution against assumptions that garden-variety Converters have this property, which so very very many will not.

For example:

&nbsp;&nbsp;// convertBackward uses name(), not toString()
&nbsp;&nbsp;public static &lt;E extends Enum&lt;E>> InvertibleConverter&lt;String, E>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stringConverter(Class&lt;E> enumClass) { ... }

Comments?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c15) posted by **t...@duh.org** on 2011-04-06 at 01:11 AM_

---

Here's another suggestion at a top-level interface (which Converter might implement as a convenience for certain types of conversions).

public interface ReversibleFunction&lt;A, B> extends Function&lt;A, B> {
&nbsp;&nbsp;&nbsp;&nbsp;Function<? super B, ? extends A> reverse();
}

Note the capture-of (?)s in the reverse() signature here. An abstract Converter class with an exact one-to-one type equivalence could easily override this with an exact Function&lt;B, A> signature, but it leaves implementers the choice to use a Function with a slightly different signature for the reverse.

The signature of reverse() here also deliberately returns solely Function, not ReversibleFunction. Besides the fact that this is required with the capture-of's, it (1) might not be guaranteed that you could reverse().reverse() and get exactly the same results, or (2) might be that the ReversibleFunction has a complicated implementation, but the reverse of it is trivial with a slightly different signature (say, from a singleton).

Sample combiner utility method that could use the above:

public static &lt;A, B> ReversibleFunction&lt;A, B> join(final Function&lt;A, B> forward, final Function<? super B, ? extends A> reverse) {
&nbsp;&nbsp;&nbsp;&nbsp;return new ReversibleFunction&lt;A, B>() {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public B apply(A a) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return }
};
.

}
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c16) posted by **kevinb@google.com** on 2011-04-06 at 03:27 AM_

---

_Issue #457 has been merged into this issue._
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c17) posted by **j...@nwsnet.de** on 2011-04-06 at 07:59 AM_

---

I like the idea of a two-way converter between enums and their name representation.

Not sure about the other details, though (but I trust you on that).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c18) posted by **finnw1** on 2011-05-07 at 12:07 AM_

---

I wonder if the "almost-but-not-quite-a-bijection" problem could be mitigated by habitually using a class like this one during development:

class CheckedConverter&lt;A, B> extends ForwardingConverter&lt;A, B> implements InvertibleConverter&lt;A, B> {
&nbsp;&nbsp;&nbsp;&nbsp;protected B convertForward(A a) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B b = delegate().convertForward();
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Ensure that the conversion is reversible for this value
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;assert (convertBackward(b).equals(a)); // Or maybe use a method from Preconditions?
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return b;
&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;protected A convertBackward(B b) ... // similar
}

With a public entry point here:

public class Converters {
&nbsp;&nbsp;&nbsp;&nbsp;// ...
&nbsp;&nbsp;&nbsp;&nbsp;public static &lt;A, B>Converter&lt;A, B> checkedConverter(Converter&lt;A, B> c) {...}
&nbsp;&nbsp;&nbsp;&nbsp;// And maybe:
&nbsp;&nbsp;&nbsp;&nbsp;public static &lt;A, B>Converter&lt;A, B> checkedConverter(Converter&lt;A, B> c, Equivalence&lt;A> e1, Equivalence&lt;B> e2) {...}
&nbsp;&nbsp;&nbsp;&nbsp;// ...
}

I expect I would often find this early warning useful, even if I did not intend to implement InvertibleConverter in my public API.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c19) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c20) posted by **kevinb@google.com** on 2011-07-16 at 08:37 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Accepted`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c21) posted by **kevinb@google.com** on 2011-10-05 at 05:19 AM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Release11`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c22) posted by **fry@google.com** on 2011-11-16 at 07:33 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Release11`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c23) posted by **fry@google.com** on 2011-12-10 at 03:38 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Base`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c24) posted by **kevinb@google.com** on 2012-02-16 at 06:45 PM_

---

This is being worked on but may miss Guava 12.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c25) posted by **wasserman.louis** on 2012-02-16 at 07:07 PM_

---

_(No comment entered for this change.)_

---

**Blocking:** #603
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c26) posted by **wasserman.louis** on 2012-05-03 at 04:17 PM_

---

_(No comment entered for this change.)_

---

**Blocking:** #985
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c27) posted by **kevinb@google.com** on 2012-05-30 at 07:43 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Addition`
**Blocking:** -#603, -#985, #603, #985
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c28) posted by **cpovirk@google.com** on 2012-11-07 at 07:39 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Release14`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c29) posted by **kak@google.com** on 2013-01-29 at 06:28 PM_

---

_(No comment entered for this change.)_

---

**Owner:** kurt.kluever
**Labels:** -`Milestone-Release14`, `Milestone-Release15`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=177#c30) posted by **cgdecker@google.com** on 2013-12-19 at 06:59 PM_

---

https://github.com/google/guava/commit/75fda2a9fea9e3415c661e8688332c24ffddc940

---

**Status:** `Fixed`
**Labels:** -`Milestone-Release15`, `Milestone-Release16`
 
Add a fold method for Iterables _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=218) created by **rwallace1979** on 2009-08-12 at 11:37 PM_ --- I find myself always wanting to iterate over a collection to aggregate the data and wishing Google Collections had a fold operation. An implementation can be found on StackOverflow, &lt;http://stackoverflow.com/questions/950751/how-to-implement-a-list-fold-in-java/951337#951337>.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c1) posted by **rwallace1979** on 2009-08-13 at 12:07 AM_

---

A possibly easier implementation is to create a new FoldFunction type. And implement
foldLeft as b);
}
.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c2) posted by **andre.ruediger** on 2009-08-13 at 08:52 AM_

---

What about a compress() function? (Maybe there's a better name.) compress() would
have to return null in the case of failure. Like }

}
.

I'd like to hear your input on this. Do my random thoughts make sense to anybody?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c3) posted by **jvdneste** on 2009-08-14 at 09:42 AM_

---

I prefer the term reduce, and i'd also like to see it in the acc;
}
.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c4) posted by **jvdneste** on 2009-08-18 at 08:42 AM_

---

This requires the inclusion of a two-argument functor interface. I've seen negative 
reactions on previous requests so I'm thinking there's little chance of this one 
getting accepted. The fact that it keeps coming back accounts for something though.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c5) posted by **kevinb9n** on 2009-08-18 at 09:51 PM_

---

I am not specifically declaring this to be out of scope, but it's important to realize 
that this kind of functional programming stuff has never been our core focus. The 
main reason we have Predicate at all is just that it's hard to filter an iterator by 
hand, and a filter() library method needs something like that, so there it is. The 
main reason we have Function is that MapMaker needs it.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c6) posted by **kevinb9n** on 2009-09-17 at 04:59 PM_

---

(remember that "Accepted" != "promise that we will do it")

---

**Status:** `Accepted`
**Labels:** `post-1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c7) posted by **kevinb9n** on 2009-09-17 at 05:45 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`post-1.0`, `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c8) posted by **kevinb9n** on 2009-09-17 at 05:57 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c9) posted by **kevinb@google.com** on 2010-07-30 at 03:53 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c10) posted by **dan.rosen** on 2010-10-09 at 12:17 AM_

---

A two arg version of Function isn't necessary. Just some sort of tuple type, let's say Pair&lt;A, B>. So Rich's implementation reduces to:

static &lt;A, B> A foldLeft(Iterable&lt;B> xs, A z, Function&lt;Pair&lt;A, B>, A> f)
&nbsp;&nbsp;&nbsp;&nbsp;{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A p = z;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for (B x : xs)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p = f.apply(new Pair(p, x));
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return p;
&nbsp;&nbsp;&nbsp;&nbsp;}
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c11) posted by **cgdecker** on 2011-04-08 at 03:30 AM_

---

_Issue #446 has been merged into this issue._
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c12) posted by **cgdecker** on 2011-04-08 at 04:48 AM_

---

Creating a Pair type and using it like that just makes the semantics of the function less clear and forces lots of Pair objects to be created when they needn't be.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c13) posted by **gscerbak** on 2011-05-10 at 11:39 AM_

---

I have written Iterable around AbstractLinkedIterator with static factory method called unfold which tkes first element and a Function to compute the next element. 
Then I added static factory methods first() and rest() for Functions decomposing Iterables using Iterables.getFirst() and Iterables.skip().
Finally fold look as simply as accumulator));
}
.

The accumulator is simply a function - guava javadocs allow for functions with side effects, so it is possible to store the state of folding in the function itself. This is a trade-off between purity and problems with tuples in Java, resp. creating custom classes just for the accumulated is(5050));
}
.

If you find this approach useful, just let me know, I will polish the source and I will publish it.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c14) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c15) posted by **fry@google.com** on 2011-12-10 at 03:40 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c16) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c17) posted by **qualidafial** on 2012-04-09 at 05:33 PM_

---

In my own project I implemented folding as follows:

&nbsp;&nbsp;interface Reducer&lt;A, B> {
&nbsp;&nbsp;&nbsp;&nbsp;A reduce(A a, B b);
&nbsp;&nbsp;}

&nbsp;&nbsp;class Iterables {
&nbsp;&nbsp;&nbsp;&nbsp;static &lt;A, B> A reduce(Iterable&lt;B> xs, A z, Reducer&lt;A, B> reducer);
&nbsp;&nbsp;}

&nbsp;&nbsp;class BigDecimals {
&nbsp;&nbsp;&nbsp;&nbsp;static Reducer&lt;BigDecimal, BigDecimal> sum();
&nbsp;&nbsp;}

With these in place I could reduce a list to a sum as follows:

&nbsp;&nbsp;Iterable&lt;BigDecimal> lineTotals = ...
&nbsp;&nbsp;BigDecimal grandTotal = Iterables.reduce(lineTotals, BigDecimals.sum());

I hope you guys will consider adding this feature to Guava; with this one exception, Guava has always seemed to have exactly what I need for data processing.

If there is interest from the project maintainers in adding this feature, I'd be delighted to do the work and submit a patchset.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c18) posted by **wasserman.louis** on 2012-04-09 at 07:17 PM_

---

FYI, I think we're astronomically more concerned about the API design, the potential for misuse, and our already existing concern over the readability of the functional idioms we provide (pending Java 8), than the difficulty of adding the feature once an API is endorsed, but we'll definitely update this issue if we decide to pursue folds and the like.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c19) posted by **kevinb@google.com** on 2012-05-30 at 07:43 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Addition`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c20) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c21) posted by **phidias51** on 2012-07-09 at 08:21 PM_

---

I recently found myself needing the same type of functionality for calculating basic statistical functions on lists of numbers. Since the interest within the project seems limited, has anyone given any thought to making this a separate project?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c22) posted by **wasserman.louis** on 2012-07-09 at 08:46 PM_

---

This certainly exists in other projects (functional-java is an example), but...yeah.

(I have to admit, I still don't see what advantage this sort of thing would gain over a traditional for-loop.)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c23) posted by **phidias51** on 2012-07-09 at 09:39 PM_

---

In my case I want to be able to do something like this:

Double sum = List.reduce(List&lt;Double> values, SumReduction summation)

Additional classes for StandardDeviation, Mean, Median, Max, Min would also be useful. Having the interfaces and a couple of simple implementations would make it easier for people to be able to implement more complex mathematical functions.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c24) posted by **wasserman.louis** on 2012-07-10 at 01:04 PM_

---

...I'm still not seeing what possible advantage this would have over methods named e.g. sum(List&lt;Double> values), mean(List&lt;Double> values), median(List&lt;Double> values), etc. If you write those methods out, they're almost certainly _shorter_ than "functional-style" reducers based on anonymous classes. If you want to be able to plug different ones in, you're still probably better off with just a Function&lt;List&lt;Double>, Double>.

Furthermore, some of those operations _can't_ be implemented as simple folds.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c25) posted by **phidias51** on 2012-07-10 at 03:53 PM_

---

The problem with creating specific methods like sum, mean, etc is that it's always incomplete in someone's mind. By creating an interface and a couple of simple implementations, it demonstrates how implementations will be used. If someone wants to then spinoff an SPI project to implement a more complete set of functions, or to create libraries of different types of functions, they could do that.

Although the Function&lt;List&lt;Double>, Double> complies with the generic signature of the Function interface, it doesn't make sense from a practical sense. As someone mentioned earlier, the intent of the Function interface is to perform an operation on a each object in a collection with the intent being that you could end up with a transformed copy of the list, or a transformed instance of the original list. 

If you accidentally tried to use one of these implementations (of Function&lt;List&lt;Double>, Double>) with Lists.transform, the code would compile, but you would have runtime errors when you ran it. Therefore, I think a new interface with a different method signature would better communicate the intent of these types of aggregate functions. 

Could you give me some examples of "operations [that] _can't_ be implemented as simple folds"? I'm not claiming that it's a panacea, nor doubting what you say, merely interested in what you mean.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c26) posted by **qualidafial** on 2012-07-10 at 08:46 PM_

---

Standard deviation cannot be implemented as a simple fold, since you must first do one pass to determine the mean, then a second pass to accumulate the square of the difference from the mean.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c27) posted by **wasserman.louis** on 2012-07-10 at 10:44 PM_

---

And median can't be implemented as a simple fold, since all the implementations I know of require either multiple passes, or at least O(n) extra storage, which kind of defeats the point.

I'm still not really getting the point, though. If you want to write a project to implement more functions...you write a library with a bunch of static methods: foo(List&lt;Double>), or whatever. (I also vaguely recall that Guava's been working on some statistics utilities, which we might see sometime in the future...)

In any event, the only advantage I can think of to making these objects, instead of e.g. static methods, is being able to pass them around polymorphically and then apply them...which is a need satisfied by Function&lt;List&lt;Double>, Double>. I'm not suggesting anyone use Lists.transform with such a function, but I am saying that you can pass them around polymorphically and call their apply() method, which seems to me like it covers the one advantage objects would have over functions.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c28) posted by **ian.b.robertson** on 2012-07-24 at 05:09 PM_

---

&lt;pedantry>Standard deviation could be implemented with a fold, although the accumulator would need to be accumulating both sum(x_i) and sum(x_i)^2.&lt;/pedantry>
That said, I agree with Louis - for loops aren't that bad, and they perform well.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c29) posted by **bytefu** on 2013-08-30 at 08:29 AM_

---

> That said, I agree with Louis - for loops aren't that bad, and they perform well.
> Assembler code isn't that bad, and it performs well. Does it mean that we don't need C and Java? 'fold' is a standard idiom for processing lists, after all.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c30) posted by **ceefour666** on 2013-08-30 at 08:45 AM_

---

+1 for #﻿29 above.

The nicest thing about anything that takes closures is that they translate well to the function composition world, that we're already seeing partly with Guava, and will be even more when Java 8 Lambda comes along (syntax sugar to write the FoldFunction). Project Sumatra and proposed Java 9 parallelism also uses this.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c31) posted by **orionllmain** on 2013-08-30 at 08:57 AM_

---

Guys, introducing fold without adding other high order functions like skipWhile, takeWhile, reduce, groupBy, flatMap, indexWhere and so on is not the best idea, I think. Guava is not an FP library. If you want fold, use totallylazy. Or Scala.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c32) posted by **bytefu** on 2013-08-31 at 08:45 AM_

---

-> #﻿31
So, you see these as FP only, despite their clear correlation to list processing? Guava has few tools for processing collections, why not add some more if people need them? Or it's like: "It's our library, we don't want this stuff. People use it and request some features? Fork 'em!"

Guava already has filter() and transform(). They're FP, they don't belong there. Or do they? I don't see any real reasons not to include some "FP" methods for collection processing. They reduce :) code, they are faster to write than beloved for-loops, and they look nicer in IntelliJ Idea even today, without Java 8 with it's nya-looking lambdas.

You suggest to use some other library or even Scala just for one or two missing functions? I don't want my pom.xml to become separate project, just because I use a library every time I need a method.

Finally, do Guava developers have any reasons to not to add fold() and stuff, other than ideological (because they think almost every list processing idiom is FP only)? "Open source" not only means that sources are open (pretty obvious, huh), but also means some cultural things - like sharing ideas, receiving requests and implementing some useful stuff, you know. Position like "I don't need that, so nobody does" seems kind of strange here.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c33) posted by **orionllmain** on 2013-08-31 at 04:24 PM_

---

I want only say that fold is definitely not enough for me. Why fold? Why not flatMap, maxBy or zip? You either add all of these methods, or you add nothing. Adding only fold looks ridiculous.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c34) posted by **xaerxess** on 2013-08-31 at 09:17 PM_

---

> Why fold? Why not flatMap, maxBy or zip?
> &nbsp;\- flatMap - there's [FluentIterable#transformAndConcat](https://google.github.io/guava/apidocs/com/google/common/collect/FluentIterable.html#transformAndConcat%28com.google.common.base.Function%29),
> &nbsp;\- maxBy - use [Ordering#onResultOf](https://google.github.io/guava/apidocs/com/google/common/collect/Ordering.html#onResultOf%28com.google.common.base.Function%29) + [Ordering#max](https://google.github.io/guava/apidocs/com/google/common/collect/Ordering.html#max%28java.lang.Iterable%29),
> &nbsp;\- zip - not in Guava - [requires Pair / Tuple](http://code.google.com/p/guava-libraries/wiki/IdeaGraveyard#Tuples_for_n_>=_2) + see [this issue](https://github.com/google/guava/issues/677).
> 
> I don't see any real reasons not to include some "FP" methods for collection processing.
> They would require adding many things to Guava API (two-arg functions, pair, etc.) - see new [java.util.function package from Java 8](http://download.java.net/jdk8/docs/api/java/util/function/package-summary.html) - but contrary to Java 8, functional idioms in Java 6/7 are horribly verbose and ugly. 

Although Guava could adopt some names / API from Project Lambda to "backport" FP goodies to Java 6/7, it would be painful to use fold without language support (i.e. without lambdas). And bringing FP idioms to Java as such is separate discussion, for example my collegue believes that average "enterprise" Java programmer will have troubles with lambdas and they are not necessary in the language where for-loop is a common and well known idiom. (FYI: I know / have been using higher order functions in Lisp / Perl and it's not the same experience for both code writer and reader in Java, at least without lambdas in core for which I am waiting impatiently.)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c35) posted by **lowasser@google.com** on 2013-12-03 at 07:56 PM_

---

_Issue #1601 has been merged into this issue._
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c36) posted by **lowasser@google.com** on 2013-12-03 at 07:56 PM_

---

_Issue #1601 has been merged into this issue._
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c37) posted by **jysjys1486** on 2014-02-04 at 08:24 AM_

---

The following assumes sequential operation:

interface Collector&lt;T,A,R> {
&nbsp;&nbsp;&nbsp;A newContainer();
&nbsp;&nbsp;&nbsp;void accumulate(A container, T element);
&nbsp;&nbsp;&nbsp;R finish(A container);
}
Like Java 8 Collector, though functions of supplying and accumulating is pushed to the interface.

&lt;T,A,R> R collect(Iterable<? extends T> elements, Collector<? super T, A, ? extends R> collector) {
&nbsp;&nbsp;&nbsp;final A container = collector.newContainer();
&nbsp;&nbsp;&nbsp;for (T each : elements)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;collector.accumulate(container,each);
&nbsp;&nbsp;&nbsp;return collector.finish(container);
}
Typical mutable reduction implemention

class Collectors {

&nbsp;&nbsp;&nbsp;public static &lt;T, C extends Collection&lt;T>> Collector&lt;T,?,C> toCollection(Supplier<? extends C> factory) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// ...
&nbsp;&nbsp;&nbsp;}

&nbsp;&nbsp;&nbsp;// ...

}
Utility for collectors.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c38) posted by **Lord.Laraby** on 2014-03-26 at 11:42 PM_

---

I just use this arrangement:
public interface Foldable&lt;A, R> {
&nbsp;&nbsp;&nbsp;&nbsp;R fold(A left, R right);
&nbsp;&nbsp;&nbsp;&nbsp;/**
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* reduce or fold the {@﻿link Iterable} with an initial value and folding function from left to right
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* @﻿param list
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* @﻿param acc
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* @﻿param f the Foldable (interface override)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* @﻿return
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_/
&nbsp;&nbsp;&nbsp;&nbsp;static &lt;A, R> R foldRight(final Iterable&lt;A> list, final R acc, final Foldable&lt;A, R> f) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (Iterables.isEmpty(list))
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return acc;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return foldRight(Iterables.skip(list, 1), f.fold(Iterables.getFirst(list, (A)null), acc), f);
&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;/_*
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* reduce or fold the {@﻿link Iterable} with an initial value and folding function from right to left
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* @﻿param list the iterable list or set etc.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* @﻿param acc initial value
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* @﻿param f the Foldable (interface override)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\* @﻿return the result of folding from right to left
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/
&nbsp;&nbsp;&nbsp;&nbsp;static &lt;A, R> R foldLeft(final Iterable&lt;A> list, final R acc, final Foldable&lt;A, R> f) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (Iterables.isEmpty(list))
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return acc;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return f.fold(Iterables.getFirst(list, (A)null), foldLeft(Iterables.skip(list, 1), acc, f));
&nbsp;&nbsp;&nbsp;&nbsp;}
}
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=218#c39) posted by **wasserman.louis** on 2014-03-27 at 01:45 AM_

---

The nested Iterables.skips will give extremely terrible performance, certainly quadratic if not worse. You should really just be folding over the Iterator instead.
 At this point we're not adding any more functional features to Guava because of Java8.
 But.... What happens with Android (Android still doesn't have Java 8) or others frameworks without Java 8?
 exactly... on Java 8 i am using the language but where i can't use java 8 i would want to rely on 3rd party libraries, like guava...
 @ekovacs : I guess they have stopped this because they are migrating to Java 8 in Android N.
 No, it's not that, we'll be supporting a pre-Java-8 Guava fork for quite a long time. And we're committed to maintaining that as a high quality experience. However, that doesn't go so far as to cover investing in brand new features that can only at best hope to be poor substitutes for what's coming in 8.
 I have not found a decent way to fold in Java 8 ;)
 
Make AbstractMultiset + Multisets.AbstractEntry public _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=157) created by **ben.maurer** on 2009-05-02 at 08:54 PM_ --- If you want to implement your own multiset, doing so requires a fair bit of code. In some cases, a ForwardingMultiset isn't appropriate for the task (in my use case I was creating a multiset class that represented a cross product of other multisets). These classes seem well thought out wrt being implemented by other classes, and therefore good candidates for being public.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c1) posted by **jared.l.levy** on 2009-05-02 at 09:16 PM_

---

What multiset implementation would you want, beyond those that are already provided?

We'd need some use cases to justify making any additional classes public.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c2) posted by **ben.maurer** on 2009-05-02 at 09:59 PM_

---

The use case I implemented was a multiset that is the product of other multisets. Eg,
given:

[a x 1, b x 2]
[c x 3, d x 4]

return [ac x 3] [ad x 4] [bc x 6] [bd x 8]

Other use cases include implementing the sum of multisets lazily, or using a counting
bloom filter to implement the multiset.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c3) posted by **kevinb9n** on 2009-05-05 at 03:59 PM_

---

Making our skeletal implementation classes like AbstractMultiset public is something
I've always figured we may eventually get around to, but it is a lot of work, and the
kind of work that isn't fun. So every additional strongly compelling use case helps.

---

**Status:** `Accepted`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c4) posted by **kevinb9n** on 2009-08-19 at 01:32 AM_

---

_(No comment entered for this change.)_

---

**Labels:** `post-1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c5) posted by **kevinb9n** on 2009-09-17 at 05:45 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`post-1.0`, `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c6) posted by **kevinb9n** on 2009-09-17 at 05:57 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c7) posted by **kevinb@google.com** on 2010-07-30 at 03:53 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c8) posted by **wasserman.louis** on 2011-02-14 at 06:59 PM_

---

Guava 7 augments ForwardingMultiset with a number of "sensible default" implementations of methods. Would this suffice for your needs?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c9) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c10) posted by **fry@google.com** on 2011-12-10 at 03:37 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c11) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c12) posted by **kevinb@google.com** on 2012-05-30 at 07:43 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Addition`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c13) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c14) posted by **eyalsoha@google.com** on 2013-11-07 at 10:49 AM_

---

I tried to extend AbstractMultiset and failed, too. Here's my use case:

I want a multiset where it's possible to delete multiple, different entries quickly. For example,

class Words extends AbstractMultiset&lt;String> {
&nbsp;&nbsp;boolean removeByLetter(char c1) {
&nbsp;&nbsp;&nbsp;&nbsp;// remove all Strings that contain the letter c1
&nbsp;&nbsp;}
}

Iterating over the Strings of the multiset, even using the entryIterator(), isn't as fast as storing a separate hashmap that maps from letter to words_containing_that_letter.

Without being able to extend AbstractMultiset, I have to resign myself to:

1) Rewrite AbstractMultiset: extend AbstractCollection, implement Multiset, and write a lot of boilerplate code. This sucks, I should just be using AbstractMultiset.

2) Don't extend nor implement, just encapsulate a Multiset. But now, I'll need to provide a getMultiset() function so that I can still use the Multiset features, eg myWords.getMultiset().entrySet(). Ugly. But that function will have to create a deep-copy of my data because if someone modifies the data without updating the words_containing_that_letter, my data will break, so really it should be myWords.getImmutableMultiset(). The copy will have to be deep because the inner members, if they were objects instead of Strings, could be modified, and then the "Immutable" would be a half-lie.

This class should be public. AbstractSet, AbstractCollection, etc, are public.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c15) posted by **kevinb@google.com** on 2013-11-07 at 02:56 PM_

---

Can you explain why ForwardingMultiset doesn't work for you?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c16) posted by **eyalsoha@google.com** on 2013-11-07 at 03:22 PM_

---

I'm not familiar with ForwardingMultiset... It seems that it might work

From what I read, it just calls all the methods of the delegate. I don't see how this is better: it's just encapsulation instead of inheritance. What was the purpose? ForwardingMultiset appears unused...
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c17) posted by **SeanPFloyd** on 2013-11-07 at 03:42 PM_

---

https://code.google.com/p/guava-libraries/wiki/CollectionHelpersExplained#Forwarding_Decorators
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c18) posted by **eyalsoha@google.com** on 2013-11-07 at 03:56 PM_

---

I read that URL. Thanks. It doesn't explain why Forwarding Decorators are better than using, for example, AbstractMultiset. And now I have to override add(Object, int) _AND_ add(Object), which is just more boilerplate for me!

What is the advantage here?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c19) posted by **cpovirk@google.com** on 2013-11-07 at 04:36 PM_

---

The place to start is probably Effective Java's "Item 16: Favor composition over inheritance."
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c20) posted by **lowasser@google.com** on 2013-11-07 at 05:22 PM_

---

If you extended AbstractMultiset, you'd have to e.g. reimplement entrySet() for yourself. In both cases you have to override something, but I would tend to say that on balance, ForwardingMultiset requires less work for this case in any event.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c21) posted by **kevinb@google.com** on 2013-11-07 at 05:29 PM_

---

Also note that you can compose with any backing Multiset, so changing to one with sorted keys, for example, is trivial.

Our AbstractMultiset class is an _implementation_, which we wish to reserve the right to change at will, as doing so benefits all its consumers. But making it public would force us to commit to many aspects of that implementation. The kinds of implementation changes we might need to make would risk breaking users.

I'm actually going to close this issue now. My plea that "every additional strongly compelling use case helps" went unanswered for _four and half years_ (a period of steady growth in Guava's popularity). If there were a reason 1/10th as strong as we'd need, we would know it by now.

---

**Status:** `WontFix`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c22) posted by **eyalsoha@google.com** on 2013-11-07 at 06:36 PM_

---

The word "Favor" implies that I'd have a choice but here my hand is forced. 

On the one hand, I should favor composition so AbstractMultiset is private. On the other hand, it's extended by many classes within the library to make all the different types of Multisets, so clearly inheriting from it isn't all _THAT_ wrong.

Guava has deviated here from the rest of the AbstractWhatevers that are public in java.util.*

So I'm stuck with inheriting from ForwardingMultiset and obeying the if-you-override-a-you-must-override-b for all the add()/remove(), etc. Such a waste. :-(
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c23) posted by **cgdecker@google.com** on 2013-11-07 at 06:47 PM_

---

@﻿eyalsoha: Did you see Louis's comment above about how you'd have to implement entrySet() for yourself if you were to extend AbstractMultiset? That's much more complex than having to override a few methods I'd say.

> Guava has deviated here from the rest of the AbstractWhatevers that are public in java.util.*

I think we're perfectly fine with that... the design of Guava's collection libraries was directly informed by experience from the JDK libraries, including things that were probably mistakes.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c24) posted by **eyalsoha@google.com** on 2013-11-07 at 07:51 PM_

---

I hadn't seen Louis' comment before my post. But I'd still rather override entrySet(), and if I forget, get an exception, than use ForwardingMultiset, override add(Object), and if I forget to also override add(Object,int), addAll()..., sliently have my data lose consistency.

AbstractIterator was a nice function for me: implement the Iterator interface but with less code. I thought that AbstractMultiset would do that, too. Instead there's ForwardingMultiset, which isn't saving me lines of code and risky if I miss an override. I'm safer just implementing the Multiset interface and skipping ForwardingMultiset. I'm having a hard time imagining a case where using ForwardingMultiset _would_ be advantageous.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c25) posted by **cpovirk@google.com** on 2013-11-07 at 07:55 PM_

---

Even with AbstractMultiset, you still have to override both add(Object) and add(Object, int), since you don't know which one of the two calls the other in the implementation. Or rather, you might be able to check which one of the two calls the other currently, but this could switch at any time. What you're asking is more than just for us to insert the word "public" to the class; what you're asking is that we commit to the implementation details of our Multiset implementations permanently. That's something that we could do if there were enough need, but it's a big project.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c26) posted by **eyalsoha@google.com** on 2013-11-11 at 04:02 PM_

---

Without that commitment, the code is useless. My only alternative is ForwardingMultiset, which is too dangerous because it exposes the underlying Multiset without maintaining my invariants.

I don't know which is the lesser of two evils:

1) Override all the methods myself (essentially re-implementing AbstractMultiset).
2) Copy AbstractMultiset into my package and use it.

In my situation, what would you suggest?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c27) posted by **cpovirk@google.com** on 2013-11-11 at 04:46 PM_

---

The delegate() method that exposes the underlying Multiset is |protected|, and your ForwardingMultiset subclass can create the Multiset itself, so it's safe from external users.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c29) posted by **eyalsoha@google.com** on 2013-11-11 at 05:46 PM_

---

Imagine my previous example: I have a Multiset&lt;String>, but I want to add to it public functions to let me get a Multiset&lt;String> based on letter. I want it to work quickly so I maintain a Map<char, Set&lt;String>> that maps from letters A-Z to the elements in the Multiset and provide an iterator iteratorByLetter(char c) that returns letterMap.get(c). I've potentially doubled the size of my data in order to gain speed for my added functions.

To keep the map valid, I need to intercept add and for each add to the Multiset, add to the map. Same for remove. So I override ForwardingMultiset#add(E,int) and ForwardingMultiset#remove(Object,int). These are the only two public functions in ForwardingMultiset, so I think that I'm covered.

However, if I forget to also override ForwardingMultiset#add(E), then I get in trouble. In that case, my underlying Multiset will be updated by the call to delegate().add() but my map won't be updated! My class will corrupt itself silently. The only way to ensure that I override ForwardingMultiset correctly is to follow the chain of all the super-classes and make sure that I don't miss a function. That is a pain. Same goes for remove() and whatever else might be in there. The documentation for ForwardingMultiset explains as much. The documentation says that if you override add(E), you must override add(E,int). So you write the latter and the former is an exact copy of add(E) from AbstractMultiset. In fact, to use ForwardingMultiset, I basically have to copy-paste all the code from AbstractMultiset except for the abstract functions, which I'll implement.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c30) posted by **lowasser@google.com** on 2013-11-11 at 05:56 PM_

---

> In fact, to use ForwardingMultiset, I basically have to copy-paste all the code from AbstractMultiset except for the abstract functions, which I'll implement.

That's why the standardXYZ methods are there: to provide the implementations you'd get from AbstractMultiset.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c31) posted by **eyalsoha@google.com** on 2013-11-11 at 07:02 PM_

---

How can I use standardAdd() to help me here? I want to write a single function that will extend all the different kinds of add(). Also, I want to write a single function to override all the different kinds of remove().

What function am I supposed to override? The documentation tells me that overriding just one of the add() functions won't work. I should override standardAdd()?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c32) posted by **xaerxess** on 2013-11-11 at 09:46 PM_

---

@﻿eyals...: See an example class I attached. It's just a ForwardingMultiset demo, not a bullet proof, ready to use solution to your problem (you should test it well by yourself, maybe using Guava testlib?). If I missed something obvious, please forgive me - it's quite late (in my timezone).

> How can I use standardAdd() to help me here? 

standardXyz methods are protected members of each ForwardingAbc class in Guava which _can_ be used in your class (it's optional because sometimes you _don't_ want add and addAll to behave in the same manner). These methods are well documented, just read them all before (or while) extending ForwardingMultiset - typically it's something like:

&nbsp;&nbsp;&nbsp;"A sensible definition of FOO in terms of BAR. If you override BAR, you may wish to override FOO to forward to this implementation."

In your case just write add(String, int) implementation and use standardAdd(String) and standardAddAll(Collection), plus override remove(Object, int) and use standardRemove(Object). There are also some pitfalls with setCount, clear and entry/elementSet (I've made these return unmodifiable Sets), but you'd have same issues extending AbstractMultiset.

Which brings me to this issue's purpose. Everything what can be done with AbstractMultiset can also be done with ForwardingMultiset and both would require care. On the other hand JDK collections don't have forwarding decorator at all, just skeleton abstract implementations. Joshua Bloch, who wrote Effective Java referenced earlier in the comments, is also creator of JDK's collections framework and AFAIK also Googler, so I'm curious what's his point of view on this particular issue (and BTW, why Multiset aka Bag wasn't included in JDK). 

Anyway, if you have more questions about your particular case (not about making AbstractMultiset public), ask on Guava discussion group or on Stackoverflow.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c33) posted by **eyalsoha@google.com** on 2013-11-12 at 09:44 AM_

---

Thanks for your hard work! That's a nice class and would certainly work. But it also serves to demonstrate the problems with ForwardingMultiset.

In your class, you've defined add(String), add(String,int), and addAll(Collection<>), the latter two in terms of standardAdd*, the former, not. It would be very easy for me, as an extender of the class, to forget one of those or to use standardAdd in the wrong place. And if I make a mistake, the default behavior would not update letterMap; it would instead silently make my data inconsistent. I'd rather throw an exception than silently fail later.

A nicer version of ForwardingMultiset or AbstractMultiset would have add(String,int) be abstract so that the user _must_ override it and then provide defaults of add(String), addAll(Collection&lt;String>), etc, that call add(String,int). The user would then have to redefine just one function. If he forgot, he would get a compile-time error. (Excellent!) And if he wanted to customize the others for speed, he could.

People talk about preferring encapsulation over inheritance but it's a preference, not a strict rule. This is a perfect example when inheritance is the right thing to do: it obeys Liskov's Substitution Principle to a tee. The "encapsulate don't inherit" rule was invented as a backlash to the misuse of inheritance, but now the pendulum has swung back the other way and we've got dozens of Forwarding____ classes that don't save programmers any boilerplate code.

For my uses, I gave up on implementing Multiset<>. "implements Multiset<>" would require writing too much code and "extends ForwardingMultiset<>" doesn't save me enough typing for the danger that is engenders.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=157#c34) posted by **cpovirk@google.com** on 2013-11-12 at 04:59 PM_

---

For what it's worth, I share your concerns with the forwarding collections, as do others on the team. We have a long-standing internal issue filed to investigate whether there is a better way.[*] For your case, that better way probably is an improved AbstractMultiset, but given the soft demand for it over the years, it's unlikely to be a priority for us soon. Sorry.

[*] Come to think of it, I should move that to the external tracker. Done: https://github.com/google/guava/issues/1575
 
Impossible-looking race in AbstractScheduledServiceTest.testDefaultExecutorIsShutdownWhenServiceFails _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1751) created by **cpovirk@google.com** on 2014-05-12 at 06:30 PM_ --- I saw this failure once at internal CL 66812931 (test ID 6c02838a-8b9d-4ff3-ab06-89ecb4a8f01b -- this is a non-mint client, but the only change (to RateLimiterTest) ought to have affected only another test suite). For all I know, there's some bug in the JVM or java.util.concurrent, but I could believe that there's a race in ListenerCallQueue or another Guava class that we missed. junit.framework.AssertionFailedError &nbsp;&nbsp;&nbsp;&nbsp;at junit.framework.Assert.fail(Assert.java:48) &nbsp;&nbsp;&nbsp;&nbsp;at junit.framework.Assert.assertTrue(Assert.java:20) &nbsp;&nbsp;&nbsp;&nbsp;at junit.framework.Assert.assertTrue(Assert.java:27) &nbsp;&nbsp;&nbsp;&nbsp;at com.google.common.util.concurrent.AbstractScheduledServiceTest.testDefaultExecutorIsShutdownWhenServiceFails(AbstractScheduledServiceTest.java:217)  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1751#c1) posted by **lukes@google.com** on 2014-05-12 at 08:11 PM_

---

le sigh.

That test does appear to depend on listeners executing in a particular order, which is problematic (and technically undefined). I think this is the same race as guava issue 1715 and the bug is in the test.

While it would appear that the listeners will execute in order there are actually two places in this test where we try to flush the call queue. 1. at the end of AbstractService.startAsync() 2. at the end of the Runnable in AbstractScheduledService.doStart() where we call notifyFailed.

notifyFailed will add the two listeners to the call queue and then try to execute them, this can race with the finally block in AbstractService.startAsync(). So since they are both on the sameThreadExecutor (and there are two threads trying to execute the tasks). It is possible for the two listeners to run on different threads. And since calling CountDownLatch.countDown() is presumably faster than shutting down a thread pool, it can return first. So the simplest fix would be just to call awaitTermination instead of asserting isShutdown. Or we can find some way to insert a happens-before edge between the two listeners.

I believe this race was probably introduced by the change to use ListenerCallQueue since that really decreased the amount of synchronization in play between listener executions.

Doe that theory of the crime seem reasonable?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1751#c2) posted by **cpovirk@google.com** on 2014-05-13 at 12:52 AM_

---

Yes, that sounds reasonable to me. Thanks for digging.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1751#c3) posted by **lukes@google.com** on 2014-05-14 at 12:54 AM_

---

This has been fixed.
https://github.com/google/guava/commit/04846be2de2d3841f2ea4f756588af8b1470635f

---

**Status:** `Fixed`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1751#c4) posted by **cgdecker@google.com** on 2014-08-05 at 10:31 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Release18`
 
Bit manipulation for primitive types  _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=739) created by **chriss.dev** on 2011-10-05 at 01:01 PM_ --- Hello everybody, it would be great to have a class to perform simple bit manipulation on primitives in a human readable way. Assume we have... &nbsp;short i=123; //0b0111_1011 &nbsp;int bitIndex=3; //the index of the bit we want to manipulate ...add the following use cases: &nbsp;&nbsp;//1. set a bit &nbsp;&nbsp;i |= (1<&lt;bitIndex); &nbsp;&nbsp;//2. clear a bit &nbsp;&nbsp;i &= ~(1<&lt;bitIndex); &nbsp;&nbsp;//3. Check if the bit at the given index is 'true' &nbsp;&nbsp;if (((i>>bitIndex)&1)==1){..} &nbsp;&nbsp;//or &nbsp;&nbsp;if ((i>>bitIndex&1)==1){..} All 3 cases are extreme hard to read and i think it can be done in an more readable way. Since 'java.util.BitSet' can't help use here I would like to see a class like 'com.google.common.primitives.Bits'. Here are 2 samples for the 3 cases using a mirco-DSL API : import static com.google.common.primitives.Bits.*; //---- proposal A &nbsp;&nbsp;//1. set a bit &nbsp;&nbsp;i = setBit(bitIndex,true).on(i); &nbsp;&nbsp;//2. clear a bit &nbsp;&nbsp;i = setBit(bitIndex,false).on(i); &nbsp;&nbsp;//3. Check if the bit at the given index is 'true' &nbsp;&nbsp;if (isBit(bitIndex).on(i)==true){..} //---- proposal B &nbsp;&nbsp;//1. set a bit &nbsp;&nbsp;i = using(i).setBit(bitIndex,true); &nbsp;&nbsp;//2. clear a bit &nbsp;&nbsp;i = using(i).setBit(bitIndex,false); &nbsp;&nbsp;//3. Check if the bit at the given index is 'true' &nbsp;&nbsp;if (using(i).getBitAt(bitIndex)==true){..} What do you think about it?  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c3) posted by **chriss.dev** on 2011-10-05 at 06:33 PM_

---

The attachment contains a simple implementation of proposal A.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c4) posted by **wasserman.louis** on 2011-10-10 at 02:30 PM_

---

This might happen in common.math, but...I'm not sure it's necessary. If you're doing bit manipulation at all, you're already doing pretty low-level stuff, and I'm not sure that most people doing bit manipulation at all need increased readability?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c5) posted by **wasserman.louis** on 2011-10-10 at 02:33 PM_

---

(I'm very much willing to be convinced here. I just want someone else to convince me.)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c6) posted by **chriss.dev** on 2011-10-10 at 04:04 PM_

---

When it comes to bit manipulation at the most ppl get afraid because:
- it is a mess of '>>', '^', ... 
- it takes to much time to remind basic bit operations (as you said, it not a common task) 
- it's error prone ('>>' vs '>>>')
- is unreadable and it takes a too looooong to understand code that perform plenty bit operations
- there a to many ways to perform even simple tasks (some ppl using math-operation like % to check for a bit)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c7) posted by **dancerjohn** on 2011-10-10 at 08:15 PM_

---

I know I have looked at code that does bit manipulation and been thinking "what the he**?" Crap, now I have to go refresh on bit manipulation. I recently looked at BitSet but was not impressed. After that I immediately went to Guava primitives and was sad to see that there was not a better bit manipulation class.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c8) posted by **Maaartinus** on 2011-10-12 at 06:12 PM_

---

I don't think that such a class would get used a lot. Dealing with bits usually means that efficiency is important, and creating an object in order to set a single bit could mean a slowdown by a big factor. Your proposal A could possibly be optimized by pre-creating 64 BitGetter-s and 128 BitManipulator-s, your proposal B could not. I'm not sure if it helps much, since it could lead to more cache-misses.

So maybe a low-level syntax A'

i = setBit(bitIndex, true, i);
if (isBit(bitIndex, i)){..}

or maybe B'

i = setBitOf(i, bitIndex, true);
if (isBitOf(i, bitIndex)){..}

could get more use as it involves no runtime cost.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c9) posted by **finnw1** on 2011-10-13 at 01:14 AM_

---

I probably wouldn't use it either (for performance reasons) but the votes on this StackOverflow answer suggest that some coders prefer an inefficient-but-more-readable syntax:

http://stackoverflow.com/questions/1092411/java-checking-if-a-bit-is-0-or-1-in-a-long/1092551#1092551
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c10) posted by **chriss.dev** on 2011-10-13 at 05:55 PM_

---

I have written a small performance test, it calculates a checksum 2mio times. 
The results are closer than expected...
~3700ms <- proposal A (without any parameter assertions)
~1500ms <- using raw operations

..not that bad, i think ! 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c11) posted by **chriss.dev** on 2011-10-13 at 06:14 PM_

---

Btw.: The raw implementation took 2h of my life, till I figured out that a right shift operation on byte-value 0b1000_0000 is not 0b0100_0000 as expected. For some crazy reason it's 0b1100_0000.
People who won't spend time on solving java puzzles could take advantage of a Bits-class... 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c12) posted by **wasserman.louis** on 2011-10-16 at 11:22 PM_

---

I would strongly prefer a somewhat less verbose implementation. I don't think builder-style syntax is the way to go here. I find bit-twiddling much more readable when important intermediate values can get their own variable with their own name.

Here are some methods I would be potentially in favor of offering, for each numeric type X:

X shiftLeft(X x, int bits)
&nbsp;&nbsp;&nbsp;// (x << bits) just ignores all but the last X.SIZE bits, which can be unexpected.
&nbsp;&nbsp;&nbsp;// shiftLeft(X, bits) would behave more predictably.
X shiftRightLogical(int x, int bits)
&nbsp;&nbsp;&nbsp;// Not only does this share the advantages of shiftLeft over <<, but it
&nbsp;&nbsp;&nbsp;// disambiguates >> and >>>.
X shfitRightArithmetic(int x, int bits)
X setBit(X x, int bit)
X clearBit(X x, int bit)
X flipBit(X x, int bit)
X setBit(X x, int bit, boolean value)
boolean testBit(X x, int bit)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c13) posted by **wasserman.louis** on 2011-10-28 at 04:30 PM_

---

I don't think this is an adequately common use case to really merit the number of methods that it would entail.

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=739#c14) posted by **kevinb@google.com** on 2011-11-15 at 10:30 PM_

---

I am also highly skeptical; I think that most users who would use this should really be using an even higher abstraction.

---

**Status:** `WontFix`
**Labels:** `Type-Enhancement`
 
CharStreams.copy(Readable from, Appendable to) hangs _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1061) created by **christoph.henkelmann** on 2012-07-11 at 12:51 PM_ --- When using a Reader that does not always fill the available buffer space completely on each read, the CharStreams.copy method can get stuck in an endless loop. The reason is that the buffer does not get cleared after a call to CharBuffer.flip(). If the buffer was not completely filled, this causes the limit of the buffer to stay at the position to which was last written. That way the buffer limit approaches 0, which causes an endless loop, as no chars are read but the end of the reader is never reached. Even if the limit does not reach zero, the performance may degrade significantly, as the buffer size gets smaller and smaller. I have attached an example file with a main method that demonstrates the behavior and a suggested fix.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1061#c1) posted by **christoph.henkelmann** on 2012-07-11 at 01:00 PM_

---

Tested against guava 12.0
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1061#c2) posted by **cgdecker@google.com** on 2012-07-11 at 07:02 PM_

---

Thanks for the report.

---

**Status:** `Accepted`
**Labels:** `Type-Defect`, `Package-IO`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1061#c3) posted by **cgdecker@google.com** on 2012-07-11 at 08:37 PM_

---

A fix for this has been submitted internally and should appear in Guava soon.

---

**Status:** `Fixed`
**Labels:** `Milestone-Release13`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1061#c4) posted by **wasserman.louis** on 2012-07-14 at 12:35 PM_

---

Fix mirrored out at https://github.com/google/guava/commit/314727b81a4f45210ab53ebd46b695150ea36f8c .
 
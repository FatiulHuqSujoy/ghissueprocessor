Interners.asFunction(Interner) _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=398) created by **ray.j.greenwell** on 2010-08-11 at 11:43 PM_ --- EOM  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=398#c1) posted by **neveue** on 2010-08-16 at 09:09 AM_

---

I could see this as being useful.

We could also have:

public interface Interner&lt;E> implements Function&lt;E, E>

Though I'm not sure whether we want com.google.common.collect to depend on com.google.common.base, or the reverse.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=398#c2) posted by **kevinb@google.com** on 2010-09-08 at 06:37 PM_

---

Collect depends on base; that direction is fine, just not the other.

I don't like public types to implement Function, because I think an interner should have one method, intern(). Not intern() and apply(). And as just covered, Functions.forInterner() is not possible, but Interners.asFunction() is.

---

**Status:** `Accepted`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=398#c3) posted by **neveue** on 2010-09-11 at 08:37 AM_

---

> I don't like public types to implement Function, because I think an interner should have one method, intern(). Not intern() and apply().

I totally agree. I made a mistake.
I checked the intern() signature before making my suggestion, making sure that the argument and return types matched Function.apply() (especially considering generics), but I missed the obvious _name_ difference...

And, as you said, adding an apply() method just to obey the Function&lt;T> contract would be bad design. Renaming the intern() method would also be wrong: we should not sacrifice clear method names for the sake of implementing the Function&lt;T> interface.

I think that an adapter such as Interners.asFunction(Interner) is the way to go.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=398#c4) posted by **boppenheim@google.com** on 2010-09-22 at 11:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Started`
**Owner:** boppenh...@google.com
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=398#c5) posted by **boppenheim@google.com** on 2010-09-25 at 12:20 AM_

---

Committed in r132.

---

**Status:** `Fixed`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=398#c6) posted by **kevinb@google.com** on 2011-01-18 at 08:38 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Release08`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=398#c7) posted by **kevinb@google.com** on 2011-01-24 at 09:33 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 
Document ImmutableMap's thread safety guarantees _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=256) created by **jheitmann** on 2009-10-09 at 05:33 PM_ --- It looks like ImmutableMap guarantees that its entries are fully visible across threads once it's constructed (since Entry objects are stored in final collections that are fully populated in the constructor). It would be nice if this was documented. I'm guessing the other immutable classes are as safe and could benefit from something like the following in their javadoc: Entries of ImmutableXXX and their references are guaranteed to have visibility from other threads at least as up-to-date as they were at the point of construction of the ImmutableXXX.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c1) posted by **tpeierls** on 2009-10-09 at 06:04 PM_

---

I agree that it's worth emphasizing the advantages of immutability, and I think it 
could be done evem more simply by saying, "Instances of immutable types may be safely 
accessed from any thread without additional synchronization."
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c2) posted by **kevinb9n** on 2009-10-16 at 09:07 PM_

---

I like it.

---

**Status:** `Accepted`
**Labels:** `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c3) posted by **kevinb@google.com** on 2010-02-12 at 05:03 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Docs`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c4) posted by **kevinb@google.com** on 2010-07-30 at 03:50 AM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Release07`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c5) posted by **kevinb@google.com** on 2010-08-25 at 09:21 PM_

---

We are using _some_ annotations from JSR-305 already, and I wonder if simply adopting @﻿Immutable here would be enough, since Tim's sentence really deserves to be explained there.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c6) posted by **kevinb@google.com** on 2010-09-14 at 04:58 PM_

---

A problem with "Instances of immutable types may be safely accessed from any thread without additional synchronization" is that if I'm not really on the ball I might think this magic wand covers me even when one thread mutates an _element_ inside the immutable collection.

---

**Labels:** -`Milestone-Release07`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c7) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c8) posted by **fry@google.com** on 2011-12-10 at 03:41 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c9) posted by **wasserman.louis** on 2012-01-16 at 06:59 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Docs`, `Type-Documentation`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c10) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c11) posted by **kevinb@google.com** on 2012-05-30 at 07:51 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Documentation`, `Type-ApiDocs`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=256#c12) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 @kevinb9n recently re-wrote a large portion of the ImmutableCollection javadocs...hopefully that adequately addresses this issue.
 
Iterables.removeFirst(Iterable, Predicate) _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=695) created by **dvoytenko** on 2011-08-23 at 10:02 PM_ --- Few additional utilities could be considered for current Collections2 class, which would follow other similar classes in Guava: - boolean isNullOrEmpty(Collection) (there are similar methods in Objects and Strings classes). Would return, of course, true if collection is NULL or empty. - T findFirst(Collection, Predicate) - void removeFirst(Collection, Predicate) - void removeAll(Collection, Predicate)  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c1) posted by **cgdecker** on 2011-08-24 at 04:14 AM_

---

Most of these are handled by Iterables since all Collections are Iterables.
- findFirst = Iterables.find
- removeAll = Iterables.removeIf
- removeFirst doesn't seem to me like something there would be a common enough need for.
- isNullOrEmpty for Collections isn't likely to be added. See Kevin's comments here: http://stackoverflow.com/questions/6910002/google-guava-isnullorempty-for-collections

---

**Status:** `WorkingAsIntended`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c2) posted by **sirgeralt** on 2011-09-02 at 07:11 PM_

---

Hello, 
I would like to vote for the removeFirst() method proposed (or, maybe, poll()?). It would be nice addition to the already superb set of functionalities of the Iterables class.

I had a case when such a method was needed (rather trival, but still - query db for a predefined list of records, iterate records and remove the conditions from the list, items left are not present in the db, handle them ;)) 

I assume the proposed removeFirst would also be faster, the using find several times on a full collection.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c3) posted by **neveue** on 2011-09-02 at 07:36 PM_

---

Why not simply combine predicates using Predicates.and() and Predicates.not() to filter the list once and for all?

Note that Iterables.filter() does not iterate over the collection: it creates a "filtered view". You can chain multiple calls to Iterables.filter() without incurring performance problems.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c4) posted by **kevinb9n** on 2011-09-03 at 04:13 AM_

---

_(No comment entered for this change.)_
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c5) posted by **kevinb9n** on 2011-09-03 at 04:14 AM_

---

Sirger..., I am not understanding how a removeFirst() method helps you in your example.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c6) posted by **sirgeralt** on 2011-09-05 at 09:17 AM_

---

nev... I dont want to just filter the list, but also match objects accordingly to a predicate. I am not quite sure how this can be achived by combined predicates. 

Example:

public class PollExpample {
&nbsp;&nbsp;&nbsp;&nbsp;public static void main(String[] args) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;List&lt;Source> sources = new ArrayList&lt;Source>();
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sources.add(new Source("a"));
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sources.add(new Source("b"));
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sources.add(new Source("c"));
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sources.add(new System.out.println(sources);
}
.

}
class Source {
&nbsp;&nbsp;&nbsp;&nbsp;String identifier;
&nbsp;&nbsp;&nbsp;&nbsp;public Source(String identifier) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super();
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;this.identifier = identifier;
&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;public String toString() {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return "S_" + identifier;
&nbsp;&nbsp;&nbsp;&nbsp;}
}
class Target {
&nbsp;&nbsp;&nbsp;&nbsp;String identifier;
&nbsp;&nbsp;&nbsp;&nbsp;public Target(String identifier) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super();
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;this.identifier = identifier;
&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;public String toString() {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return "T_" + identifier;
&nbsp;&nbsp;&nbsp;&nbsp;}
}

There is already a method Iterables.removeIf(removeFrom, predicate), but it does return boolean, not the removed object
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c8) posted by **neveue** on 2011-09-05 at 09:59 PM_

---

I think I better understand what you want to do. Instead of the removeFirst method suggested by dvoyte:

void removeFirst(Collection, Predicate))

you want a polling method that looks for the first element matching a given Predicate, removes it from the collection, and returns it:

public &lt;T> T poll(Collection&lt;T>, Predicate<? super T>)

I never needed such a method, but I think it makes sense. A straightforward implementation could }
}
.

Maybe I should replace Iterators.any() with Iterators.indexOf(), since the latter actually specifies the end-state of the iterator (Iterators.any() does not specify anything)...

All this being said, I wonder about the performance implications of "polling" a collection multiple times.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c9) posted by **stephan202** on 2011-09-06 at 09:08 AM_

---

The implementation of #poll above would advance the iterator up to and including the first element that matches the predicate. As such calling #next() once more returns the element _after_ the first match (if any... an exception may be thrown). Unfortunately Iterators#indexOf() is potentially quite expensive.

It seems to make more sense to simply implement #poll from scratch: it's basically a slight variation of Iterables#any. Perhaps a slightly more elegant solution would employ a PeekingIterator.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c10) posted by **sirgeralt** on 2011-09-06 at 09:51 AM_

---

nev...

Yes, the poll() method is exactly what I had in mind. I mistakenly assumed that the propsed removeFirst() does return T, not void (as find()) and did not check the proposal. Sorry for misleading you.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=695#c11) posted by **dvoytenko** on 2011-09-08 at 07:58 PM_

---

RE: isNullOrEmpty

While I agree that the notes in the post are legitimate, however, the same could be said about Strings' isNullOrEmpty and others. This request was aimed at making work with collections a bit more "similar" with many other languages such as JavaScript, XSLT, Groovy, etc.
 
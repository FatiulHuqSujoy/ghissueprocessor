NullPointerException at ComparisonChain.compare after upgrade from java 6 to java 7 _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1373) created by **m...@nimbustecnologia.com.br** on 2013-04-18 at 08:06 PM_ --- After upgrade from java 6 to java 7, the same code started to raise this exception when passing null objects to ComparisonChain: Caused by: java.lang.NullPointerException &nbsp;&nbsp;&nbsp;&nbsp;at com.google.common.collect.ComparisonChain$1.compare(ComparisonChain.java:72) Workround: Prevent from compare any null object using ComparisonChain.compare  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1373#c1) posted by **kurt.kluever** on 2013-04-18 at 08:14 PM_

---

_(No comment entered for this change.)_

---

**Owner:** cpovirk@google.com
**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1373#c2) posted by **cpovirk@google.com** on 2013-04-18 at 11:20 PM_

---

I wonder why there was no NPE before. Maybe there was a change in the implementation of the class whose instances you were comparing? What class of object were you comparing? Could you provide a test case to reproduce the problem? Thanks.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1373#c3) posted by **m...@nimbustecnologia.com.br** on 2013-04-19 at 01:03 AM_

---

I tried to create a unit test, but I was not able to reproduce it on test. Running with testng I get an NPE on both versions java, 6 and 7. 

When I run App Engine dev server, the same code works with java 6, but not with java 7. 

I'm comparing a date and an chain.result();
}
.

"this" and "that" is an Balance object that has a reference to a parent Key object. The parent Key sometimes is null.

I'm comparing an already persisted object with other not yet persisted, putting them on a TreeSet.

It was working normally with java 6. I just changed the compliance level to java 7, and started getting the NPE. If I set back to Java 6, it starts working again.

I found some similar problem here:
https://code.google.com/p/jclouds/issues/detail?id=1069

Thanks
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1373#c4) posted by **Maaartinus** on 2013-04-19 at 02:12 AM_

---

> The parent Key sometimes is null.

so you're doing something like `compare(null, aComparable)`, which on line 72 calls `null.compareTo(aComparable)`, which throws as it should. You can avoid it by using `compare(@Nullable T left, @Nullable T right, Comparator<T> comparator)`&nbsp;with a comparator like `Ordering.natural().nullsFirst()`.

There's a difference between Java 6 and 7 described under "Inserting an Invalid Element into a TreeMap Throws an NPE" in
http://www.oracle.com/technetwork/java/javase/compatibility-417013.html
which might explain the changed behavior. 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1373#c5) posted by **cpovirk@google.com** on 2013-04-19 at 02:15 PM_

---

I think comment #﻿4 has it. Thanks.

There was a bug in earlier JDKs' TreeSet implementations that allowed the first element inserted to have a broken compareTo() method. (As soon as a second element was inserted, it would throw the expected NPE.) JDK7 fixed this.

ComparisonChain is doing what I'd expect. If the goal is to accept null values, you'll need to tell it where to put them (beginning or end), as Maaartinus also described.

---

**Status:** `WorkingAsIntended`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1373#c6) posted by **m...@nimbustecnologia.com.br** on 2013-04-19 at 02:30 PM_

---

Perfect, thanks!
 
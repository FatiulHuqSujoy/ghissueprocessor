ImmutableTable.Builder builds array backed table for large sparse tables due to overflow error _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1322) created by **rma350** on 2013-03-07 at 12:19 AM_ --- When creating an ImmutableTable using a builder, the builder uses a heuristic to determine what data structure to use for storing data, as seen below from line 163 of RegularImmutableTable.java (in version 14.0): . If the data for the table is large and sparse, then rowSpace.size()*columnSpace.size() may overflow to a negative number, causing a DenseImmutableTable to be created instead of a sparse one. This in turn will use enough memory to crash the program on a large 2d array allocation. A minimal fix might be to change the logical condition to: cellList.size() > ((long)rowSpace.size()) \* columnSpace.size()/2 Attached is a short piece of code where a very sparse Table of 5,000,000 entries is created successfully, and then the ImmutableTable creating with the builder on the same data fails.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1322#c1) posted by **lowasser@google.com** on 2013-05-03 at 11:36 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Accepted`
**Owner:** lowasser@google.com
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1322#c2) posted by **kurt.kluever** on 2013-05-04 at 12:16 AM_

---

_(No comment entered for this change.)_

---

**Status:** `Fixed`
**Labels:** `Milestone-Release15`
 
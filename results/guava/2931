Add size method for immutable collection builders It's often necessary to know the size of the collection being built. For example, to special case if this is the first element, or if the collection is empty. The workarounds are ugly. This should be free for most builders since they already track the size. I only care about this for `List`, `Set`, and `Map`, so if it's fine if it can't be added for more exotic collections like `ListMultimap`.  I believe this has been done for `ImmutableList` [recently](https://github.com/google/guava/commit/7928bbe079303c54373453353c9ef2cd7de9365e) - so recently that it's not had the chance to make it into a release yet. AFAICT, sized builders have yet to be added for `ImmutableSet` and `ImmutableMap`. That doesn't expose the size, it just allows you to preconfigure the expected size.

What would you expect this to do for `ImmutableSet`? The number of elements after deduplication, or the number of elements before deduplication? Exposing this API imposes some constraints on the implementation of the builder. Oops, I think I posted too quickly for my own good!

If I understand what you're asking, you're asking for a method like `Immutable{List,Set,Map}.Builder#size` for querying the size of a builder, rather than builders which are pre-sized to a certain size. Have I understood you correctly? (@lowasser got there before me. :) ) @jbduncan yes, exactly. Apologies for not explaining more clearly.

@lowasser that's a great point about duplicates for `ImmutableSet`.

We could add a `size()` method for `ImmutableList`, `ImmutableMap`, and any others where it makes sense and is free. Additionally, we could add an `isEmpty()` method for most/all builders. Knowing if the collection is empty will cover a good number of use cases, though sometimes you do need the size (most often for `ImmutableList`). I think we generally recommend using a mutable type and then calling `copyOf`. ImmutableList.copyOf(builder);
.

Are you concerned about the overhead of copying the list? In the cases where people have asked for this internally, we've found that their code reads better if it separates the "add things to builder" and "read from resulting collection" steps, rather than intermixing the two, even in the small way you suggest here. I would imagine that there are exceptions, of course, but overall, I fear that people will use this to make code worse rather than better.

We've also found that people try to avoid calling `build()` on an empty builder. That turns out not to be necessary because `build()` is free in that case. 
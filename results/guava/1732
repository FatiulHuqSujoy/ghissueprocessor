Revist deprecation of Objects.equal _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1732) created by **ch...@digitalascent.com** on 2014-04-19 at 03:54 PM_ --- Commit 2b3b26449f7c recommends using JDK 7 Object.equals in preference to Guava's Objects.equal. This Google+ post is related: https://plus.google.com/118010414872916542489/posts/TtFEKhU8qnG. java.util.Objects.equals cannot be statically imported, due to Java language spec limitations on static imports combined with a poorly chosen name of 'equals' (it's obviously already used, causing a conflict for static imports). Without the static import, code size for this construct will increase by 180% (Guava equal - 5 characters; j.u.Objects.equals - 14 characters -> 9 extra characters). These extra characters are uninteresting, adding no semantic or syntactical value. An argument could be made to 'roll your own' (the code is trivial), however that has a hidden downside: many code analysis tools recognize Guava's Objects.equals and java.util.Objects.equals to be equivalent to x.equals(y) for the purposes of validating the correctness of the code. For example: Integer integer = 5; String string = "guava"; integer.equals(string); // caught as a likely bug com.google.common.base.Objects.equal(integer,string); // caught as a likely bug java.util.Objects.equals(integer,string); // caught as a likely bug my.package.Objects.equals(integer,string); // not flagged as likely bug Proposing that Objects.equal remain for reasons of code brevity/cleanliness, broad adoption (including by code analysis tools) and inertia.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c1) posted by **kak@google.com** on 2014-04-20 at 05:00 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Base`, `Type-Other`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c2) posted by **kevinb@google.com** on 2014-04-21 at 06:32 PM_

---

(Note that Guava's Objects.equal() is not deprecated, just discouraged.)

In general, I don't think that "exactly like a JDK method but can be static-imported" is enough justification for a Guava method to exist. When it _eventually_ is deprecated, the best thing to do is just grumble about the JDK mistake and write out the "Objects.equal()".
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c3) posted by **ch...@digitalascent.com** on 2014-04-21 at 06:44 PM_

---

I'd agree if this was a _new_ method - it wouldn't be justifiable to _add_ it. 

However, the bar should be different for discouraging/deprecating established methods.

Guava's Objects.equal(), while (now) providing the same functionality as the JDK, is statically importable, which is valuable in reducing the noise in a code base (especially given the high frequency of use for this specific method).

Having to read ~2x the code for no additional value is a step backward. Not concerned about typing it out, as IDE templates address this regardless of length - we spend considerably more time _reading/reviewing_ code than writing it.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c4) posted by **kevinb@google.com** on 2014-04-21 at 08:51 PM_

---

Applying a markedly different standard for whether something should be _added_ compared to whether something should be _kept_ is a recipe for accumulating a lot of cruft over the years. It isn't the Guava way.

As well, having two different library methods in existence that do the exact same thing is not a good thing. At best it leads to wasted time in fruitless debates between developers over minor concerns like static-importability. This also isn't the Guava way. We want to add real value on top of JDK libraries, and be seen as adding real value on top of JDK libraries, not as simply creating our own dialect.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c5) posted by **ch...@digitalascent.com** on 2014-04-21 at 09:11 PM_

---

Code readability/brevity add real value on top of the JDK.

With Guava - 213 characters:
return equal( id, bean.id ) && equal( data, bean.data ) && equal( isActive, bean.isActive ) && equal( searchSynonym, bean.searchSynonym ) && equal( sequence, bean.sequence ) && equal( userAdded, bean.userAdded );

With j.u.Objects - 261 characters:
return Object.equals( id, bean.id ) && Object.equals( data, bean.data ) && Object.equals( isActive, bean.isActive ) && Object.equals( searchSynonym, bean.searchSynonym ) && Object.equals( sequence, bean.sequence ) && Object.equals( userAdded, bean.userAdded );

That's a ~22% increase in code size; the added code ('Objects.') is a distraction from reading/comprehending the executable logic.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c6) posted by **kevinb@google.com** on 2014-04-21 at 09:25 PM_

---

Classes where all fields are nullable are rare (and _should_ be even more rare than they are) yet you've zeroed in on that rare case for your example.

Moreover, you're talking about snipping mere _dozens_ of characters from hand-written value-type classes -- which are _enormous_ sources of mind-numbing boilerplate in Java. Why be content with such small savings; why not save way more with AutoValue (https://github.com/google/auto/tree/master/value), or something else like it?

We've already adequately explained that this character savings is NOT enough justification for us to change our plans for this method.

---

**Status:** `WorkingAsIntended`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c7) posted by **Maaartinus** on 2014-04-23 at 12:37 AM_

---

> Classes where all fields are nullable are rare (and _should_ be even more rare than they are) yet you've zeroed in on that rare case for your example.

Kevin, do you suggest to use equal for nullable fields only, i.e., to write something like

return equal(id, bean.id) && data.equals(bean.data) && equal(isActive, bean.isActive) && searchSynonym.equals(bean.searchSynonym) && ...

if some field were nullable and some not?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c8) posted by **chrispurcell@google.com** on 2014-08-12 at 09:55 PM_

---

I think "snipping mere _dozens_ of characters from hand-written value-type classes" mischaracterizes how common comparing two objects is in Java. I find 17.5K instances of the new "Objects.equals" method in Google code, and over 2 million calls to "Object.equals". Every time I see the latter, I have to pause for a moment to consider nullness. Objects.equal is trivially safe, and more readable.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c9) posted by **kevinb@google.com** on 2014-08-12 at 10:17 PM_

---

The purpose of the "mere dozens" comment is that, if the goal is saving code, there's orders of magnitude more to be gained from AutoValue. I don't see how your response addresses that. You say (correctly of course) that this is extremely common, which makes me think "all the more value in moving to AutoValue".

What other methods in Guava do _exactly_ what a JDK method does at only a fixed savings of characters?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c10) posted by **kevinb@google.com** on 2014-08-12 at 10:19 PM_

---

@#﻿7 it's what I would do, but I'm not going around proselytizing it.

I get that the inconsistency sucks, but the whole thing sucks anyway and should be generated. :-)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c11) posted by **chrispurcell@google.com** on 2014-08-14 at 11:00 AM_

---

Why do you think AutoValue will solve this? I don't see any evidence that data types are the common usage of equals.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1732#c12) posted by **kevinb@google.com** on 2014-08-16 at 12:05 AM_

---

I've quickly reviewed a pseudorandom sample of 40 calls to this method (both Guava and JDK forms) in the Google codebase. 33 of them were in implementations of Object.equals(). Not every one of those classes can necessarily be converted to AutoValue, but I still maintain that the benefits of AutoValue make this character-count concern articulated in this bug look like small potatoes.

Whenever we include a method in Guava that does the exact same thing as a JDK method, we are introducing a dialect. There are real costs to introducing a dialect even when we believe our dialect is superior.

We see the benefits of "equal" over "Objects.equals". You'd have to be blind not to see which one is less obtrusive in your code. We simply do not judge those benefits to be massive enough to justify the "dialecting".

At this point we feel we've explained our reasoning as well as we can. You are totally within your rights to disagree and believe we are horribly wrong. If so, continued conversation is not going to help either of us.
 
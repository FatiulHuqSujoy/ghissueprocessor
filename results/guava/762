request for clone() _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=762) created by **kpietrza...@rajant.com** on 2011-10-17 at 12:03 PM_ --- I would love to see: &nbsp;\* the Immutable(Set|Map|etc.) classes implement Cloneable &nbsp;\* ...and implement clone() I don't see any downsides right now, but I'm sure I'm missing something. If the powers that be don't say no, I'll submit a patch. P.S. The primary motivator for this is Eclipselink: it uses clone() deep inside its own codebase, so currently I avoid using Guava with my entities, which is a darn shame.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c1) posted by **wasserman.louis** on 2011-10-18 at 11:18 PM_

---

I'm not positive, but I suspect we don't have this because we consider clone to be flawed and evil...and we don't want to encourage its use?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c2) posted by **raymond.rishty** on 2011-10-19 at 12:35 PM_

---

I can't think of a single good reason to clone an immutable. If Eclipselink is forcing you to do it, Eclipselink is probably wrong. Definitely a case of "two wrongs don't make a right"
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c3) posted by **finnw1** on 2011-10-19 at 02:02 PM_

---

In a final, immutable class is there any harm in implementing clone() as "return this;"?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c4) posted by **ogregoire** on 2011-10-19 at 03:57 PM_

---

Yes, it breaks the clone specs. http://download.oracle.com/javase/6/docs/api/java/lang/Object.html#clone()
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c5) posted by **finnw1** on 2011-10-19 at 08:03 PM_

---

The spec also says "but these are not absolute requirements."
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c6) posted by **ogregoire** on 2011-10-19 at 10:55 PM_

---

Agreed. However, as a programmer, I expect that libraries like Guava are as much compliant to the specs as possible. For instance, Guava doesn't play with the specs and put 1,000 exceptions like Apache Commons does. So in the scope of consistency Guava tends to have, I'd find it extremely hard to understand that the Cloneable classes inside of Guava don't respect these non-mandatory requirements.

The clone() method is not just some method defined by some interface. It's present on the very mother of all classes itself.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c7) posted by **kevinb@google.com** on 2011-10-20 at 06:19 AM_

---

We gave up on cloneability on all our classes a few years ago, after realizing how dumb and broken the whole thing is.

---

**Status:** `WorkingAsIntended`
**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c8) posted by **kap4020** on 2011-10-20 at 02:41 PM_

---

@﻿kevinb: When you say you guys gave up on cloneability, do you mean cloneability in general or only via java.lang.Cloneable?

i.e., Is there a good way to do cloneability within the context of the way Java does generics, etc.?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=762#c9) posted by **kevinb@google.com** on 2011-10-21 at 08:35 PM_

---

There is no good way to do implementation-specific cloning; there's just "copy constructor"-style APIs, such as ArrayListMultimap.create(Multimap) or ImmutableSet.copyOf(Iterable).
 
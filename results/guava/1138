Feature request: RemovalNotification able to guarantee non-null key/value  _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1138) created by **brianfromoregon** on 2012-09-06 at 09:28 PM_ --- I'd like to write a cache that wraps a weak-valued Cache and may rescue an evicted entry when evicted. It would rescue it by storing a strong reference to the key/value in a secondary Map. I think this is not possible today. Have you considered allowing the user to request that in your ReferenceQueue callback you store off a strong ref to the object so that RemovalNotification never has a null? Your map would still behave as advertised, the entry would be evicted, the only side effect is that the once-weakly/softly reachable object is now strongly reachable again when the user's RemovalListener is invoked.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1138#c1) posted by **wasserman.louis** on 2012-09-06 at 09:42 PM_

---

I don't think this is an even possibly doable thing.

Once a reference gets into a ReferenceQueue, there are no more references to the referenced object in the entire VM, by definition. If there's a strong reference anywhere in the VM, it didn't get GC'd. You can't "intercept" an object from getting garbage-collected like that.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1138#c2) posted by **brianfromoregon** on 2012-09-06 at 09:56 PM_

---

Yeah ok that's right. Whereas from within your finalizer you can promote from weak to strongly reachable. Was confused, thanks.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1138#c3) posted by **wasserman.louis** on 2012-09-06 at 10:13 PM_

---

Yes...though that's one of the main reasons finalization is weird, unpredictable, and best avoided.

Marking as Invalid because it's not possible.

---

**Status:** `Invalid`
 
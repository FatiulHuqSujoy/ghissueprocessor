ImmutableMap.keySet concurrency bug _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=949) created by **reardencode** on 2012-03-26 at 11:25 PM_ --- It looks to me like the ImmutableSet.keySet implementation is vulnerable to the same memory consistency issue as double checked locking in its current state. I propose the fix of making keySet volatile (line 410) to create a happens-before which ensures that the new KeySet() is fully constructed before another caller of keySet can read it.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c1) posted by **wasserman.louis** on 2012-03-26 at 11:28 PM_

---

Have you actually demonstrated this in practice, or does it just look like it?

It uses the racy single-check idiom, which basically means that two threads _might_ get different KeySet objects (but we don't care), without adding the overhead of a volatile field lookup. It's discussed in Effective Java item 71, and used e.g. to implement String.hashCode() -- which is certainly thread-safe.

Is there some reason you don't think that works here?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c2) posted by **wasserman.louis** on 2012-03-26 at 11:34 PM_

---

See e.g. Jeremy Manson's blog post at http://jeremymanson.blogspot.com/2008/12/benign-data-races-in-java.html, which discusses it in more detail, and mentions that object references are safe to manipulate using this idiom if we don't mind the possibility that some threads might get different KeySets.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c3) posted by **reardencode** on 2012-03-26 at 11:38 PM_

---

That's all fine for things like _integers_.

The problem with Double Check Locking that makes it require the volatile is that the pointer may be fully assigned, while the object it points to is still running its constructor.

The keySet variable must be volatile to avoid this.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c4) posted by **reardencode** on 2012-03-26 at 11:39 PM_

---

As the blog you linked points out, it the idiom isn't even safe if the value being data-raced is a 64bit integer, let alone a pointer to a complex object.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c5) posted by **reardencode** on 2012-03-26 at 11:41 PM_

---

http://stackoverflow.com/questions/3578604/how-to-solve-the-double-checked-locking-is-broken-declaration-in-java

As you'll see here, the idiom suggested by effective java uses volatile.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c6) posted by **wasserman.louis** on 2012-03-27 at 12:16 AM_

---

You didn't read the blog post fully. "Java has a special guarantee for fields that are 32-bit or fewer ***_and object references**_\* — a thread will always see some value that was assigned to that field." (Emphasis, obviously, added.)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c7) posted by **wasserman.louis** on 2012-03-27 at 12:17 AM_

---

The pointer isn't assigned until the createKeySet method call returns and the constructor has already completed. It can't be "still running its constructor" at that time.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c8) posted by **reardencode** on 2012-03-27 at 12:36 AM_

---

Your assumption about method call returning sequence is not a valid way to prove the correctness of the implementation. createKeySet can be assumed to be inlined, resulting in the exact same call pattern which would cause problems for double checked locking.

That said, you are correct that this particular situation is safe because of the immutability of KeySet. As I was reading the code, I was considering KeySet to be an arbitrary object as opposed to only ever being an immutable object.

If KeySet were any object with any non-final fields this would be an unsafe idiom.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c9) posted by **wasserman.louis** on 2012-03-27 at 12:39 AM_

---

I'll buy your claim for it being an unsafe idiom if KeySet was mutable, but immutability isn't the same thing as having no non-final fields. KeySet.asList, for example, is a non-final field -- lazily initialized, by the way, with the same racy single-check idiom. But yes, this all works because everything is immutable.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c10) posted by **reardencode** on 2012-03-27 at 12:42 AM_

---

True enough.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c11) posted by **wasserman.louis** on 2012-03-27 at 12:46 AM_

---

_(No comment entered for this change.)_

---

**Status:** `Invalid`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c12) posted by **wasserman.louis** on 2012-03-27 at 12:46 AM_

---

_(No comment entered for this change.)_

---

**Status:** `Invalid`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=949#c13) posted by **reardencode** on 2012-03-27 at 12:47 AM_

---

MIght want to put a one-line comment "// immutability of KeySet makes this safe"
 
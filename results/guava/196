Allow pre-sizing for appropriate ImmutableCollection builders _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=196) created by **lindner+unus...@inuus.com** on 2009-06-23 at 01:15 AM_ --- It might be nice to initialize the temporary arraylist used for Immutable classes to a specific size by adding an optional constructor that takes sizing parameters.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c1) posted by **kevinb9n** on 2009-08-12 at 06:13 PM_

---

Seems plausible to me, we'll see how it works out.

---

**Status:** `Accepted`
**Labels:** `post-1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c2) posted by **kevinb9n** on 2009-09-17 at 05:45 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`post-1.0`, `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c3) posted by **kevinb9n** on 2009-09-17 at 05:57 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c4) posted by **kevinb@google.com** on 2010-07-30 at 03:53 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c5) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c6) posted by **wasserman.louis** on 2011-10-20 at 08:57 PM_

---

+1 for something like builder.ensureCapacity, maybe.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c7) posted by **fry@google.com** on 2011-12-10 at 03:39 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c8) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c9) posted by **opinali@google.com** on 2012-04-26 at 07:31 PM_

---

Lots of input in the list, before the original poster, me and others, knew about this issue: https://groups.google.com/forum/?fromgroups#!topic/guava-discuss/2rZDhvsOXss
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c10) posted by **wasserman.louis** on 2012-04-26 at 07:34 PM_

---

I...think I'd be +1 on this change, still -- either with an ensureCapacity method, a capacity-accepting builder() variant, or both.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c11) posted by **tpeierls** on 2012-04-26 at 08:14 PM_

---

Prefer ensureCapacity style over mysterious int argument.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c12) posted by **opinali@google.com** on 2012-04-26 at 08:15 PM_

---

Concrete proposal: at minimum, we need a new public constructor for all Immutable*.Builder classes, with a parameter "int initialCapacity". Ideally we could also have an overload of the builder() factory with the same parameter, but only the constructor is strictly necessary. In any case, the builder's constructor will intialize its backing store with the precise initialCapacity provided.

This change alone will make sure that no reallocation/growing/copying costs will be incurred as elements are added. It's worth notice that this is useful not only when the exact element count is known beforehand; it's very useful too when a reasonable guess can be made, so in the worst case, costs of storage growth will be reduced.

It's also possible to make other optimizations to reduce buffer copying; the attached code illustrates implementation including a simple benchmark. Running the benchmark (java -server -D64 -Xms256m -Xmx256m) results, in my system, in a score of 76ms for the standard code and 41ms for the code with all suggested optimizations, almost twice as fast, with no performance tradeoffs. Actually the major tradeoff is in the code, as I'm using raw arrays as backing store instead of relying on ArrayList; but the builder's needs are very simple so the extra code and implementation complexity are minimal. This new code will be slightly faster even for the non-optimal case where preallocation and copy avoidance optimizations are not effective.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c13) posted by **wasserman.louis** on 2012-04-26 at 09:42 PM_

---

We have much more sophisticated internal benchmarks using Caliper. I'll do some experiments.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c14) posted by **wasserman.louis** on 2012-04-27 at 01:50 AM_

---

I sent in a code review at http://codereview.appspot.com/6092044/. This change doesn't actually expose any of the added features -- not until we come to some agreement about which methods should be exposed and how -- but there were also some ways to improve on opinali's implementation -- which didn't pass unit tests, by the way.

I'm slightly concerned that some of the builder implementations can set internal capacity, and some cannot. Exposing control over the capacities seems a bit like exposing an implementation detail -- one that might even change in the future. =/ That makes me...a bit wary, but not too much.

But let me summarize our options, as far as I can see. We could choose any subset of the following.
1. Expose a new Builder(int) constructor directly. I'm not a fan of this option -- we prefer factory methods for a reason (most notably, they can have useful names). (Indeed, in retrospect I wish that the Builder constructor weren't exposed at all.)
2. Expose a new factory method for Builders that sets the initial capacity. This could have a clarifying name like "builderWithCapacity(int)" or "builderWithExpectedSize(int)".
3. Expose an "ensureCapacity" method from the Builder objects. This option could reasonably become a no-op for builders that don't have that implementation detail. Disadvantage: you can't shrink builders beyond the default initial size, and this seems like a much rarer use case -- usually the user either knows the size at construction time, or doesn't know it until build() gets called.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c15) posted by **stephan202** on 2012-04-27 at 06:05 AM_

---

Louis, to me option 2 clearly jumps out as "better than the other two". 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c16) posted by **wasserman.louis** on 2012-04-27 at 06:08 AM_

---

I think I agree. Any alternative suggestions on the name, though? I'd prefer not to just expose builder(int), which really doesn't make the meaning of its argument clear. As Tim said, a "mysterious int argument" is rather suboptimal.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c17) posted by **neveue** on 2012-04-27 at 07:44 AM_

---

I like the names you suggested ("builderWithCapacity(int)" and "builderWithExpectedSize(int)"), because they are similar to "Lists.newArrayListWithCapacity(int)" and "Sets.newHashSetWithExpectedSize(int)". Similarity is good when naming things.

The names are long, but it doesn't bother me. Most people will use the default "builder()" method. Having a longer and precise name is good for the few cases where users need to specify the capacity.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c18) posted by **kevinb@google.com** on 2012-05-30 at 07:41 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Enhancement-Temp`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c19) posted by **kevinb@google.com** on 2012-05-30 at 07:45 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement-Temp`, `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c20) posted by **cpovirk@google.com** on 2012-06-21 at 09:11 PM_

---

We finally discussed this in our API-review meeting. We're skeptical that the performance improvement would be enough to justify the API complexity, especially when the new API's mere existence suggests to users that they ought to be pre-sizing their builders. Additionally, users can always create a pre-sized ArrayList themselves and then call copyOf(), reducing the cost to one arraycopy.

We're still considering how to optimize the Immutable\* construction paths, including the idea of never "expanding" a builder array, only creating additional arrays as necessary for the spillover. Somewhere along the line, we'll write some Caliper benchmarks to help guide us.

(We've already taken one optimization CL (&lt;https://github.com/google/guava/commit/03afbea918471a1b536c0754b10719adc0317135 and it's possible that even that was a step too far, but it turned up an interesting bug elsewhere in our code (&lt;https://github.com/google/guava/commit/26b9a4da08e4ee8002c527195121c2741fa43f29 so even benchmarks were to show that it weren't worthwhile, it will have been worth something :) (Also note that it didn't require an API change, though it did complicate the implementation a bit.))
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c21) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c22) posted by **kevinb@google.com** on 2012-12-20 at 06:45 PM_

---

Good news, everyone. We've convinced ourselves these methods should be added. We apologize, of course, that we couldn't make this decision a higher priority a while ago.

Our _unordered_ top preferences are:

a) ImmutableList.builderWithInitialSize(42)
b) ImmutableList.builder(42)
c) ImmutableList.builder(42) _and_ new ImmutableList.Builder&lt;Foo>(42)

Your thoughts?

---

**Status:** `Accepted`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c23) posted by **kevinb@google.com** on 2012-12-20 at 06:46 PM_

---

Also consider whether your preference changes if the parameter passed in is a variable, or a call to someCollection.size(), etc., rather than an int literal.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c24) posted by **neveue** on 2013-01-06 at 05:36 PM_

---

I prefer a) because it's clearer, and similar to "Lists.newArrayListWithCapacity()".

What about "ImmutableList.builderWithCapacity()"?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c25) posted by **Maaartinus** on 2013-01-06 at 09:21 PM_

---

I wonder if using a bigger growth factor instead of 1.5 for `ImmutableList.Builder`&nbsp;would be a good idea. Unlike with `ArrayList`, the needlessly allocated memory gets usually freed (eligible for GC) immediately, so the main reason for using a small growth factor doesn't apply here.

According to the `ImmutableListCreationBenchmark`, replacing the growth factor of 1.5 by 4 is a clear win:

. size benchmark ns linear runtime
. 10 BuilderAddFastGrow 66.3 =
. 10 BuilderAdd 91.0 =
. 10 PreSizedBuilderAdd 38.2 =
. 10 CopyArrayList 55.7 =
. 10 CopyPreSizedArrayList 65.5 =
. 1000 BuilderAddFastGrow 3701.5 =
. 1000 BuilderAdd 5091.7 =
. 1000 PreSizedBuilderAdd 2294.7 =
. 1000 CopyArrayList 5289.3 =
. 1000 CopyPreSizedArrayList 3775.2 =
. 1000000 BuilderAddFastGrow 4149068.4 ==================
. 1000000 BuilderAdd 5519544.3 ========================
. 1000000 PreSizedBuilderAdd 2568223.4 ===========
. 1000000 CopyArrayList 6836726.6 ==============================
. 1000000 CopyPreSizedArrayList 4610333.8 ====================

I also wonder what a public `Builder`&nbsp;constructor is good for when the class is final and a static factory method is provided?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c26) posted by **wasserman.louis** on 2013-01-07 at 12:30 AM_

---

A clear win for CPU time, sure, but not all users can tolerate high GC churn.

Opinions vary over whether the public Builder constructor is preferable when type inference cannot determine the type it should have -- e.g. new Builder&lt;Foo>() when ImmutableList.builder() can't infer the proper type -- but at this point, it's part of a non-Beta API, so we don't have much choice about it.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c27) posted by **Maaartinus** on 2013-01-07 at 08:26 PM_

---

The CG churn is just one more reason _against_ using such a small growth factor. Using it leads to many allocated chunks, and their total size is _big_. The exact figures depend on the target size, but out of the growth factors of `{1.5, 2, 3, 4}`&nbsp;the smallest one is nearly always the _worst_ choice in this respect!

This small program outputs the total memory allocated in the builder when lists of all sizes from 1 to `10**N`&nbsp;get created:
https://dl.dropbox.com/u/4971686/published/maaartin/guava/collect/ImmutableListBuilderMemoryAllocatedMeasurer.java

Such totaling (or averaging) is necessary so that certain factors don't get favored accidentally like e.g. a size of 40 favors the factor 3 (initialSize=4 -> 4_3+1=13 -> 13_3+1=40).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c28) posted by **neveue** on 2013-01-17 at 10:08 AM_

---

> Opinions vary over whether the public Builder constructor is preferable when type
> inference cannot determine the type it should have -- e.g. new Builder&lt;Foo>() when
> ImmutableList.builder() can't infer the proper type -- but at this point, it's part
> of a non-Beta API, so we don't have much choice about it.

Couldn't users write ImmutableList.&lt;Foo>builder()? In which case the public constructor becomes redundant, and we don't have to provide a public constructor for pre-sized builders.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c29) posted by **kevinb@google.com** on 2013-01-17 at 03:56 PM_

---

They _can_, but as noted, "opinions vary" over whether that's horrible. It certainly wasn't what we intended people to do when we created the method (we assumed it was only for the case that type inference would work).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c30) posted by **Maaartinus** on 2013-01-31 at 01:14 AM_

---

Concerning the builder syntax, there are two equally bad possibilities:

ImmutableList&lt;Number> list1 = new ImmutableList.Builder&lt;Number>().add(1).add(2.3).build();
ImmutableList&lt;Number> list2 = ImmutableList.&lt;Number>builder().add(1).add(2.3).build();

In theory, the former allows to use the diamond operator, but it doesn't work either. Allowing both syntaxes is the _third_ possibility, IMHO worse as it leads to mixed syntax. Maybe you could deprecate one of them, maybe _advising_ against it would be enough.

Concerning future Java progress I don't expect the inference to work in any of the cases before 2050, so it doesn't matter which one you choose.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c31) posted by **drichter@google.com** on 2013-06-13 at 09:39 PM_

---

Java7 syntax improves this.

ImmutableList&lt;Number> list1 = new ImmutableList.Builder<>().add(1).add(2.3).build();
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c32) posted by **cgdecker** on 2013-06-13 at 09:57 PM_

---

@﻿drichter: Actually, that won't compile unfortunately, for the same reason that

ImmutableList&lt;Number> list = ImmutableList.builder().add(1).add(2.3).build();

won't compile. Java can't intefer the the type returned by ImmutableList.builder() or new ImmutableList.Builder<>() without it being assigned to something.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c33) posted by **paulduffin@google.com** on 2013-06-27 at 09:36 AM_

---

Mixing Number and literal numbers is not handled well by Java at all. It should be able to infer the types of the following, and actually does a very good job but in fact it is too good as it infers the type of ImmutableList.of as ImmutableList&lt;Number & Comparable> which is not assignable to ImmutableList&lt;Number>:
&nbsp;&nbsp;&nbsp;&nbsp;ImmutableList&lt;Number> list = ImmutableList.of(1.1, 2.3);

Ignoring that I think that the use of .add() chain followed by .build() to construct a known size list is a bad way to do it. Better in my opinion to just use ImmutableList.of() - for most usages Java can infer the type correctly whereas it cannot for the .add() chain - plus of() is faster.
&nbsp;&nbsp;&nbsp;&nbsp;ImmutableList&lt;String> list = ImmutableList.of("1", "2"); // Fails
.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c34) posted by **cpovirk@google.com** on 2013-12-02 at 02:54 PM_

---

_Issue #1598 has been merged into this issue._
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c35) posted by **schlosna** on 2014-04-30 at 02:21 AM_

---

Is there any further input that would help get this change into the next release of Guava (18.0), be it code, additional API comments, etc.? I have several places building large collections that would benefit greatly from being able to specify the desired capacity when known a priori. This is especially true when dealing with serialization/deserialization.

Based on issue 1736, I would favor Immutable*.builderWithCapacity(int).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=196#c36) posted by **snekse** on 2014-04-30 at 11:00 PM_

---

I'm in favor of the general Immutable*.builderWithCapacity(int) format, but would the int be the actual capacity with resizing occurring before int elements have been added?
 A simple workaround is to use a mutable list to store intermediate results. ImmutableList.copyOf(temp);
.

I assume that this will result in better performance than using `ImmutableList.builder()` for the same operation.

The downside, of course, is that this doesn't provide fluent syntax.
 +1 We've been waiting for this for a while. Any movement?
 +1, any status on this? 
 +1 Excited to hear about an update!
 +1 Please put this in the next release.
 We have actually been sinking a fair amount of time into studying this.

At every turn we keep finding that the actual measurable benefits are significant in _fewer circumstances_ than we thought, and that those benefits when they do exist are _smaller_ than we thought.

We're not denying that those benefits exist. But the costs are also real; there IS an added burden that many thousands of users will take on once a tuning parameter like this were to exist - deciding whether to use it, discussing with a reviewer whether to use it, adding bulk to their code to use it, actually putting thought into how to come up with a "good guess" when the real value isn't known (note that is not the common case), etc. That may not seem like a big cost, but it's not free.

Our job before adding new APIs to Guava is to make damned sure the benefits will outweigh the costs, and we may actually get there. I guess I'm just explaining why this is not a no-brainer and why it is taking us some time.

Also, I'm sorry that it is now too late for 19.0, as rc1 is already out.
 I do realize that on 2012-12-20 I announced that we had decided in favor of this. I'm sorry that decision eroded. That's pretty annoying.
 Looks like some things landed in master, so maybe in Guava 24.0 release TBD?

- [`ImmutableList.builderWithExpectedSize`](https://github.com/google/guava/commit/7928bbe079303c54373453353c9ef2cd7de9365e): https://github.com/google/guava/commit/7928bbe079303c54373453353c9ef2cd7de9365e
- [`Immutable{Map,BiMap}.builderWithExpectedSize`](https://github.com/google/guava/commit/8158909d7bda9f98a4576f89eb5fd88cb12c2bd6): https://github.com/google/guava/commit/8158909d7bda9f98a4576f89eb5fd88cb12c2bd6
- [`ImmutableSet.builderForExpectedSize`](https://github.com/google/guava/commit/d7501abccbd666a8db4dff77fa110c801a094c75): https://github.com/google/guava/commit/d7501abccbd666a8db4dff77fa110c801a094c75 Phew, I had a minor heart attack when I saw those commit titles, but it turns out we did indeed use `builderWithExpectedSize()` consistently for all of the APIs, and did _not_ use `builder**For**ExpectedSize()` for `ImmutableSet`. 
Faster LocalCache recencyQueue _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1487) created by **Ben.Manes** on 2013-07-24 at 09:35 AM_ --- ## Background Guava's Cache uses a ConcurrentLinkedQueue per segment as a buffer of the recently accessed entries, drained when the lock is held. This avoids contention by amortizing the lock acquisition, rather than banging against it on every operation to update the policy state. A CLQ may suffer contention at the tail due to all threads spinning in a CAS loop to append their element. This is reduced by having one per segment, by default 4, though hot entries will hash to the same segment and accessing threads may contend. In ConcurrentLinkedHashMap this is reduced by having a global policy and best-effort simulating a CLQ per core, mapped by the thread id, and recombining in LRU order during the drain. Regardless, under simulated load CLQ does show up as a hotspot in a profiler. Two proposals are provided. (A) provides closer semantics to the current behavior. (B) provides the fastest alternative (prefered). ## Proposal (A) An elimination stack is useful in any scenario where messages need to be transferred quickly and FIFO ordering is not required, by allowing operations to cancel out. This is ideal in an object pool and fits the requirements for Guava's usage of the recencyQueue. The stack works by allowing operations to cancel out, which is linearizable. When contention is detected by a failed CAS on a treiber stack, a thread backs off to an arena, selects a location, and attempts to transfer the element to/from another thread. This reduces contention on the stack's top by spreading the CAS operations across isolated references. In benchmarking an object pool, I see significant performance gains under contention and similar single-threaded performance. Guava's cache only prefers fifo ordering, but does not require it. By not ensuring that constraint, the CLQ overhead could be reduced. An implementation is provided at, https://github.com/ben-manes/multiway-pool/blob/8c55b8e03f84d97df964caf82cae456197762a7a/src/main/java/com/github/benmanes/multiway/EliminationStack.java ## Proposal (B) In CLHM the buffers record both read and writes for replaying against the internal policy, whereas in Guava's cache only recording of reads is maintained. As a cache is best-effort bounding based on semi-intelligent logic, it may be argued that the recencyQueue could be lossy. This could be done by using a ring buffer (padded AtomicReference array), retaining an index cursor for the reader and writers, and use lazySet to null out consumed slots. A read could be lost or processed out-of-order, definitely so under load. This approach has a significant benefit of not creating garbage and minimizing contention. An implementation is trivial enough to not be provided, though easy to sketch out if desired.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c1) posted by **Ben.Manes** on 2013-07-29 at 01:58 AM_

---

I implemented (B) in CLHM by separating the read and write buffers. The result was a significant speedup as there is no more GC pressure, no CAS operations, and overall less contention.

In a synthetic test with 100% reads...

@ 4 threads
&nbsp;\- CacheBuilder: 14M/s
&nbsp;\- CLHM: 50M/s
&nbsp;\- CHM: 184M/s
&nbsp;\- CHMv8: 225M/s

@ 25 threads
&nbsp;\- CacheBuilder: 2-3M/s
&nbsp;\- CLHM: 95-109M/s
&nbsp;\- CHM: 220-240M/s
&nbsp;\- CHMv8: 318-330M/s

Because CLHM has a single LRU, the processor's cache invalidation on volatile fields has a noticeable impact. If there was a ProcessorLocal storage option the most egregious problems could be mitigated, and is currently simulated as much as possible using the thread's id.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c2) posted by **cgdecker@google.com** on 2013-07-29 at 01:54 PM_

---

_(No comment entered for this change.)_

---

**Owner:** lowasser@google.com
**CC:** fry@google.com
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c3) posted by **lowasser@google.com** on 2013-07-29 at 05:38 PM_

---

I would like to see an implementation sketch or reference implementation, please.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c4) posted by **Ben.Manes** on 2013-07-29 at 05:42 PM_

---

CLHM has typically been the reference with code & tests ported.

http://code.google.com/p/concurrentlinkedhashmap/source/browse/src/main/java/com/googlecode/concurrentlinkedhashmap/ConcurrentLinkedHashMap.java
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c5) posted by **Ben.Manes** on 2013-07-29 at 11:25 PM_

---

let me know if you need a trimmed down example if that's too much code to go trekking through.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c6) posted by **lowasser@google.com** on 2013-07-30 at 09:19 PM_

---

I'm pursuing B), but question: could it not result in memory leaks? Could an AtomicReference.lazySet result in an indefinitely held reference to an object?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c7) posted by **Ben.Manes** on 2013-07-30 at 10:54 PM_

---

Technically, but only if the cache is inactivity for the remainder of the application. This is already possible with the CLQ, though. The difference is that since the lazily set buffer is racy and lossy, a linear read until a null slot doesn't mean that the buffer is empty. If that was assumed by #cleanUp, an inactive cache may retain stale references even if cleanUp is called periodically. I'd have the explicit cleanUp calls walk the entire array instead. I wouldn't do that on the amortized calls, just the explicit one through the Cache interface where the extra guarantee / performance hit is expected.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c8) posted by **lowasser@google.com** on 2013-08-08 at 09:36 PM_

---

I've got an implementation. (FWIW, one alternative might be to do the ring buffer of WeakReference objects; that wouldn't create any more garbage than the CLQ, but would have other advantages.)

I'm not sure where your benchmarks live, or how to port them to Caliper?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c9) posted by **Ben.Manes** on 2013-08-08 at 10:32 PM_

---

I do have benchmarks, including Caliper. However Caliper doesn't handle multi-threading very well and this typically showed up on synthetic testing.
http://code.google.com/p/concurrentlinkedhashmap/source/browse/src/test/java/com/googlecode/concurrentlinkedhashmap/benchmark/

See the Profile class for a simple hook. I ran this independently for an ad hoc benchmark or through a profiler for allocation & cpu hotspots (Yourkit and JProfiler). Make sure to use -XX:+UseCondCardMark to avoid some JVM introduced contention.
http://code.google.com/p/concurrentlinkedhashmap/source/browse/src/test/java/com/googlecode/concurrentlinkedhashmap/Profile.java

Dominic's analysis tool is useful as an independent analysis,
https://github.com/tootedom/google-cache-builder-inv

WeakReference would require creating an object wrapper on every read. The removal of any heap allocation on a read was a nice feel good improvement, though minor, and WeakReferences have some GC implications. I decided against it as I didn't think it was necessary, but you're welcome to experiment and use them if you prefer to.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1487#c10) posted by **Ben.Manes** on 2013-08-15 at 09:27 AM_

---

One challenge, at least in CLHM, has been that minor changes can significantly impact the performance profile. The removal of some delay, e.g. a volatile read & write, will increase the rate of accessing the remaining volatile fields. This causes them to be more contested, leading to bus storms, that result is worse performance due to the lack of a backoff strategy. This makes it frustrating to tune at a micro-level.

In the past we've tried to hash to different shared queues to minimize threads colliding. The lack of processor-local is always frustrating because the shared state demolishes performance. 

It may be worth revisiting the idea of using thread-local queues, which are added as weak references in a shared registry. The victim thread that drains could iterate the registry to apply pending tasks and remove a dead thread's queue. These queues would still be ring buffers, to keep them fast and lightweight. This approach might create enough isolation to see a substantial gain, so its worth prototyping.

Unfortunately that approach deviates from what Guava's Cache is structured around. It may be considered incompatible as per-segment hashing for selecting the queue is fundamental, so mixing that with thread locals may not be worth it. The structure was chosen to optimize soft reference caching when that was the encouraged strategy (prior to having efficient alternatives) to remove the object overhead when decorating with ReferenceMap, etc.
 Please close this as the easiest solution would be to port the implementation from [Caffeine](https://github.com/ben-manes/caffeine), which doesn't appear to be worth the effort.
 
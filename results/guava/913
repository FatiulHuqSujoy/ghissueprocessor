Predicate based cache invalidate  _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=913) created by **mirko.teran** on 2012-02-29 at 06:54 PM_ --- Hi! Thanks for all your great work on Guava. Is there a chance you could add a method into Cache to invalidate keys/entries based on given Predicate&lt;Key>? I usually use compound objects as keys and it would be great if there was a way to remove all entries based on some criteria. I believe Predicate&lt;Key> suits this role perfectly. For now I've made a workaround to use asMap().keySet() but its just to crude.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=913#c1) posted by **fry@google.com** on 2012-02-29 at 06:58 PM_

---

That's exactly what our code would look like too. I don't see a problem with that. :-)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=913#c2) posted by **wasserman.louis** on 2012-02-29 at 07:07 PM_

---

cache.invalidateAll(Sets.filter(cache.asMap().keySet(), keyPredicate));

Seems perfectly good to me.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=913#c3) posted by **mirko.teran** on 2012-03-01 at 06:35 AM_

---

OK - I'll just keep my code as it is.

You can close/remove the issue.

Thank you!
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=913#c4) posted by **fry@google.com** on 2012-03-01 at 02:36 PM_

---

_(No comment entered for this change.)_

---

**Status:** `WorkingAsIntended`
 > cache.invalidateAll(Sets.filter(cache.asMap().keySet(), keyPredicate));

Just want to mention that this doesn't seem to be very thread safe.

The cache.asMap().keySet() will not return values that are currently being loaded and are not yet in the cache.
Invalidation and loading is usually done in 2 threads so it might happen that the loading thread loaded the some value. At the same time cache.asMap().keySet() did not return the newly-just-loaded value so it won't get properly invalidated. 
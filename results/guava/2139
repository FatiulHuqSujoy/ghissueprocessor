WeakOuter annotations can cause crashes in j2objc code Per https://github.com/google/j2objc/issues/521, the @WeakOuter annotations added in https://github.com/google/guava/commit/c62b07df3a68126e1cf549c85c35f4e55178990b can cause crashes in Objective-C translations of Guava (when the client possess a strong reference only to a view of a collection, and retains no strong references to collection itself). Since this hasn't made it to official v19 and is still in RC, should this be rolled back until it can be guaranteed correct? cc: @kstanger  Removing the @WeakOuter would result in a memory leak in ALL use cases. With the @WeakOuter, the code at least works correctly in MOST use cases. This is consistent with our current strategy for collection views and is not a regression from our J2ObjC fork of Guava.

We may need to revise our strategy for collection views, but until then, this is "working as intended".
 So a leak is considered worse than a crash?
 One strategy is not _better_ than the other. The point is, neither strategy is fully _correct_ and we had to chose one. I'm happy to reconsider this decision based on your feedback, but this will be done with very careful consideration when I have the time to focus on this problem.
 I'm not sure I follow, FWIW, why the collection's reference to the view
can't be a weak reference, and the view holding a strong reference? I know
very little about this other than that the goal is to avoid circular strong
references.

On Mon, Aug 24, 2015 at 11:37 AM Keith Stanger notifications@github.com
wrote:

> One strategy is not _better_ than the other. The point is, neither
> strategy is fully _correct_ and we had to chose one. I'm happy to
> reconsider this decision based on your feedback, but this will be done with
> very careful consideration when I have the time to focus on this problem.
> 
> —
> Reply to this email directly or view it on GitHub
> https://github.com/google/guava/issues/2139#issuecomment-134332661.
 That scenario would result in the collection's reference to the view being a dead pointer at some point in the future. Keep in mind we don't necessarily have ARC's automatically nil'ed __weak reference semantics, and if we did we'd still need to make the necessary modifications to check for nil.
 > That scenario would result in the collection's reference to the view
> being a dead pointer at some point in the future.

In most cases I believe we'd be okay with that -- it's supposed to be a
cache; it's okay if the view gets deleted.

On Mon, Aug 24, 2015 at 12:06 PM Keith Stanger notifications@github.com
wrote:

> That scenario would result in the collection's reference to the view being
> a dead pointer at some point in the future. Keep in mind we don't
> necessarily have ARC's automatically nil'ed __weak reference semantics, and
> if we did we'd still need to make the necessary modifications to check for
> nil.
> 
> —
> Reply to this email directly or view it on GitHub
> https://github.com/google/guava/issues/2139#issuecomment-134342830.
 It's not okay because the collection will continue to return the same pointer after it's deleted. It has no way to detect that the pointer has been deleted.
 Ahhh, got it.

On Mon, Aug 24, 2015 at 12:12 PM Keith Stanger notifications@github.com
wrote:

> It's not okay because the collection will continue to return the same
> pointer after it's deleted. It has no way to detect that the pointer has
> been deleted.
> 
> —
> Reply to this email directly or view it on GitHub
> https://github.com/google/guava/issues/2139#issuecomment-134344281.
 
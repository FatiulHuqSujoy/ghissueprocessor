Queues.drain takes longer than timeout for large Queues _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=794) created by **thomas.andreas.jung** on 2011-11-18 at 12:28 PM_ --- This bug is probably just a documentation issue. This test uses considerably more time to drain than the given timeout: @﻿Test public void drainIt() throws Exception{ &nbsp;&nbsp;&nbsp;&nbsp;int n = 1 << 21; &nbsp;&nbsp;&nbsp;&nbsp;BlockingQueue&lt;String> myQueue = Queues.newArrayBlockingQueue(n); &nbsp;&nbsp;&nbsp;&nbsp;myQueue.addAll(Collections.nCopies(n, "x")); &nbsp;&nbsp;&nbsp;&nbsp;long time = System.nanoTime(); &nbsp;&nbsp;&nbsp;&nbsp;long wait = TimeUnit.MILLISECONDS.toNanos(300); &nbsp;&nbsp;&nbsp;&nbsp;Queues.drain(myQueue, new ArrayList&lt;String>(), n + 1, wait, TimeUnit.NANOSECONDS); &nbsp;&nbsp;&nbsp;&nbsp;long diff = System.nanoTime() - time; &nbsp;&nbsp;&nbsp;&nbsp;assertTrue(diff + " " + wait, diff < 1.25 \* wait && diff > 0.75 \* wait); } The initial time used to drain before waiting for more is not taken into account. (The n necessary to see the problem depends on the speed of your machine. If the tests passes on your machine increase n.) Version: https://github.com/google/guava/commit/c6de7a5f6d4f990ce38abffe1c67d413bcaef8b8  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c1) posted by **andreou@google.com** on 2011-11-18 at 07:36 PM_

---

Thanks for the test, let me check if I can reproduce this
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c2) posted by **andreou@google.com** on 2011-11-18 at 08:06 PM_

---

Ok, the explanation is that we assume that the Queues#drainTo() invocation will be very fast (we just take existing elements), thus we don't have to check the time _again_ after the invocation, since it would be barely different. Thus in this test, after the copying, only then we wait for a full timeout. 

Is this something that has arisen in practice? (I'll look whether a simple code reordering could improve this)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c3) posted by **kurt.kluever** on 2011-11-18 at 08:39 PM_

---

Dimitris just submitted a fix internally, so I'll push the change out on Monday. Thanks for the report!

---

**Status:** `Fixed`
**Owner:** cpovirk@google.com
**Labels:** `Type-Performance`, `Milestone-Release11`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c4) posted by **andreou@google.com** on 2011-11-18 at 11:44 PM_

---

This was fixed internally, eventually it will be pushed, thanks again
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c5) posted by **cpovirk@google.com** on 2011-11-21 at 04:59 PM_

---

Someone observed internally that this will never really be "fixed" as long as we call queue.drainTo in our implementation. Dimitris has improved things, but perhaps we should document the small remaining bug?

---

**Status:** `New`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c6) posted by **jim.andreou** on 2011-11-22 at 07:47 AM_

---

Well, we do an O(n) between time reads, instead of the usual O(1). But even if we read the time between accessing _every_ element, we may still exceed the timeout. It's a feature of time, it's slippery :-) 

I hesitate to either complicate the doc or the code just to be more compatible with rather unrealistic scenarios. If I'm proved wrong, we can always cap it to, say, drainTo(.., 42) - but why a queue from a producer would grow huge enough to matter? We put timeouts to deal With I/O delays, not reading from memory! And such a setting would mean that overstepping a tiny (and unrealistic for I/O) timeout would be the least of the problems in the app. So, yeah. We don't need to do anything here, I trust mother nature to take care of this :-) 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c7) posted by **thomas.andreas.jung** on 2011-11-30 at 06:41 AM_

---

In the end it boils down to: Should the documentation be correct? I would argue, that the description of the contract has to be correct for all valid inputs.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c8) posted by **andreou@google.com** on 2011-11-30 at 07:59 AM_

---

"Waiting up to the specified time" is correct - no wait is ever issued above that. And actively draining available queue elements can't be confused with 'waiting' anyway, so I wonder what would be a 'more correct' specification for you.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c9) posted by **thomas.andreas.jung** on 2011-11-30 at 10:08 AM_

---

According to your argument this implementation

/**
&nbsp;\* @﻿param timeout how long to wait before giving up 
&nbsp;*/
void timeout(long timeout) {
&nbsp;&nbsp;&nbsp;&nbsp;while(true);
}

is correct because it does not wait longer than timeout. It's only busy longer than timeout.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c10) posted by **jim.andreou** on 2011-11-30 at 05:40 PM_

---

You compare a busy wait to actually doing exactly the work which was requested, in the quickest available way? Hmm.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c11) posted by **thomas.andreas.jung** on 2011-12-01 at 07:24 AM_

---

What was requested was to drain at the point in time when the Queues.drain was called.

One way out would be to say this will drain all items that are in the Queue at this moment and the timeout applies only to the waiting period for new items (minus time spent in the initial drain). Once the waiting for new entries starts the timeout should be kept in any case.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c12) posted by **cpovirk@google.com** on 2011-12-02 at 07:05 PM_

---

I just realized that I had no idea what the current doc says. Here it is:

"waiting up to the specified wait time if necessary for {@﻿code maxElements} elements to become available"

There's a decent grammatical argument that, if maxElement elements are _already_ available, the "up to the specified wait time" part doesn't apply. Still, as a reader, I'd probably subconsciously "correct" the statement to mean what I expect. Probably I'd do that for _any_ phrasing short of a separate sentence focused on this behavior. Arguably an edge case doesn't warrant that sentence, since it doesn't seem to have caused problems in practice.

I'm happy to defer to Dimitris on this one. I'll tentatively restore the bug status to "Fixed," and he can leave it or change it as appropriate.

---

**Status:** `Fixed`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c13) posted by **jim.andreou** on 2011-12-03 at 12:17 AM_

---

Yes, that was what I tried to describe. The request is for a certain number of elements, the wait only applies if the elements are not there. 

Perhaps I should s/maxElements/elements/, because the "max" might throw someone off, in the sense of "hey, I don't /really/ want maxElements, that's just an upper bound I'm giving you". 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c14) posted by **thomas.andreas.jung** on 2011-12-18 at 07:47 AM_

---

If the the order of poll and drainTo could be changed from

/**
- Drains the queue as {@﻿link BlockingQueue#drainTo(Collection, int)}, but if the requested
- {@﻿code numElements} elements are not available, it will wait for them up to the specified
- timeout.
 */
 while (added < numElements) {
 &nbsp;q.drainTo(..);
 &nbsp;q.poll(..);
 }

to

/**
- Drains the queue as {@﻿link BlockingQueue#drainTo(Collection, int)} returning {@﻿code numElements} 
- of requested elements or all elements that are available when the timeout elapses.
 */
 while (added < numElements) {
 &nbsp;q.poll(..);
 &nbsp;q.drainTo(..);
 }

documentation and implementation could be described consistently. The open question is if the description is too tricky. And frankly, it's not perfectly true because a lot can happen between the poll and drainTo calls resulting in much more elements than where available when the timeout actually elapsed. At least this documentation gives the user the chance to anticipated that the timeout is not strict.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=794#c15) posted by **jim.andreou** on 2011-12-18 at 06:49 PM_

---

The only difference of the other ordering is that it issues an extra (/unnecessary) poll(...) invocation, when numElements are available, without altering the rest of the behavior. I suspect this is not what you aimed for.
 
Fail-fast iterator behavior _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=85) created by **jared.l.levy** on 2008-06-17 at 01:11 AM_ --- The non-concurrent JDK collection classes have fail-fast iterators. If the an iterator's underlying collection is changed by any mechanism besides the iterator, subsequent iterator methods will throw a ConcurrentModificationException. However, some Google Collection classes, such as multimaps & multisets, yield iterators that aren't fail-fast. Those iterators may or may not throw a ConcurrentModificationException, depending on the call sequence. Should we document that the iterators aren't fail-fast? Should we provide collection wrappers that guarantee that the generated iterators are fail-fast?  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c1) posted by **jared.l.levy** on 2009-04-08 at 01:12 AM_

---

Since nobody asked for fail-fast behavior, adding new functionality is a low
priority. However, we should decide whether additional documentation is needed.

---

**Owner:** ---
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c2) posted by **kevinb9n** on 2009-09-17 at 06:02 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c3) posted by **kevinb@google.com** on 2010-07-30 at 03:53 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c4) posted by **kevinb@google.com** on 2010-07-30 at 03:56 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Priority-Medium`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c5) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c6) posted by **fry@google.com** on 2011-12-10 at 03:32 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c7) posted by **wasserman.louis** on 2012-01-16 at 01:14 AM_

---

I'd like to reopen this issue.

A lot of Guava methods make a pretty big point of failing fast on bad inputs, not because the behavior was requested, but because it was the Right Thing to Do. It's generally true that we strongly prefer failing fast on exceptional conditions to the alternative.

If it's an issue of the effort involved, I'd be willing to do a lot of work on this myself, but I think that we should do it because it's the right thing to do, even if the behavior isn't explicitly requested.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c8) posted by **wasserman.louis** on 2012-01-16 at 03:03 AM_

---

Fail-fast behavior in the face of concurrent modification has been added for the Multiset implementations, complete with additions to the TestSuiteBuilders to test that behavior automagically, at http://code.google.com/r/wassermanlouis-guava/source/list?name=fail-fast-multiset, split into two commits for the test suites and for the Multiset implementation.

I'm quite happy with the implementation approach -- AbstractMultiset got two new methods, notifyModification() and &lt;E> Iterator&lt;E> checkingForComodification(Iterator&lt;E>). By default, those methods use a traditional modification-count-based implementation, but by overriding them you can get behavior like TreeMultiset (which uses the root pointer, rather than modification count, to check for comodification, which is necessary to make sub-multisets behave properly), or you can get ConcurrentHashMultiset, which overrides both of those methods to basically do nothing, since CHM is fine with concurrent modification.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c9) posted by **wasserman.louis** on 2012-01-31 at 01:44 AM_

---

_(No comment entered for this change.)_

---

**Status:** `Started`
**Owner:** wasserman.louis
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c10) posted by **kevinb@google.com** on 2012-05-30 at 07:41 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Enhancement-Temp`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c11) posted by **kevinb@google.com** on 2012-05-30 at 07:45 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement-Temp`, `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=85#c12) posted by **lowasser@google.com** on 2013-02-25 at 10:38 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Fixed`
 
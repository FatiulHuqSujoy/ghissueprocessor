Throw NPE instead of JavaScriptException in primitives gwt tests _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=275) created by **cpovirk+external@google.com** on 2009-10-26 at 05:10 PM_ --- Some of our GWT tests currently accept JavaScriptException instead of NullPointerException. It would be nice to eliminate this diff between GWT and non-GWT tests. Whether we do this for 1.0 depends on whether it gets in our way when getting the tests out the door.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c1) posted by **kevinb9n** on 2009-10-26 at 05:15 PM_

---

Honestly, I am not even viewing "getting the tests out the door" as critical for 1.0. 
We'll just run them internally. Then after 1.0 we can move everything over to guava, 
and then work on getting the tests working there. Hopefully by then there'll have 
been time to make more of the tests "amphibious" than are currently.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c2) posted by **cpovirk+external@google.com** on 2009-10-26 at 05:39 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c3) posted by **kevinb@google.com** on 2010-07-30 at 03:53 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c4) posted by **fry@google.com** on 2011-01-26 at 10:16 PM_

---

_(No comment entered for this change.)_

---

**Owner:** cpov...@google.com
**Labels:** `Type-Defect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c5) posted by **kevinb@google.com** on 2011-01-27 at 01:31 PM_

---

Chris, is this ancient, or still relevant?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c6) posted by **kevinb@google.com** on 2011-01-27 at 01:47 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Tests`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c7) posted by **kevinb@google.com** on 2011-01-27 at 01:57 PM_

---

_(No comment entered for this change.)_

---

**Owner:** ---
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c8) posted by **cpovirk@google.com** on 2011-01-27 at 06:41 PM_

---

Just barely still relevant:

$ grep JavaScriptException -r javatests/com/google/common/
javatests/com/google/common/primitives/gwt/CharsTest.java:import com.google.gwt.core.client.JavaScriptException;
javatests/com/google/common/primitives/gwt/CharsTest.java: } catch (JavaScriptException expectedInGwt) {
javatests/com/google/common/primitives/gwt/BytesTest.java:import com.google.gwt.core.client.JavaScriptException;
javatests/com/google/common/primitives/gwt/BytesTest.java: } catch (JavaScriptException expectedInGwt) {
javatests/com/google/common/primitives/gwt/DoublesTest.java:import com.google.gwt.core.client.JavaScriptException;
javatests/com/google/common/primitives/gwt/DoublesTest.java: } catch (JavaScriptException expected) {
javatests/com/google/common/primitives/gwt/IntsTest.java:import com.google.gwt.core.client.JavaScriptException;
javatests/com/google/common/primitives/gwt/IntsTest.java: } catch (JavaScriptException expectedInGwt) {
javatests/com/google/common/primitives/gwt/LongsTest.java:import com.google.gwt.core.client.JavaScriptException;
javatests/com/google/common/primitives/gwt/LongsTest.java: } catch (JavaScriptException expectedInGwt) {

I don't know offhand whether primitives has other reasons to have separate GWT and non-GWT tests; the larger issue here is probably to merge them, starting with the JavaScriptException=>NPE change.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c9) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c10) posted by **fry@google.com** on 2011-12-10 at 03:41 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Primitives`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c11) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c12) posted by **kevinb@google.com** on 2012-05-30 at 07:49 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Tests`, `Type-Dev`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=275#c13) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 We are dropping GWT-RPC support in ~6 months: https://groups.google.com/forum/#!topic/guava-discuss/ZLpBaYAtUl4 (We'll still be supporting GWT, just not GWT-RPC (serialization). That said, it looks like we fixed this anyway, probably long, long ago.) 
Unicode version for Utf8.isWellFormed According to the java docs 'isWellFormed' returns true for a well-formed byte sequence according to Unicode 6.0 but Java 8 Character is based on Unicode 6.2.0. Are there any cases for which the method would need an update to return the correct value or is this just a documentation problem?  This looks to be just a documentation problem. From the javadocs,

`This method returns true if and only if Arrays.equals(bytes, new String(bytes, UTF_8).getBytes(UTF_8)) does, but is more efficient in both time and space`

For instance, the character U+10200 is unassigned, but Java 8 will happily create a String from that codepoint, and both the Arrays.equals(...) form and Utf8.isWellFormed return true with a valid array (in this case [0xF0, 0x90, 0x88, 0x80]).

The method only checks to see if the UTF-8 encoding of Unicode is correct, and not if the characters exist inside any valid script. Since the only difference between 6.0 and 6.2 is additions of new scripts, and no new planes / code point ranges, no changes other than in documentation should be required. 
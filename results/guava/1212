Add Uninterruptibles#awaitUninterruptibly() methods for java.util.concurrent.locks.Condition _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1212) created by **knut.wannheden** on 2012-11-26 at 08:17 AM_ --- The interface java.util.concurrent.locks.Condition has await() methods just like those of java.util.concurrent.CountDownLatch. It would be nice if the Uninterruptibles class had equivalent awaitUninterruptibly() methods for Condition.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1212#c1) posted by **cpovirk@google.com** on 2012-11-26 at 07:02 PM_

---

Condition provides a no-arg awaitUninterruptibly() to correspond to the no-arg await():
void await() => void awaitUninterruptibly()

So I guess you're asking about these two?
long awaitNanos(long nanosTimeout)
boolean awaitUntil(Date deadline)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1212#c2) posted by **knut.wannheden** on 2012-11-26 at 08:40 PM_

---

Yes, sorry for not making that clear. I was referring to uninterruptible methods corresponding to Condition#await(long, TimeUnit) and Condition#awaitUntil(Date).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1212#c3) posted by **cpovirk@google.com** on 2012-11-26 at 08:57 PM_

---

Thanks.

My stance on Uninterruptibles methods for java.util.concurrent classes is pretty much that we'll add whatever people need. They're just a little bit of a pain to test, so they're not something we can bang out in 5 minutes, so I don't know exactly when we'll get to this.

---

**Status:** `Accepted`
**Labels:** `Type-Addition`, `Package-Concurrent`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1212#c4) posted by **kasperni** on 2012-12-05 at 11:18 AM_

---

awaitUntil(Date deadline) should not be added. All JUC classes relies on System.nanotime() not on System.currentTimeMillis()
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1212#c5) posted by **kak@google.com** on 2013-08-22 at 11:33 PM_

---

_(No comment entered for this change.)_

---

**Owner:** cpovirk@google.com
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1212#c6) posted by **lukes@google.com** on 2013-08-22 at 11:38 PM_

---

This isn't really what you are asking for, but I would suggest switching to use Monitor instead of Condition (https://google.github.io/guava/apidocs/com/google/common/util/concurrent/Monitor.html).

Monitor is easier to use (IMHO) and already supports a timed uninterruptible wait.
 Looks like we did `Condition#await(long, TimeUnit)` in 23.6. If someone does want the `Date` version, please reopen or open a new bug. 
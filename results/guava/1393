Add Optional.get() method that takes an error message string _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1393) created by **electrum** on 2013-04-30 at 06:20 PM_ --- I rarely call Optional.get() without first checking isPresent() because I want to provide a useful error message. For example, I end up writing this: . I would rather write this: . This gets annoying when it breaks chaining: . The additional method would allow chaining: .  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1393#c1) posted by **kurt.kluever** on 2013-04-30 at 06:22 PM_

---

_(No comment entered for this change.)_

---

**Owner:** kurt.kluever
**Labels:** `Type-Addition`, `Package-Base`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1393#c2) posted by **d...@iq80.com** on 2013-04-30 at 06:23 PM_

---

Maybe naming the method checkedGet(String message) would be more clear to a reader.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1393#c3) posted by **kurt.kluever** on 2013-04-30 at 06:24 PM_

---

Interesting...What do you do with the exceptions/error messages? Presumably they get caught at a higher level and logged? Are you concerned about debugging (and in that case, isn't the stack trace enough)?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1393#c4) posted by **hammerschmidt.tobias** on 2014-02-07 at 12:51 PM_

---

From my POV this is a convenience method which can be used for instance to return a more descriptive error message to a user (instead of get called on absent). BTW other frameworks provide this functionality too: https://github.com/functionaljava/functionaljava/blob/master/core/src/main/java/fj/data/Option.java#L183.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1393#c5) posted by **kevinb@google.com** on 2014-02-10 at 06:12 PM_

---

Your example is of using a fixed literal string, which conveys no additional information that the combination of stack trace and API docs don't already provide. To us, that's not enough motivation. Why bother providing these strings? The audience for them is an engineer who's going to have to look at the code regardless.

The real motivation to do this would be if you commonly have data you need to supply to that string, but in that case, the message construction features of Preconditions are useful to have anyway.

---

**Status:** `WorkingAsIntended`
 
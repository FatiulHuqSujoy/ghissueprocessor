Feature request: Functions.memoize(Function) _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=489) created by **yogy.namara** on 2010-11-30 at 06:08 AM_ --- Just like Suppliers.memoize(Supplier). Consider also adding the expiration-dated variant.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=489#c1) posted by **cgdecker** on 2010-11-30 at 03:18 PM_

---

Use MapMaker to create a computing map with the behavior you want, then create a Function based on that with Functions.forMap.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=489#c2) posted by **kevinb@google.com** on 2010-12-08 at 03:25 AM_

---

Exactly.

---

**Status:** `WontFix`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=489#c3) posted by **pimlottc** on 2012-04-03 at 03:19 PM_

---

In light of the changes to MapMaker.makeComputerMap, I think this issue should be revisited.

The original suggestion:

&nbsp;Functions.forMap(new MapMaker().makeComputingMap(someFunction));

is now deprecated.

Naively following the MapMaker Migration Guide suggests:

&nbsp;Functions.forMap(CacheBuilder.newBuilder().build(CacheLoader.from(someFunction)).asMap());

But this does not work as expected, as get() on the map view does not trigger cache loads.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=489#c4) posted by **wasserman.louis** on 2012-04-03 at 03:26 PM_

---

Hmmmm. That suggests two options:
1. Functions.forCache, or Caches.asFunction, or something like that.
2. Push users away from this approach in general, as Caches can throw Exceptions and the like, and Function cannot throw exceptions.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=489#c5) posted by **cpovirk@google.com** on 2012-04-03 at 03:27 PM_

---

You can get the original behavior with "CacheBuilder.newBuilder().build(CacheLoader.from(someFunction)," as LoadingCache implements Function.

This is still messier than the original, but Function isn't typically used for heavyweight operations, so a little verbosity is probably OK for the cases in which it is.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=489#c6) posted by **wasserman.louis** on 2012-04-03 at 03:37 PM_

---

Oh man, I totally forgot that LoadingCache implements Function. That works just fine, then.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=489#c7) posted by **kevinb@google.com** on 2012-04-03 at 09:58 PM_

---

Plus, in many cases, you can just implement CacheLoader in the first place instead of Function.
 
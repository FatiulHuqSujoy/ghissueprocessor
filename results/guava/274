Call to exhaust a given iterator while preserving the last n elements _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=274) created by **tony.chan.nospam** on 2009-10-24 at 04:22 PM_ --- Sometimes I only want the last n elements from an iterator and sometimes I want those last n elements in the reverse order they came in. The code to do this is pretty simple but not exactly one-liners you can chain together to do this, at least not easily. This contribution is a small patch of code that does the above as a simple one line call. Thought this might be useful enough to be considered for inclusion in the Iterators library.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c1) posted by **jared.l.levy** on 2009-10-24 at 05:06 PM_

---

Thanks for the contribution. Your suggestion is definitely reasonable. The only
question is how often people would want that functionality.

FYI, your methods could return a List instead of an Iterable, while still taking an
Iterable as an input parameter. Simply create a subclass of AbstractList that
implements get().

---

**Labels:** `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c2) posted by **kevinb9n** on 2009-10-24 at 05:30 PM_

---

List is a better return type. Especially because I'd have it expected it not to be in
reverse order, but to be able to pass it to Iterables.reverse() if I did want it in
reverse order.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c3) posted by **tony.chan.nospam** on 2009-10-24 at 07:45 PM_

---

Good points, I forgot about AbstractList. Here's a much simpler implementation where the iterable version 
returns a list. And yeah, the use-case for this is pretty specific. Thought I'd throw it out there anyhow.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c4) posted by **kevinb@google.com** on 2010-07-30 at 03:53 AM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c5) posted by **fry@google.com** on 2011-01-26 at 10:08 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Accepted`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c6) posted by **fry@google.com** on 2011-01-26 at 10:16 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c7) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c8) posted by **cpovirk@google.com** on 2011-07-13 at 08:37 PM_

---

_(No comment entered for this change.)_
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c9) posted by **fry@google.com** on 2011-12-10 at 03:41 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c10) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c11) posted by **kevinb@google.com** on 2012-05-30 at 07:43 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Addition`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=274#c12) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 You should be able to easily do this using EvictingQueue that we added in 15.0
 
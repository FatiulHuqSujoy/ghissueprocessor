New utility function to com.google.common.base.Throwables _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1408) created by **zolyfar...@yahoo.com** on 2013-05-11 at 12:31 AM_ --- I propose adding a function: . Use case: &nbsp;exception is thrown in a Exception handler: { .... } catch (SomeException e1) { &nbsp;&nbsp;{ &nbsp;&nbsp;&nbsp;... &nbsp;&nbsp;} catch (SomeOtherException e2) { &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;throw Throwables.chain(e1, e2); &nbsp;&nbsp;} } This will allow re-throwing both exceptions as a causal chain, in my view a cleaner choice than logging... I have implemented Throwables.chain at: http://code.google.com/p/spf4j/source/browse/trunk/src/main/java/org/spf4j/base/Throwables.java I would love to have this in my view useful function as part of guava. let me know if you have any questions.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1408#c1) posted by **cgdecker** on 2013-05-11 at 01:28 PM_

---

So, there really isn't a good way to address the type of situation you describe in Java 6. That's why they added suppressed exceptions in Java 7:

} catch (SomeException e1) {
&nbsp;&nbsp;{
&nbsp;&nbsp;&nbsp;&nbsp;...
&nbsp;&nbsp;} catch (SomeOtherException e2) {
&nbsp;&nbsp;&nbsp;&nbsp;e1.addSuppressed(e2);
&nbsp;&nbsp;}
&nbsp;&nbsp;throw e1;
}

The big issue with the approach you suggest is that it misappropriates the cause of an exception for something else that isn't a cause. I can't see this doing anything but causing lots of confusion to someone looking at the stack trace for an exception to determine what happened. You go down through the stack trace looking for the root cause, and at some point come to a totally unrelated exception with a different stack trace and its own causes... it's just a mess, and even worse if there's more than one exception that you need to suppress.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1408#c2) posted by **zolyfar...@yahoo.com** on 2013-05-11 at 02:42 PM_

---

I agree, this implementation came out of my dislike of the other way of dealing with this situation in java 1.6: logging the suppressed exception... 

One way of reducing the disadvantages you mention is:

Throwables.chain(e1, new SuppressedException(e2));

or: Throwables.suppress(e1, e2) witch can to the above automatically.

this might be a better compromise?

Actually I am going to implement this in my library since it is clearer :-)

Java 7 will make this obsolete... but until then I find this the lesser evil...
 
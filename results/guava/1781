optimalNumOfHashFunctions() May not Return what you Expect _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1781) created by **justatheory** on 2014-06-13 at 08:25 PM_ --- Was experimenting with JavaScript code when I noticed this. If you call . It returns 6. A quick JavaScript version returned 7. The reason is because the division acts only on integers and returns an integer. In JavaScript everything is a float. That is, in Java, `3072 / 319`&nbsp;returns 9, but in JavaScript it returns 9.63. Maybe it was intentional to have an implicitly truncated value, but I'm guessing not. The fix would be something like: --- a/guava/src/com/google/common/hash/BloomFilter.java +++ b/guava/src/com/google/common/hash/BloomFilter.java @@ -363,7 +363,7 @@ public final class BloomFilter&lt;T> implements Predicate&lt;T>, Serializable { &nbsp;&nbsp;&nbsp;&nbsp;*/ &nbsp;&nbsp;&nbsp;@﻿VisibleForTesting &nbsp;&nbsp;&nbsp;static int optimalNumOfHashFunctions(long n, long m) { - return Math.max(1, (int) Math.round(m / n \* Math.log(2))); - return Math.max(1, (int) Math.round((float)m / n \* Math.log(2))); &nbsp;&nbsp;&nbsp;} &nbsp;&nbsp;&nbsp;/**  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1781#c1) posted by **kak@google.com** on 2014-06-13 at 08:30 PM_

---

Hey Dimitri,

Mind taking a look at this since you originally wrote that code?

Thanks,
-kak

---

**Owner:** andreou@google.com
**Labels:** `Package-Hash`, `Type-Defect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1781#c2) posted by **andreou@google.com** on 2014-06-16 at 10:00 AM_

---

Sure. This looks valid; the current code is not a faithful rendering of the intended formula. An extra pair of parens around "n \* Math.log(2)" should do. This can be fixed, since the number of hash functions is part of the serial form, so a different function for the create() method can't affect existing BFs.

The impact of this should be fairly low - at worst, the BF is using one less hash function than it would (5 instead of 6), and this doesn't affect the default fpp either.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1781#c3) posted by **kak@google.com** on 2014-06-16 at 03:05 PM_

---

@﻿andreou: I don't think the extra pair of parens will solve it. Right now we're doing:
m / n \* Math.log(2) // this is really (m / n) \* Math.log(2)
Your proposal changes the formula:
m / (n \* Math.log(2))

---

**Status:** `Started`
**Owner:** kak@google.com
**CC:** andreou@google.com
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1781#c4) posted by **andreou@google.com** on 2014-06-16 at 03:07 PM_

---

Oops, yeah. That was too fast. :)
 
Multi-argument Objects.firstNonNull _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=632) created by **mariusz.lotko** on 2011-05-24 at 01:34 PM_ --- Why is it limited to just two arguments? What about that implementation: &nbsp;&nbsp;public static &lt;T> T firstNonNull(@﻿Nullable T... objects) { &nbsp;&nbsp;&nbsp;&nbsp;for (T o : objects) { &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (o != null) { &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return o; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} &nbsp;&nbsp;&nbsp;&nbsp;} &nbsp;&nbsp;&nbsp;&nbsp;throw new NullPointerException(); &nbsp;&nbsp;} BTW, I'm a bit confused that you decided to throw NPE if there's no non-null references in argument list (Preconditions.checkNotNull() does so()). It could be left to the client whether he want's to have "guarantee of non-null reference" or just "first which is not null or null if all are nulls".  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=632#c1) posted by **cgdecker** on 2011-05-24 at 02:04 PM_

---

It wouldn't be "firstNonNull" if it were allowed to return null.

---

**Status:** `Duplicate`
**Merged Into:** #384
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=632#c2) posted by **mariusz.lotko** on 2011-05-24 at 02:20 PM_

---

True - the name wouldn't strictly reflect the effect. But it's not completely odd - you get first non-null reference or null if there's no such. It doesn't change a lot if you expect that some argument MUST be non-null - the difference would be just the place where the NPE is thrown (caller vs. lib). And if you allow it on caller's side you enable yet another usage scenario.

I find explicit throw of NPE a little bit strange. JVM will do it for you. There are opinions it's the programming pattern: http://pmd.sourceforge.net/rules/strictexception.html
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=632#c3) posted by **cgdecker** on 2011-05-24 at 03:23 PM_

---

Guava strongly favors avoiding null in your code and detecting nulls that should not be there as close to the source as possible. Objects.firstNonNull() is primarily for converting objects that might be null (from 3rd party code you don't have control over, say) to definitely non-null values in order to help keep null out of your code.

As Kevin mentions in issue 384, Iterables.find() is a more general method that can be used to satisfy your requirement. Another option you'll have soon with Guava is to wrap possibly-null values using Optional. It has a nice syntax that can be used to get the first present value from a series of optionals, and you can even choose to get null at the end if none are first.or(second).or(third).orNull();
.
 
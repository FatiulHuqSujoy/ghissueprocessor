Function interface mismatch causes failure at runtime  _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=765) created by **Todd.Schiller** on 2011-10-19 at 05:43 AM_ --- I define a Function of the form: new Function&lt;MyType, String>(){ &nbsp;&nbsp;@﻿Override &nbsp;&nbsp;public String apply(MyType arg0){ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ...; &nbsp;&nbsp;} } , which compiles in Eclipse as expected. However, at runtime in hosted mode, I receive the following errors: [ERROR] [MyProject] - Line XXX: The type new Function&lt;MyType,String>(){} must implement the inherited abstract method Function&lt;MyType,String>.apply(Object) [ERROR] [MyProject] - Line YYY: The method apply(MyType) of type new Function&lt;MyType,String>(){} must override or implement a supertype method <b>What version of the product are you using? On what operating system?</b> Guava GWT 10.0.1 <b>Please provide any additional information below.</b> GWT 2.4.0 Guava 10.0.1 Eclipse 3.7.1 JavaSE-1.6 Fedora Core 14 (2.6.35.14-96.fc14.i686.PAE)  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c1) posted by **Todd.Schiller** on 2011-10-19 at 06:03 AM_

---

A similar issue also arises with the Predicate interface:

[ERROR] [MyProject] - Line XXX: The type new Predicate&lt;MyType>(){} must implement the inherited abstract method Predicate&lt;MyType>.apply(Object)

[ERROR] [MyProject] - Line YYY: The method apply(MyType) of type new Predicate&lt;MyType>(){} must override or implement a supertype method
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c2) posted by **wasserman.louis** on 2011-10-19 at 06:10 PM_

---

This is caused by Java 5/Java 6 issues, not Guava. (Specifically, JDK 5 refuses to accept @﻿Override annotation on interface methods, where JDK 6 handles it fine. I think you're supposed to be able to compile with JDK 6, execute with JDK 5?)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c3) posted by **wasserman.louis** on 2011-10-19 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c4) posted by **cpovirk@google.com** on 2011-10-20 at 11:47 PM_

---

I wouldn't have guessed it was an @﻿Override problem, since the compiler doesn't seem to recognize that there's an apply() implementation at all. I'm actually wondering if it's a GWT bug.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c5) posted by **Todd.Schiller** on 2011-10-21 at 02:08 AM_

---

@﻿wasserman.louis: I'm not mixing JDK 5 and JDK 6. I'm using 1.6 both for compilation and for the runtime environment.

Attempting to run with a 1.5 JRE gives the warning: "WARNING: Use on a JRE prior to version 1.6 is deprecated", and then everything dies.

With JDK 6, I'm also getting the following errors during validation, which I thought were supposed to be fixed since R9:

[ERROR] [MyProject] - Errors in 'jar:file:/.../MyProject/war/WEB-INF/lib/guava-gwt-10.0.jar!/com/google/common/base/Predicate.java'
[ERROR] [MyProject] - Line 21: The import javax.annotation.Nullable cannot be resolved
[ERROR] [MyProject] - Line 45: Nullable cannot be resolved to a type
[ERROR] [MyProject] - Line 58: Nullable cannot be resolved to a type
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c6) posted by **andy.dennie** on 2011-11-10 at 04:25 PM_

---

I'm having the same "Nullable" errors as described in comment 4, except that mine are reported with respect to 'jar:file:.../myproject/war/WEB-INF/lib/guava-gwt-10.0.1.jar!/com/google/common/base/Equivalence.java'
.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c7) posted by **kevinb@google.com** on 2011-11-15 at 10:11 PM_

---

We believe this is a GWT issue, but if there's something specific we can do let us know.

---

**Status:** `WontFix`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c8) posted by **andy.dennie** on 2011-11-16 at 02:21 PM_

---

In comparing guava 9 (which doesn't exhibit the problem) to guava 10, I suspect that the cause has something to do with the fact that the javax.annotation.\* packages were included in 9 but not in 10. See the attached screenshot. Could you confer with the GWT team to confirm?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c9) posted by **pondruska+o...@csas.cz** on 2011-11-20 at 09:45 PM_

---

Yes, this looks like javax.annotation.\* is missing in guava 10 (but present in guava 9). See https://github.com/google/guava/issues/776 as that is similar.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=765#c10) posted by **cpovirk@google.com** on 2011-12-02 at 10:42 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Duplicate`
**Merged Into:** #776
 
EvictionListener is not called during computation _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=552) created by **tom%tommay....@gtempaccount.com** on 2011-02-18 at 10:49 PM_ --- What steps will reproduce the problem? 1. Compile the attached file: javac -cp guava-r08.jar Tom.java 2. Run it: java -Xmx50m -cp .:guava-r08.jar Tom What is the expected output? It should print a bunch of lines like this that show the size of the Map increasing as entries are added: Size: 1 Size: 2 Size: 3 ... Eventually one or more entries will be gc'd and the EvictionListener should be called when the map's public methods size() and/or get() are called, according to the MapMaker javadoc: "It will invoke the listener synchronously, during invocations of any of that map's public methods (even read-only methods)." At that point, the program should print "Evicted ..." and exit. What do you see instead? The size increases, then decreases and increases again. The EvictionListener is never called even though entries are being removed and public methods get() and size() are being called. E.g., Size: 4640 Size: 4641 Size: 4642 Size: 4643 Size: 4644 Size: 4645 Size: 4646 Size: 4328 Size: 4324 Size: 4325 Size: 4323 Size: 4259 ... Eventually the program will slow to a crawl then crash, presumably because memory is depleted since the notification queue is never drained: Size: 529 Size: 530 Size: 531 Exception in thread "main" com.google.common.collect.ComputationException: java.lang.OutOfMemoryError: GC overhead limit exceeded &nbsp;&nbsp;&nbsp;&nbsp;at com.google.common.collect.ComputingConcurrentHashMap$ComputingSegment.compute(ComputingConcurrentHashMap.java:167) &nbsp;&nbsp;&nbsp;&nbsp;at com.google.common.collect.ComputingConcurrentHashMap$ComputingSegment.compute(ComputingConcurrentHashMap.java:116) &nbsp;&nbsp;&nbsp;&nbsp;at com.google.common.collect.ComputingConcurrentHashMap.apply(ComputingConcurrentHashMap.java:67) &nbsp;&nbsp;&nbsp;&nbsp;at com.google.common.collect.MapMaker$ComputingMapAdapter.get(MapMaker.java:623) &nbsp;&nbsp;&nbsp;&nbsp;at Tom.main(Tom.java:29) Caused by: java.lang.OutOfMemoryError: GC overhead limit exceeded &nbsp;&nbsp;&nbsp;&nbsp;at Tom$1.apply(Tom.java:23) &nbsp;&nbsp;&nbsp;&nbsp;at Tom$1.apply(Tom.java:20) &nbsp;&nbsp;&nbsp;&nbsp;at com.google.common.collect.ComputingConcurrentHashMap$ComputingSegment.compute(ComputingConcurrentHashMap.java:155) &nbsp;&nbsp;&nbsp;&nbsp;... 4 more What version of the product are you using? On what operating system? guava-r08.jar javac -version javac 1.6.0_22 java -version java version "1.6.0_22" Java(TM) SE Runtime Environment (build 1.6.0_22-b04) Java HotSpot(TM) 64-Bit Server VM (build 17.1-b03, mixed mode) Ubuntu 10.10 Please provide any additional information below.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=552#c1) posted by **boppenheim@google.com** on 2011-02-21 at 10:13 AM_

---

See the discussion in Issue 482.

---

**Status:** `Duplicate`
**Merged Into:** #482
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=552#c2) posted by **fry@google.com** on 2011-02-24 at 02:27 PM_

---

Yes, this is a bug in release 8 that has already been fixed, and will be available in release 9.

---

**Status:** `Fixed`
**Merged Into:** -#482
**Labels:** `Milestone-Release09`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=552#c3) posted by **fry@google.com** on 2011-03-25 at 08:16 PM_

---

_Issue #579 has been merged into this issue._
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=552#c4) posted by **jonathaz** on 2011-04-01 at 03:56 PM_

---

I am getting a negative size() and isEmpty() of false on a Map with weakKeys() and weakValues() using guava r09-rc2. The map is indeed empty and iterating over the keySet() shows no entries, but the size(), keySet().size(), and entrySet().size() all return the same negative value.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=552#c5) posted by **fry@google.com** on 2011-04-05 at 04:02 PM_

---

I created issue 591 for the negative size report.
 
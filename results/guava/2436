Why doesn't Cache.invalidate() return a boolean? Removal listeners are nice and all, but why doesn't `Cache.invalidate()` return true when an entry is successfully removed? The information is available to it and from a thread-safety perspective we cannot really know whether a particular call to `Cache.invalidate()` really removed an entry by using removal listeners. Consider: T1: Cache.invalidate("key") //"key" wasn't originally in the map T2: Cache.put("key", "value") T2: Cache.invalidate("key") T1 seems that RemovalListener.onRemoval("key") was invoked, but it really corresponds to action by T2.  I asked a similar question internally a couple weeks ago...I've copied out the relevant parts of that discussion:

me: Should Cache.invalidate(Object) return the cached value (if present)?

@lowasser : Note that if you want this behavior, Cache.asMap().remove(key) will do this 

@cgdecker : I think it just doesn't make much sense in general to get the value back from a Cache when you want to invalidate it... the reason you want to invalidate it is presumably because the cached object is no longer current and you want a new version. Also, Cache is out of @Beta as of 19.0 so I do think it's probably not something we can change regardless.

fry: For the record it was (as everything else in Cache) an explicit design decision. @kevinb9n might remember better than I, but the elements I recall were that we were semantically trying to model something which could be thought of as a Cache instead of a Map, and in such a model invalidate simply had no need to return anything, even though it was possible.
 @kluever Thanks for the clarification. Closing seeing as this is by design and there is a reasonable workaround.
 
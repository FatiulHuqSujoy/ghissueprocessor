ImmutableTable.copyOf(), ImmutableTable.builder().putAll() should skip null Given: . `copyOf` and `putAll` throw exceptions as if the developer had asked them to populate cell values with `null` but in actuality the developer didn't didn't explicitly populate the cells in question. In this case, I suggest simply skipping over `null` values and only `put`ing the remaining values.  This seems a pretty clear duplicate of https://github.com/google/guava/issues/1442.
 To summarize the conclusion of that issue: the null values in ArrayTable are, in every sense, "really there." They count, whether or nut null was explicitly put in.
 @lowasser I was attempting to implement a Stream Collector for `ImmutableTable` using `ArrayTable` as an intermediate container. I guess my point is that it is practically impossible to convert an `ArrayTable` to any other kind of `Table` because of the way nulls are being handled.

Contrast this with `java.util` that differentiates between `null` values set by users versus non-existent mappings. This allows users to easily convert a `HashMap` to a `ConcurrentHashMap` even though the former supports `null` values while the latter does not.

Is there any particular reason that you decided to bubble this implementation detail up to the API level? An alternative implementation would be to add a `BitSet` that keeps track of which cells were explicitly set.
 We should probably document `ArrayTable` to say "This is probably not the `Table` implementation that you want." It accounts for <2% of the `Table` usages in Google code, for example.

I think that its reason for existing is pretty much exactly "to support its weird API." Maybe there are cases in which an array-backed "normal" `Table` would come out ahead in memory usage, though. Sadly, I suspect that they'll be rare enough that we don't add such a thing.
 Also, @cowwoc, I don't believe your claim about java.util is actually true. `new ConcurrentHashMap<>(myHashMap)` fails if `myHashMap` has null keys or values, and the same with `Collectors.toConcurrentMap`.

It's true that you have to explicitly put the null values in, but that's the point: `ArrayTable` _has those null values._ That's how it's defined; it starts filled with nulls. It's not an implementation detail, it's a conscious and deliberate part of the API design. If that hadn't been intentional, we would've done something like what EnumMap does, with an internal explicit object being used to represent null that gets translated away.
 > ArrayTable has those null values. That's how it's defined; it starts filled with nulls.

Okay. That point didn't come across when I read `The value corresponding to a given row and column is null unless another value is provided.` I wish there was a way to emphasize/clarify this point. Anyway, thanks for the clarification.
 Thanks. We should emphasize it: https://github.com/google/guava/issues/2495
 
checkNotNull in Preconditions with multiple values in one call (e.g. as varargs) _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1447) created by **stefan.bicher** on 2013-06-13 at 12:23 PM_ --- Hi Guavians, I´d prefer if there was a "concluded" checkNotNull-method in Preconditions to save a few lines of code. I often uses constructors like these: public Person (@﻿Nonnull final String name,@﻿Nonnull final String surname, final Adress adress) { &nbsp;&nbsp;&nbsp;&nbsp;Preconditions.checkNotNull(name); &nbsp;&nbsp;&nbsp;&nbsp;Preconditions.checkNotNull(surname); &nbsp;&nbsp;&nbsp;&nbsp;Preconditions.checkNotNull(adress); . } I would like to write something like this: public Person (@﻿Nonnull final String name,@﻿Nonnull final String surname, final Adress adress) { &nbsp;&nbsp;&nbsp;&nbsp;Preconditions.checkNotNullForAll(name,surname,adress); // constructor stuff like this.name = name and so on.... } By the way: Thanks for all the amazing stuff in guava - it´s awesome!!!  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1447#c1) posted by **kurt.kluever** on 2013-06-13 at 03:12 PM_

---

Hey Stefan,

You're best bet is to just inline the checkNotNull with your assignments. In fact, you'll end up with even fewer lines of code doing this than if we had checkNotNullForAll(). For example:

public Person (@﻿Nonnull final String name,@﻿Nonnull final String surname, final Adress adress) {
&nbsp;&nbsp;&nbsp;&nbsp;this.name = Preconditions.checkNotNull(name);
&nbsp;&nbsp;&nbsp;&nbsp;this.surname = Preconditions.checkNotNull(surname);
&nbsp;&nbsp;&nbsp;&nbsp;this.address = Preconditions.checkNotNull(adress);
}

A common approach is to actually static import checkNotNull as well. Then you end up with just:

public Person (@﻿Nonnull final String name,@﻿Nonnull final String surname, final Adress adress) {
&nbsp;&nbsp;&nbsp;&nbsp;this.name = checkNotNull(name);
&nbsp;&nbsp;&nbsp;&nbsp;this.surname = checkNotNull(surname);
&nbsp;&nbsp;&nbsp;&nbsp;this.address = checkNotNull(adress);
}

Thanks,
-Kurt

---

**Status:** `WontFix`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1447#c2) posted by **stefan.bicher** on 2013-06-13 at 03:52 PM_

---

Hey Kurt,

that´s a pretty solution - I missed the return value of Preconditions.checkNotNull().

Thanks a lot!

Stefan
 
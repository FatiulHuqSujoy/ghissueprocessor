Throwables.getRootCause leads to infinite loop sometimes _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1173) created by **Alexandr.Berdnik** on 2012-10-19 at 06:36 AM_ --- Some SqlExceptions thrown by Oracle jdbc drivers has reference to itself in cause field. For this exception: ex == ex.getCause() and it leads to infinite loop if getRootCause() used. Actually it can be treated as defect in drivers. Anyway, the proposed fix: additional condition could be used to prevent it. &nbsp;&nbsp;public static Throwable getRootCause(Throwable throwable) { &nbsp;&nbsp;&nbsp;&nbsp;Throwable cause; &nbsp;&nbsp;&nbsp;&nbsp;while ((cause = throwable.getCause()) != null && cause != throwable) { &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;throwable = cause; &nbsp;&nbsp;&nbsp;&nbsp;} &nbsp;&nbsp;&nbsp;&nbsp;return throwable; &nbsp;&nbsp;}  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c1) posted by **stephan202** on 2012-10-19 at 07:58 AM_

---

That fix doesn't guard against cycles of size > 1 ;).

(No, this is not a serious complaint.)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c2) posted by **Alexandr.Berdnik** on 2012-10-19 at 08:09 AM_

---

I can hardly imagine that :) Approach with slow/fast iterator will be better in this case.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c3) posted by **cgdecker@google.com** on 2012-10-19 at 05:04 PM_

---

I don't know where exactly, but I seem to remember something similar to this being brought up before, and the conclusion I recall was that Throwables containing a loop in the causes are fundamentally broken (printStackTrace() will stack overflow, for example) and that we shouldn't do anything to try to accommodate them.

---

**Labels:** `Package-Base`, `Type-Defect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c4) posted by **Alexandr.Berdnik** on 2012-10-19 at 06:57 PM_

---

Jdk's printStackTrace() uses IndentityHashSet named dejavu inside it to prevent such loops. 
I've implemented simple loop-detecting iterator - it works well with normal and end-looped cases and finds last element, but fails to find last element in case of cycles like 1->2->3->4->5->2. In latter case it returns some random element inside the cycle.
It can be reused in getCausalChain() which is affected to this issue too.
Approach with set of visited elements a much more heavier though it provide a better result.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c5) posted by **cgdecker@google.com** on 2012-10-19 at 08:25 PM_

---

Interesting... that seems to be new in JDK7.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c6) posted by **Maaartinus** on 2012-10-19 at 10:27 PM_

---

I wonder what should be the result in such a case. With loops of length 1 it's rather clear, but with longer ones?

I don't like the "slow/fast iterator" as it's a clever solution for cases when you can't afford the memory for the whole loop, which doesn't apply here. For maximum efficiency in the common case, you could follow `cause`&nbsp;(handling `cause==this`&nbsp;as `null`) for some bound number of iterations and switch to using dejaVu if it fails.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c7) posted by **ogregoire** on 2012-10-20 at 01:01 PM_

---

Personally, I'd throw an exception as it's not possible to identify the root cause in this scenario. We might get clues, but if we do we're making assumptions on a certain behavior and I don't think Guava should go that way.

What I'd do is fix getCausalChain to detect loops and force people to use this one if they fear a loop might occur.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c8) posted by **Alexandr.Berdnik** on 2012-10-20 at 08:23 PM_

---

Well, I don't want to get any exception during getting root cause, I'd prefer to get last exception in terms of dejaVu - i.e. last unvisited one.
It can be implemented reasonable fast - bounded loop with fallback to dejaVu, or slow/fast with fallback to dejaVu, or even dejaVu itself - usage scenarios for me is error reporting, which is of course not a bottleneck :).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c9) posted by **ogregoire** on 2012-10-21 at 02:44 AM_

---

That's exactly what I mean: you'd prefer to get the last unvisited one, but on my side, I have absolutelty no guarantee that it's the first in the whole chain. What you suggest is ambiguous.

Getting the whole chain is then possible by retrieving the last element returned by getCausalChain, which is be guaranteed to return what you'd expect. Here, the result is unequivocal.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c10) posted by **Alexandr.Berdnik** on 2012-10-21 at 05:41 AM_

---

We can get ambiguous result only in case when loop contains more than one element. I've never seen anything like this. Maybe throwing an exception is OK here. For one element loop the cause can be determined and returned.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c12) posted by **cpovirk@google.com** on 2012-10-29 at 02:58 AM_

---

"Some SqlExceptions thrown by Oracle jdbc drivers has reference to itself in cause field.": I think this is a red herring. By default, _every_ Throwable has its |cause| field set to |this|. It's a special value that merely means "uninitialized." That's why getCause() looks like cause);
}
.

...and initCause looks like this;
}
.

Notice in particular that getCause cannot return |this|.

None of this takes away from the larger point that it's possible to have cycles in the chain of causation. However, it doesn't sound like we have any reports that this is a problem in practice.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c13) posted by **Alexandr.Berdnik** on 2012-10-29 at 06:38 AM_

---

Looked into jre (1.7) implementation - sometimes (see ClassNotFoundException for example) getCause() is overriden and returns value of private field w/o checks.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c14) posted by **cpovirk@google.com** on 2012-10-29 at 01:24 PM_

---

Yes, and ClassNotFoundException requires that the cause be passed to the constructor. As a result, it can't be used to produce self-causation, either.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c15) posted by **lowasser@google.com** on 2012-10-29 at 02:32 PM_

---

Do we have any idea why some rare exceptions would need to have causative loops? What made them do that in the first place?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c16) posted by **cpovirk@google.com** on 2012-10-29 at 05:38 PM_

---

Unclear. The JDK bug that prompted their change has no details:
http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6962571
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c17) posted by **brianfromoregon** on 2012-11-13 at 01:46 AM_

---

Related: http://stackoverflow.com/questions/7585345/cycles-in-chained-exceptions
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1173#c18) posted by **kak@google.com** on 2013-08-22 at 10:55 PM_

---

Someone could cause a similar infinite loop if they passed a cycling iterable to an immutable copyOf() method. These bugs should be easy to detect and catch and really aren't worth additional code, complexity, and runtime cost to "normal" users.

---

**Status:** `WorkingAsIntended`
 
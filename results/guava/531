additional Strings.repeat() methods  _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=531) created by **wromich** on 2011-01-27 at 12:59 PM_ --- public static String repeat(String str, int count, String separator) { } This method is very useful for building sql queries. Example of use: &nbsp;String query = "SELECT \* from t WHERE t.id in (" + Strings.repeat("?",10,",") + ")".  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=531#c1) posted by **kevinb@google.com** on 2011-01-27 at 01:22 PM_

---

Joiner.on(',').join(Collections.nCopies(10, "?"))

---

**Status:** `WontFix`
**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=531#c2) posted by **kuaw26** on 2011-01-27 at 01:31 PM_

---

Kevin, don't you think that "Joiner.on(',').join(Collections.nCopies(10, "?"))" is too verbose?

Making a string of params separated by some delimiter is very common task and special Strings.repeat() would be very helpful.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=531#c3) posted by **neveue** on 2011-01-28 at 11:29 PM_

---

I often need to join collections / iterables containing various elements, but I never encountered this use case. I can see how it could be useful for SQL queries, but I believe Kevin is right here.

Sure [Joiner.on(',').join(Collections.nCopies(10, "?"))] is more verbose than [Strings.repeat("?", 10, ",")], but it's also more powerful. For example, say you actually want to append your repeated string to a StringBuilder. With Joiner, you could do:

StringBuilder sb = ...
Joiner.on(',').appendTo(sb, nCopies(10, "?"));

Moreover, the [Strings.repeat(String str, int count, String separator)] method is risky: people might unknowingly invert the "str" and "separator" arguments.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=531#c4) posted by **j...@nwsnet.de** on 2011-01-31 at 08:52 AM_

---

Not implementing such a shortcut might also additionally motivate to replace risky manual SQL query assembly ;)
 
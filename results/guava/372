Add startsWithIgnoreCase and endsWithIgnoreCase to Strings class _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=372) created by **attilan3i** on 2010-06-18 at 01:37 PM_ --- I find these methods convenient and their implementation is quite simple, just calling String.regionMatches: public static boolean startsWithIgnoreCase(String s, String start) { &nbsp;&nbsp;return s.regionMatches(true, 0, start, 0, start.length()); } public static boolean endsWithIgnoreCase(String s, String end) { &nbsp;&nbsp;return s.regionMatches(true, s.length() - end.length(), end, 0, end.length()); }  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=372#c1) posted by **finnw1** on 2010-09-13 at 09:04 PM_

---

This would suffer from the problem that you need a Locale to determine whether two strings are equal "ignoring case", because the meaning of that varies from one language to another.

See http://www.w3.org/International/wiki/Case_folding

Maybe you could add something like:

class Strings {
&nbsp;&nbsp;...
&nbsp;&nbsp;public static boolean startsWith(String s, String start, Equivalence&lt;String> eq) {
&nbsp;&nbsp;...
&nbsp;&nbsp;public static Equivalence&lt;String> ignoreCase(Locale locale);
}

but it would not be easy to implement efficiently as case folding can change the length of a string.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=372#c2) posted by **kevinb@google.com** on 2011-02-03 at 06:22 AM_

---

This seems out of scope for Strings.

---

**Status:** `WontFix`
 
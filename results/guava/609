Wanted in Iterables: Iterable<T> narrow(Iterable<?>, Class<? super T>) _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=609) created by **jaredjacobs** on 2011-04-19 at 05:51 PM_ --- The method would throw ClassCastException when an object of the wrong type is encountered. The same method would also be welcomed in Collections2. This feature request was recently mentioned in the comments of: https://github.com/google/guava/issues/604  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c1) posted by **jaredjacobs** on 2011-04-21 at 04:35 PM_

---

The second argument should just be Class&lt;T>. Eclipse adds "? super" a lot, and I copied from exploratory code I'd written there.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c2) posted by **finnw1** on 2011-04-25 at 10:26 AM_

---

Could this be implemented in Functions instead and then passed to {Collections2,Iterables}.transform?

e.g.

public abstract class Functions {
&nbsp;&nbsp;&nbsp;&nbsp;// ...
&nbsp;&nbsp;&nbsp;&nbsp;public static &lt;T>Function&lt;Object, T> narrow(Class&lt;T> clazz) {...}
&nbsp;&nbsp;&nbsp;&nbsp;// ...
}

Iterable&lt;Number> numbers;
// ...
Iterable&lt;Integer> integers = Iterables.transform(numbers, Functions.narrow(Integer.class));
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c3) posted by **jaredjacobs** on 2011-04-25 at 05:05 PM_

---

I like that idea. In Functions, I would probably name it "cast" though, since it's only acting on one particular member of the Collection/Iterable at a time.

One drawback is that this would make the result deferred. So in the case of wanting the casts done immediately, this solution would require two pieces of cruft more than the solution originally proposed:

ImmutableList.copyOf(Iterables.transform(numbers), Functions.cast(Integer.class))

vs.

Iterables.narrow(numbers, Integer.class)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c4) posted by **gscerbak** on 2011-05-10 at 12:15 PM_

---

How is this really different than this:

public static &lt;T> Iterable&lt;T> filter(Iterable<?> unfiltered,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class&lt;T> type)

in Iterables?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c5) posted by **neveue** on 2011-05-10 at 12:58 PM_

---

Iterables.narrow() would fail fast (throwing ClassCastExceptions), while Iterables.filter() simply ignores wrongly typed instances.

I like the Functions.cast() idea.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c6) posted by **gscerbak** on 2011-05-10 at 01:12 PM_

---

Yes, it would be nice to have something to replace the Eclipse template }
.

Maybe it would make sense to add a version which will return Optional&lt;T>...
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c7) posted by **kevinb@google.com** on 2011-07-13 at 07:31 PM_

---

I'll close this as WontFix when I have time to explain why. :)

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c8) posted by **kevinb@google.com** on 2011-07-13 at 07:32 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c9) posted by **cpovirk@google.com** on 2011-07-13 at 07:34 PM_

---

Copy-and-paste of an internal explanation:

---

I share Sam's view that this feels kinda sorta like checkedCollection.
&nbsp;The difference is that checkedCollection's contents are checked at
write time[*], while the contents of transform(..., cast(...)) are
checked at read time. The weird thing is, the contents are typically
going to be checked at runtime, anyway, e.g.:

Foo first = queryResult.get(0);

This performs the same cast as your proposed method would. Given a
choice between the two ways of producing the cast, I'd lean toward the
unsafe (List&lt;Foo>) cast. That's the essential meaning of an unsafe
cast: You may get ClassCastExceptions at a later point in your
program. transform(), by contrast, generally means to derive one
object from another without possibility of failure (and, typically,
returning a different object).

Admittedly cast() is a bit more strict, forcing the exception to occur
immediately and always. Still, the fact that it throws an exception
at all makes it feel less like a Function to me and more like a
separate debugging utility. (And we're back to checkedCollection :))

---

Let us know if you disagree.

---

**Status:** `WontFix`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c10) posted by **cpovirk@google.com** on 2011-07-13 at 07:35 PM_

---

My mistake: the discussion was external after all. Context here: http://groups.google.com/group/guava-discuss/browse_thread/thread/f4d4688dca2a7b7e
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=609#c11) posted by **jaredjacobs** on 2011-07-14 at 04:41 PM_

---

I understand and concur with the points made. However, the alternatives discussed don't address the case in which you have a broadly typed collection that you know only contains (or only _should_ contain) a particular narrower type that you want to pass to a method that requires a collection of the narrower type.

Here's a contrived example. This can happen in situations where you can't easily change the signatures of the methods providing and consuming the collection.

&nbsp;&nbsp;public static void main(String[] args) {
&nbsp;&nbsp;&nbsp;&nbsp;List&lt;Number> ints = generateIntList();
&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("Sum: " + sum(ints)); // compile error!
&nbsp;&nbsp;}

&nbsp;&nbsp;private static List&lt;Number> generateIntList() {
&nbsp;&nbsp;&nbsp;&nbsp;return Arrays.&lt;Number>asList(1, 2, 3);
&nbsp;&nbsp;}

&nbsp;&nbsp;private static int sum(Collection&lt;Integer> ints) {
&nbsp;&nbsp;&nbsp;&nbsp;int total = 0;
&nbsp;&nbsp;&nbsp;&nbsp;for (Integer i : ints) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;total += i;
&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;return total;
&nbsp;&nbsp;}

I don't feel too strongly about this particular issue, so leave it closed if you like. I just felt compelled to explain the original motivation.

Yes, a type filtering call would be just as effective at creating a more narrowly typed collection, but that approach would silently ignore objects of the wrong type.
 
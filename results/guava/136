Add Interner _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=136) created by **blair-ol...@orcaware.com** on 2009-04-07 at 06:01 PM_ --- According to this thread, Google has an Interner class: http://groups.google.com/group/google-collections-users/browse_thread/thread/5989b42be75f9d5c#msg_140ae5d0060794ff This would be useful and a welcome addition to the open-source google-collections library.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c1) posted by **kevinb9n** on 2009-08-12 at 06:23 PM_

---

Unfortunately, the status of this remains unchanged. The interner implementation we 
use internally has a serious flaw that makes it unreleasable; we know how to go about 
reimplementing it; but we haven't had/made the time for it yet.

---

**Status:** `Accepted`
**Labels:** `post-1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c2) posted by **jim.andreou** on 2009-08-12 at 06:54 PM_

---

:(
A bit disappointing, I think many of us were looking forward to that...
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c3) posted by **blair-ol...@orcaware.com** on 2009-08-12 at 07:43 PM_

---

Can you make the current code available and what needs to be done with it to fix the
flaw and somebody that wants it enough would do the correct implementation. The
power of open-source :)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c4) posted by **kevinb9n** on 2009-08-12 at 09:09 PM_

---

Sadly... I hate having to say this... but we are not yet set up to be able to accept 
code from non-Googlers. The reasons are complicated internal technical ones that 
can't easily be explained. We'll get there soon, I hope.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c5) posted by **heroldsilversurfer** on 2009-09-09 at 03:17 PM_

---

is there anything hideously wrong with the following code for an interner for
immutable objects? a very simple interner, should be reasonably thread-save but maybe
not high-performance because it uses synchronizedMap instead of ConcurrentHashMap.

unfortuately, the [new MapMaker().weakKeys().makeMap()] does NOT work as expected. it
fails my testcasesand and crashes with OOME, it seems to hold references it should
not hold. this could be a seperate issue, or did i misread the javadocs?

public class Interner {
&nbsp;&nbsp;&nbsp;&nbsp;private static final Map&lt;Object, Object> WEAK_REFERERNCES
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= Collections.synchronizedMap(new WeakHashMap&lt;Object, Object>());
// private static final Map&lt;Object, Object> WEAK_REFERERNCES
// = new }
}
.

}
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c6) posted by **andre.ruediger** on 2009-09-10 at 08:49 AM_

---

shouldn't you use putIfAbsent
(http://java.sun.com/javase/6/docs/api/java/util/concurrent/ConcurrentMap.html#putIfAbsent(K,%20V))
here?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c7) posted by **kevinb9n** on 2009-09-17 at 05:45 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`post-1.0`, `Milestone-Post1.0`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c8) posted by **kevinb9n** on 2009-09-17 at 05:57 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c9) posted by **blair-ol...@orcaware.com** on 2009-11-25 at 06:26 PM_

---

It looks like one could implement an interner using the JSR166 CustomConcurrentHashMap

http://gee.cs.oswego.edu/dl/jsr166/dist/extra166ydocs/extra166y/CustomConcurrentHashMap.html

See this thread discussing it:

http://concurrency.markmail.org/search/?q=intern#query:intern+page:1+mid:4pedmyvl3llhqakn+state:results
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c10) posted by **ian.phillips** on 2010-01-06 at 04:28 PM_

---

Couldn’t this just be built on top of a computing hash map? Like this:

public class Interner&lt;T> {
&nbsp;&nbsp;private final Map&lt;T, T> map = new MapMaker()
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.makeComputingMap(new Function&lt;T, T>() {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@﻿Override
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public T apply(@﻿Nullable T from) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return from;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});
&nbsp;&nbsp;public T intern(T object) {
&nbsp;&nbsp;&nbsp;&nbsp;return map.get(object);
&nbsp;&nbsp;}
}

Although maybe the intern method would need to be synchronised, but other than that (I’ll need to check 
the code/docs for MapMaker to be sure) I don’t see why it needs to be any more complicated than this.

Maybe with options to control the reference type (weak/soft/&c.) of the map, and an associated builder 
class.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=136#c11) posted by **kevinb@google.com** on 2010-04-09 at 04:07 PM_

---

Most users would not want strong references, so using MapMaker in this way involves 
creating not one but _two_ weak references for every interned object. There is a 
clever way to avoid that, but it's nonobvious and possibly suboptimal, and in any 
case, our Interner gives you a nice simple API.

---

**Status:** `Fixed`
 
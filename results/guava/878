Make ReferenceEntries with StrongValueReferences less heavy-weight _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=878) created by **s...@st.informatik.tu-darmstadt.de** on 2012-01-20 at 10:28 AM_ --- Currently, a ReferenceEntry like a WeakEntry in a Cache is quite heavy-weight (in terms of memory-consumption), even if it strongly refers to its value. (I expect weak keys/strong values to be a very common pattern.) This is because the (on my machine) 40 bytes WeakEntry refers to another 16 byte StrongValueReference instead of just storing the reference "unboxed" in the WeakEntry. One way to address this, without breaking the interface of ReferenceEntry, would be to add a specialized WeakEntryWithStrongValue (name could arguably be improved ;-) which implements the ValueReference interface itself. Its getValueReference() method would then simply return "this".  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=878#c1) posted by **wasserman.louis** on 2012-01-20 at 04:04 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Performance`, `Package-Cache`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=878#c2) posted by **wasserman.louis** on 2012-02-20 at 05:15 PM_

---

I'd be interested in doing this, though I might request some help from Dmitris and the memory analysis on Cache that he's already done.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=878#c3) posted by **jim.andreou** on 2012-02-20 at 11:05 PM_

---

Sounds about right, 40 + 16 + a reference (aligned) holding this structure = 64 bytes that I've been measuring. Without looking the code, I vaguely recall a TODO note calling out this possibility, is that right? 

But the important detail is whether the cost can drop to 48 bytes, rather than 56. One would represent a 25% reduction for these caches, the other 'just' 12.5%, which would be able to justify less extra code complexity than the former.

This is a case where an external patch would help. If I'm given a patch, I can easily tell which is the case, or one can use an external memory measuring tool, or other cruder means to measure the space reduction. And Charles would be able, at a glance, to see what kind of complexity we're talking.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=878#c4) posted by **wasserman.louis** on 2012-02-20 at 11:07 PM_

---

I will attempt a patch, and let y'all know if I encounter any particular difficulties.

---

**Status:** `Accepted`
**Owner:** wasserman.louis
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=878#c5) posted by **wasserman.louis** on 2012-02-23 at 09:25 PM_

---

This is somewhat more complex than I'd hoped. I will continue working on it, but it'll require a fair amount of refactoring.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=878#c6) posted by **kevinb@google.com** on 2013-03-12 at 06:43 PM_

---

_(No comment entered for this change.)_

---

**CC:** fry@google.com
 We're unlikely to make significant changes to common.cache. We recommend Caffeine. 
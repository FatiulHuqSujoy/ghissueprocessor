Media type: provide text type constants w/o charset, and how does comparison work? _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=917) created by **j...@nwsnet.de** on 2012-03-01 at 05:14 PM_ --- Text type constants only exist with a specified charset (UTF-8). APIs that want to create a, say, plaintext media type, but receive the charset as an argument, a separate constant without the parameters must be created. It would be very hand if such constants for all those text types would already be there. Also, I often use just the type/subtype pair w/o the parameters to check if, say, an e-mail part is plaintext or HTML using something like `MediaType.PLAINTEXT.equals(someMediaType.withoutParameters())`. An alternative approach would be to compare both the type and the subtype of both objects separately, which is somewhat cumbersome to write and read, but it doesn't require the creation of a new instance without parameters. (Comparison of just the type against a [not yet public] constant is easily done, though.) What is the preferred way to compare just type and subtype? Would an additional instance or static method for that make sense? Or even (I don't like this) the evaluation of wildcard subtypes like "image/*" using string deconstruction?  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=917#c1) posted by **kak@google.com** on 2012-03-02 at 05:10 AM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
**Owner:** gak@google.com
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=917#c2) posted by **gak@google.com** on 2012-03-02 at 05:01 PM_

---

The constants that have the charset are specified that way to make it explicitly hard to serve those types without a charset because serving content without a charset can lead to security issues in browsers that attempt to sniff the charset. Since MediaType is likely to be used most often in code that serves content, that's our default stance. Using withoutParameters() is the intended way to get an instance without the charset for both cases.

E.g.:
MediaType plainText = MediaType.PLAIN_TEXT_UTF_8;
or
boolean isHtml = someMediaType.withoutParameters().equals(MediaType.HTML_UTF_8.withoutParameters());

Though, again, it is not recommended to strip the charsets off of MediaTypes as 80% of the time you _do_ want to be serving these types as UTF_8.

---

**Status:** `WorkingAsIntended`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=917#c3) posted by **j...@nwsnet.de** on 2012-03-05 at 09:17 AM_

---

Doesn't the overhead of `MediaType.create(String, String, Multimap<String, String>)`&nbsp;justify the introduction of a constant instead of calling `withoutParameters`, at least in project-local code?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=917#c4) posted by **j...@nwsnet.de** on 2012-03-14 at 03:14 PM_

---

As I think it relates to this request, take a look at change 66b3ca82937b at https://github.com/google/guava/commit/66b3ca82937bd6194039cda7e58825eb1d3b6683

It introduces both wildcards as well as a method `is`&nbsp;to be used for comparison.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=917#c5) posted by **gak@google.com** on 2012-03-14 at 04:51 PM_

---

I'm not quite sure what you're driving at with comment #﻿4, but yes, if you really need to consistently work with a charsetless version of the MediaType, creating a constant in your local project is certainly justified. I just don't believe that putting in MediaType is the proper place for the reasons stated in #﻿2.

Though, it's probably worth noting that creating a new instance is likely inexpensive and I would avoid optimizing around that until it has been shown that it is a particular bottleneck.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=917#c6) posted by **j...@nwsnet.de** on 2012-03-14 at 05:32 PM_

---

I was basically referring to my initial mention of wildcards and that it seemed comparison via just type, subtype, or using `withoutParameters`&nbsp;would sufficiently cover this. Seeing the mentioned change, I was surprised that wildcards have been introduced.

This was partly meant as a continuation pointer for users that stumble upon this request (and me) and might still be wondering about wildcards and ways to deal with them/reproduce wildcard matching.

So the "official" way to e.g. check for any kind of image now seems to be `mediaTypetoCheck.withoutParameters().is(MediaType.ANY_IMAGE_TYPE)`&nbsp;instead of `"text".equals(mediaTypeToCheck.type()`, is that correct?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=917#c7) posted by **gak@google.com** on 2012-03-14 at 05:43 PM_

---

mediaTypeTocheck.is(ANY_IMAGE_TYPE) should work just fine.

text/html -> false
image/jpeg -> true
image/svg+xml; charset=utf-8 -> true
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=917#c8) posted by **j...@nwsnet.de** on 2012-03-14 at 06:13 PM_

---

Aaah, I see! Great, thanks for clearing this up.
 
ImmutableMap.Builder should have remove method Without `Builder.remove()` method, it will be very inefficient to create a new immutable map from an existing immutable map removing a key: .  In general, we recommend mutating a mutable map type instead of trying to pretend a builder is a mutable map.

So, use a `LinkedHashMap` or `HashMap`, perform your additions/removals, and then `ImmutableMap.copyOf(mutableMap)` Then one has to make a mutable copy of an existing map, even the original map is mutable, because we may not want the original map to be mutated. ImmutableMap.copyOf(copy);
. That seems fine to me.

If you're super worried about performance, you can add the entries from the existing mutable map to the `ImmutableMap` builder in a loop, and just _don't_ add the entry if it has a certain key (the key you want to "remove").

Or, alternatively, use a lazy `Maps.filterKeys()` and `Predicate.not(badKey)`, wrapped in `ImmutableMap.copyOf()`. Thanks. It is fine to me as well.

If I'm supper worried about performance, I have to use `ImmutableMap.builderWithExpectedSize()` before adding entries in a loop, otherwise the performance is almost the same as making a mutable copy. Is my understanding correct? That sounds approximately correct, yes. 
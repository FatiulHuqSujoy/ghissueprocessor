Bug in MapMaker when using maximumSize _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=564) created by **morten.hattesen** on 2011-02-28 at 02:51 PM_ --- This issue is reported against guava-r08.jar It turns out, that restricting the size of a map using MapMaker.maximumSize() in certain circumstances will have a strange consequence that certain map keys may not coexist in the map. The bug can be reproduced consistently, see attached JUnit test case. When building a map using... &nbsp;&nbsp;Map&lt;Integer, String> map = new MapMaker().maximumSize(5).makeComputingMap(intToString); ... the resulting seem "unwilling" to simultaneously hold map entries with the keys Integer(1) and Integer(2). When adding one, the other entry disappears. This behavior seems to occur for maximumSize < 6. The attached test case fails test method testSizeMaximum5() with the error: "java.lang.AssertionError: expected:&lt;3> but was:&lt;2>"  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=564#c1) posted by **fry@google.com** on 2011-02-28 at 03:16 PM_

---

Currently maximum size is enforced on a per-segment basis (with each segment limiting its size to a fraction of the total maximum size). The behavior you are seeing is thus expected: when a segment is full it will evict an entry on addition of a new entry.

Note that maximumSize puts an upper bound, but allows for eviction to occur before the total number of elements reaches that bound.

---

**Status:** `WorkingAsIntended`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=564#c2) posted by **morten.hattesen** on 2011-02-28 at 03:41 PM_

---

OK, my bad.

I take it that the JavaDoc of the maximumSize() method allows for the current behavior:
http://guava-libraries.googlecode.com/svn/tags/release08/javadoc/com/google/common/collect/MapMaker.html#maximumSize(int)
"While the number of entries in the map is not guaranteed to grow to the maximum, ..."
 
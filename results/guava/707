Basic support for DOM-XML _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=707) created by **ogregoire** on 2011-09-14 at 09:01 AM_ --- What about XML? I didn't see anything that suggests the org.w3c.dom API wouldn't be supported by Guava. For start, what I typically see is something similar to LineProcessor: - XmlProcessor being a one-method interface: interface XmlProcessor&lt;T> { T process(Document); } - some default suppliers of javax.wml.parsers.DocumentBuilder to - IO methods that support parsing to DOM directly and that also accept the DocumentBuilder I also see a few helper methods returning easy to use objects: - Iterable&lt;Element> childrenElementsOf(Element[, Predicate&lt;Element>]) - Map&lt;String,String> attributesOf(Element[,String(namespace)]) being a Map view of the attributes. - A few Element/Attribute predicates I know that this can become quite quick huge, that's why I suggest to strongly limit the scope this API, if it becomes accepted.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=707#c1) posted by **fry@google.com** on 2011-12-05 at 06:40 PM_

---

We don't want to get into XML processing.

---

**Status:** `WontFix`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=707#c2) posted by **david.nouls** on 2012-12-07 at 09:05 AM_

---

I've implemented (for the scope of my own, closed source project) a possibility to use DOM nodes as Iterables - I mostly based it on what I saw in the guava collect package.

I have some methods that allow me to navigate through the DOM tree with a pure linear Iterable, or let me find siblings, ancestors of a node, only go through Elements, ... etc. This makes DOM code much nicer to use. And all this is using lazy operations, so no need to put Nodes in Lists or anything, so I can easily use the Iterables APIs of Guava to make some very easy to read code.

I mainly wrote this because people in my team tend to use the wrong API's all the time with a big performance impact as a result. In some cases it caused a 5x speedup by using getFirstChild. getNextSibling instead of using a NodeList approach in the Iterables.

So, it would be nice if something similar would be available in Guava ... but it is easy to implement outside as well.
 
FluentPredicate _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=334) created by **blank101** on 2010-02-26 at 11:00 PM_ --- I find the following very annoying: Predicates.and(Predicates.not(Predicates.in(someCollection)),Predicates.not Null()) ...etc. I wrote a (very bare) wrapper implementation of Predicate (attached), which provides for all the methods in Predicates in a functional style (ala the similar proposal for Iterables), which allows some thing like: ComposablePredicate.in(someCollection).not().and(Predicates.notNull()) which I think is more clear than the example above. I don't see this in the dead ideas or otherwise closed issues, so I thought I'd suggest it. The one implementation decision I'm not sure about is an immutable vs mutable delegate, but I'm pretty happy with the immutable version. N.B.: this composition problem also applies to functions: e.g., Functions.blah(Functions.blah(Functions.blah( ... ) becomes rapidly unreadable for any substantive number of functions.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c1) posted by **kevinb@google.com** on 2010-02-27 at 12:48 AM_

---

We have great "fluent" versions of Predicate, Function and Iterable, and I hope we can 
release them soon. The only real problem is that these classes are THE hardest things 
to name EVER.

Right now the best I have come up with is FluentPredicate, FluentFunction and 
FluentIterable. :(
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c2) posted by **blank101** on 2010-02-27 at 05:41 AM_

---

Fluid_, perhaps - one character shorter, aye? Or perhaps Flow_. Maybe even Flo*, if
it doesn't conjure up to much an advertising theme.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c3) posted by **david.henderson** on 2010-03-09 at 10:31 PM_

---

The term "Chainable" is used in some contexts.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c4) posted by **blank101** on 2010-03-09 at 11:27 PM_

---

I know IDEs, auto-completion, blah-blah-blah...but "Chainable" is really long.

The "Fluent" moniker seems to have some traction, and the original metaphor for that 
moniker seems to explicitly include the notion of "Flow"; I'm not particularly fond of 
"Fluent" (too ambiguous, too long), hence would prefer "Flow" as the prefix (and that's 
how I've named my parallel Collections framework), but it would be a reasonable name.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c5) posted by **cgdecker** on 2010-03-09 at 11:55 PM_

---

"Flow" is a noun or a verb, not an adjective which can describe something. 
"FlowPredicate" does not read naturally (to me, anyway) and saving 2 characters doesn't 
seem worth using a name that neither reads well nor describes the class as well as 
Fluent. That said, I think if Kevin felt this general category of names was what he 
really wanted to use for these classes he'd just have used Fluent and been done with 
it. I imagine he's looking for something more on the lines of Ordering.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c6) posted by **blank101** on 2010-03-10 at 01:22 AM_

---

You mean, something more like:

Function -> Composite
Predicate -> Filter

?

I still think I consistent prefix would be preferable. The basic behavior of all these 
things (functions, predicates, iterables, iterators, etc) remains largely consistent with 
their "parent" objects, but each has the same sort of sequentially callable 
transformation methods appended.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c7) posted by **daniel.yokomizo** on 2010-03-10 at 07:58 AM_

---

This fluency support could be extended to more than predicates/functions/iterables.
In my Collections Framework I use (using the equivalent guava names):

interface Filterable&lt;E> {
&nbsp;&nbsp;&nbsp;&nbsp;Filterable&lt;E> filter(Predicate<? super E> p);
}
interface Transformable&lt;E> {
&nbsp;&nbsp;&nbsp;&nbsp;&lt;F>Transformable&lt;F> transform(Function<? super E,? extends F> f);
}
interface Composable&lt;E> {
&nbsp;&nbsp;&nbsp;&nbsp;&lt;D>Composable&lt;D> compose(Function<? super D,? extends E> f);
}
interface FluentFunction&lt;D,E> extends Transformable&lt;E>, Composable&lt;D>, Function&lt;D,E> 
{
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Functions.compose
&nbsp;&nbsp;&nbsp;&nbsp;&lt;F>FluentFunction&lt;D,F> transform(Function<? super E,? extends F> f);
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Functions.compose
&nbsp;&nbsp;&nbsp;&nbsp;&lt;C>FluentFunction&lt;C,E> compose(Function<? super C,? extends D> f);
}
interface FluentPredicate&lt;E> extends Filterable&lt;E>, Composable&lt;E>, Predicate&lt;D,E> {
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Predicates.and
&nbsp;&nbsp;&nbsp;&nbsp;FluentPredicate&lt;E> filter(Predicate<? super E> p);
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Predicates.compose
&nbsp;&nbsp;&nbsp;&nbsp;&lt;D>FluentPredicate&lt;D> compose(Function<? super D,? extends E> f);
&nbsp;&nbsp;&nbsp;&nbsp;FluentPredicate&lt;E> and(Predicate<? super E> p);
&nbsp;&nbsp;&nbsp;&nbsp;FluentPredicate&lt;E> or(Predicate<? super E> p);
&nbsp;&nbsp;&nbsp;&nbsp;FluentPredicate&lt;E> not();
}
interface FluentIterable&lt;E> extends Transformable&lt;E>, Filterable&lt;E>, Iterable&lt;E> {
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Iterables.filter
&nbsp;&nbsp;&nbsp;&nbsp;FluentIterable&lt;E> filter(Predicate&lt;E> p);
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Iterables.transform
&nbsp;&nbsp;&nbsp;&nbsp;&lt;F>FluentIterable&lt;F> transform(Function<? super E,? extends F> f);
}
interface FluentIterator&lt;E> extends Transformable&lt;E>, Filterable&lt;E>, Iterator&lt;E> {
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Iterables.filter
&nbsp;&nbsp;&nbsp;&nbsp;FluentIterator&lt;E> filter(Predicate&lt;E> p);
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Iterables.transform
&nbsp;&nbsp;&nbsp;&nbsp;&lt;F>FluentIterator&lt;F> transform(Function<? super E,? extends F> f);
}
interface FluentCollection&lt;E> extends Collection&lt;E>, FluentIterable&lt;E> {
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Collections2.filter
&nbsp;&nbsp;&nbsp;&nbsp;FluentCollection&lt;E> filter(Predicate&lt;E> p);
&nbsp;&nbsp;&nbsp;&nbsp;// equivalent to Collections2.transform
&nbsp;&nbsp;&nbsp;&nbsp;&lt;F>FluentCollection&lt;F> transform(Function<? super E,? extends F> f);
}
// and so on, giving FluentMap, FluentList, FluentQueue, FluentSet, FluentSortedMap, 
FluentSortedSet
// we can add FluentFuture, FluentThreadLocal, etc. as we would like

class Fluency {
&nbsp;&nbsp;&nbsp;public static &lt;E> FluentIterable from(Iterable&lt;E> iterable);
&nbsp;&nbsp;&nbsp;public static &lt;E,F> FluentFunction from(Function<? super E,? extends F> function);
// and so on
}
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c8) posted by **blank101** on 2010-03-10 at 01:27 PM_

---

That is very similar to what I do; I do put the additional interface requirement for 
FluentIterable that the iterator() method return a FluentIterator. Also, for the various 
collections, I included fluent versions of the mutators (fluentAdd, fluentRemove, etc).

Also my base interfaces (the Filterable, Composable, Transformable equivalents) I 
covered more of the functionality in the Predicates, Functions, Iterables, Iterators 
static helper classes.

What are the specifics of your Fluent map? It's not as obvious to me what's being 
filtered/transformed/etc with both keys and values.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c9) posted by **daniel.yokomizo** on 2010-03-10 at 10:09 PM_

---

As you said I also put the additional constraints of FluentIterable returning a 
FluentIterator on iterator(). FluentMap is similar, it returns a FluentSet on keySet() 
and entrySet() and a FluentCollection on values(). Also it implements 
Filterable&lt;Map.Entry&lt;K,V>> but not Transformable (it's not a pressing need for me).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c10) posted by **finnw1** on 2010-04-23 at 01:46 PM_

---

So will you also be renaming "Ordering" to "FluentComparator"?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c11) posted by **kevinb@google.com** on 2010-04-23 at 02:52 PM_

---

That horse is out of the gate.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c12) posted by **blank101** on 2010-04-23 at 04:35 PM_

---

Indeed; any news on when the other fluent horses will be released?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c13) posted by **fry@google.com** on 2011-01-26 at 10:46 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Accepted`
**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c14) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c15) posted by **fry@google.com** on 2011-12-10 at 03:45 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Base`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c16) posted by **wasserman.louis** on 2012-02-16 at 06:48 PM_

---

_(No comment entered for this change.)_
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c17) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c18) posted by **em...@soldal.org** on 2012-05-08 at 01:19 PM_

---

These are the slowest horses ever!
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c19) posted by **kevinb@google.com** on 2012-05-30 at 07:43 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Addition`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=334#c20) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 Any Progress on FluentPredicates and FluentFunctions? 
 I think this issue is now resolved with the introduction of the `java.util.function.{Predicate, Function, ...}` functional interfaces in Java 8, and with `com.google.common.base.{Predicate, Function, ...}` now extending those interfaces.

For example, [`Predicate`](https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html) has the "fluent" methods [and](https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html#and-java.util.function.Predicate-), [or](https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html#or-java.util.function.Predicate-) and [negate](https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html#negate--), which I think fulfill the OP's original request, as one can now write the following quite 
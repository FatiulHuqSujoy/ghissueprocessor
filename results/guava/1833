Add a Preconditions.chechNoNulls() method for collections _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1833) created by **dimo414** on 2014-08-15 at 04:19 PM_ --- I'd like to verify a collection contains all non-null elements, which currently seems to be most easily done with: for(Object o : c) { &nbsp;&nbsp;checkNotNull(o); } Obviously not terrible, but in particular it loses the one-line assignment benefit of checkNotNull(). I'd like to be able to do something like: List&lt;String> safeList = checkNoNulls(unsafeList);  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1833#c1) posted by **cpovirk@google.com** on 2014-08-15 at 05:59 PM_

---

As it turns out, we have this method internally, but we decided to get rid of it. The justification:

"""
Advice:
- if you're going to NPE immediately anyway, just do nothing.
- if the NPE might only happen _later_, you should probably be making a defensive copy; use Immutable*
- but if you're sure this is what you want, use checkArgument(!foos.contains(null)) or checkArgument(!Iterables.contains(foos, null))
 """

---

**Status:** `WorkingAsIntended`
**Labels:** `Type-Addition`, `Package-Base`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1833#c2) posted by **kevinb@google.com** on 2014-08-15 at 06:01 PM_

---

Basically, asserting facts about the contents of an untrusted _mutable_ collection is unsatisfying. Who knows if that condition once verified will actually remain true? Defensive copies are a good thing, and ImmutableBlah.copyOf() will already do null-checks for you.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1833#c3) posted by **dimo414** on 2014-08-15 at 06:51 PM_

---

Fair points, and thanks for the checkArgument suggestion (though that raises an IAE rather than an NPE, I suppose it's a valid response). Perhaps my use case will explain why I think this would be useful.

I'm implementing a series of forwarding collections (Map, Set, Multimap, Multiset) that have some additional behavior, one of which is forbidding null keys and values, in the spirit of Guava's immutable collections. However, since the backing collections allow nulls, I have to manually check for nulls before I can safely insert into them.

So for instance, I have to do:

@﻿Override
public boolean addAll(Collection&lt;E> c)
{
&nbsp;&nbsp;for(E e : c) {
&nbsp;&nbsp;&nbsp;&nbsp;checkNotNull(e);
&nbsp;&nbsp;}
&nbsp;&nbsp;return delegate().addAll(c);
}

Rather than simply:

public boolean addAll(Collection&lt;E> c)
{
&nbsp;&nbsp;return delegate().addAll(checkNoNulls(c));
}

Granted, it's not that much worse, and arguably this is an edge-case need, but to me it seemed like a logical Precondition candidate.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1833#c4) posted by **lowasser@google.com** on 2014-08-15 at 06:53 PM_

---

Observation: In Java 8, you could just do c.forEach(Preconditions::checkNotNull);
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1833#c5) posted by **kevinb@google.com** on 2014-08-15 at 06:55 PM_

---

Note that your code isn't robust when c can be modified by other threads. One of the reasons we recommend Immutable*.copyOf() is that we had to go to great pains to get that level of robustness in Guava.
 
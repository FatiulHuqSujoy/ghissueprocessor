Add a built in default value for MediaType in NullPointerTester (and related) _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1669) created by **matt.nathan** on 2014-02-14 at 09:59 AM_ --- It would be nice if Guava could configure the NullPointerTester with a default instance for the MediaType type so we don't have to configure it manually in our tests. MediaType.ANY_TYPE would be fine.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1669#c1) posted by **kak@google.com** on 2014-02-18 at 03:09 AM_

---

_(No comment entered for this change.)_

---

**Owner:** gak@google.com
**Labels:** `Type-Enhancement`, `Package-Testing`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1669#c2) posted by **cpovirk@google.com** on 2014-02-18 at 06:00 PM_

---

Perhaps NullPointerTester (or one of the classes it uses) should be smart enough to look for instances of a class in that class's static final fields.

If we instead handle MediaType specifically, I have some reservations about whether we want to go down the road of adding default values for our types when we don't personally need them. But I don't feel that strongly, and given that this is the first request we've gotten that I can recall, I'm not too concerned.

(Also, Greg points out that a direct usage of MediaType would introduce a testing->net dependency. Kevin and Christian suggest that we won't ever want a net->testing dep, and I agree: There's no concern about circularity.)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1669#c3) posted by **cpovirk@google.com** on 2014-02-18 at 06:02 PM_

---

Ben, is this something that you've considered in the past...?

"Perhaps NullPointerTester (or one of the classes it uses) should be smart enough to look for instances of a class in that class's static final fields."

---

**CC:** benyu@google.com
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1669#c4) posted by **benyu@google.com** on 2014-02-18 at 08:23 PM_

---

There seems to be a few different options. Ordering from the most specific to more generic:
1. Just add default value for MediaType.
2. ArbitraryInstances can look for public-static-final fields of type Foo in class Foo.
3. Why stop there? ArbitraryInstances can also look for zero-arg static factory methods.
4. Why stop there? ArbitraryInstances also can look for public-static-final fields in a class called "Foos".

ArbitraryInstances already look for zero-arg public constructors. So it doesn't seem that out of line to expand the search to constants and factory methods. The use of "Foos" is a bit of strech. Although it seems pretty common that constants and factories live in a Foos class instead of the original Foo?

We probably need to get determinism right. Whichever field/factory we choose to use among multiple choices, we don't want the choice to change when the source code doesn't change. So picking the first from getDeclaredFields() or getDeclaredMethods() isn't ideal.

Sorting by field/method name possibly will suffice.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1669#c5) posted by **cpovirk@google.com** on 2014-03-03 at 03:11 PM_

---

We went with "2. ArbitraryInstances can look for public-static-final fields of type Foo in class Foo."

https://github.com/google/guava/commit/deff3845959d6be8b6eeb802d76b2b7637d1cc02

---

**Status:** `Fixed`
**Owner:** benyu@google.com
**Labels:** `Milestone-Release17`
**CC:** -benyu@google.com
 
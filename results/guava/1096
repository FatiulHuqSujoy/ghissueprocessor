Release 13.0 breaks builds that don't provide Google's Findbugs/JSR-305 jar themselves _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1096) created by **j...@nwsnet.de** on 2012-08-06 at 12:32 PM_ --- From release 12.0 to 13.0, the scope of the dependency on `com.google.code.findbugs`/`jsr305`/`1.3.9`&nbsp;has been changed from default ("compiled") to "provided". I haven't found this change in the source `pom.xml`&nbsp;files, yet, but the `guava-{12, 13}.0.pom`&nbsp;differs in this regard. As a result, code that depends on that artifact because it e.g. uses `javax.annotation.Nullable`&nbsp;breaks. Was this (AFAIK undocumented) change intentional?  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c1) posted by **wasserman.louis** on 2012-08-06 at 01:29 PM_

---

Trying to figure out exactly which thing is which -- jsr305 shouldn't be needed by Guava at runtime, because @﻿Nullable isn't retained at runtime, I think. Are you saying that 13.0 does, or doesn't need jsr305 to be provided by the user? Bleah, words.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c2) posted by **stephan202** on 2012-08-06 at 01:55 PM_

---

Irrespective of retention type, the Java spec does not require referenced annotations to be on the classpath; if not found the classloader should silently drop them. At least, that's my understanding.

It seems to me that the issue described here is different: some users might have started using the JSR-305 annotations in their _own_ code without explicitly declaring the findbugs dependency in their own pom.xml. In that case the problem is with the user, cause their project configuration is effectively incomplete.

From my perspective the change to Guava (i.e., changing the JSR-305 scope to "provided") was the right this to do.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c3) posted by **stephan202** on 2012-08-06 at 01:56 PM_

---

Relevant background information: http://stackoverflow.com/questions/3567413/why-doesnt-a-missing-annotation-cause-a-classnotfoundexception-at-runtime
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c4) posted by **j...@nwsnet.de** on 2012-08-06 at 02:38 PM_

---

In my case, the code (_my_ code) did not _compile_.

After all, I added the JSR-305 as a dependency to fix that.

I agree that the change makes sense (and that Guava itself shouldn't provide the dependency), but I wonder if that should've been documented.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c5) posted by **stephan202** on 2012-08-06 at 02:49 PM_

---

At some level this is a discussion about whether a Maven project's dependencies are part of its API. Hmm. I'm inclined to answer that question in the negative.

Still, I do agree that this is a user-visible change, so a release notes entry mentioning the caveat described here makes sense.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c6) posted by **wasserman.louis** on 2012-08-06 at 02:57 PM_

---

I concur that it's worth documenting, but I'm not sure my Maven fu is up to describing the nature of the change accurately. Can anyone suggest wording?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c7) posted by **cgdecker** on 2012-08-06 at 03:41 PM_

---

Perhaps this should be documented (though it would unfortunately just be more noise for users who aren't depending on the transitive dependency), but maybe we should wait until we've figured out if this was the right change? See issue 1018.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c8) posted by **stephan202** on 2012-08-06 at 03:44 PM_

---

How about this:

"Guava depends on com.google.code.findbugs:jsr305. As of Guava 13 this dependency is declared in the "provided" scope. Since it is no longer "compile" scoped, Guava 13 will no longer automatically add JSR-305 to the build classpath. Thus, if your project also relies on JSR-305, then please make sure that your pom.xml explicitly declares this dependency."

There may be a conflict between clarity and conciseness here. Am I too verbose?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c9) posted by **SeanPFloyd** on 2012-08-06 at 03:44 PM_

---

Maven dependencies can be marked as &lt;optional>, which might be the best fit.
see http://maventest.apache.org/pom.html#Dependencies
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c10) posted by **cpovirk@google.com** on 2012-08-06 at 04:09 PM_

---

I like Stephan's phrasing. Let's see what comes of Sean and Matt's suggestion to use &lt;optional>, tracked on the issue 1018 thread that Colin linked to.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c11) posted by **kurt.kluever** on 2012-08-07 at 05:22 PM_

---

Christian will update the relevant docs.

---

**Status:** `Accepted`
**Owner:** cgruber@google.com
**Labels:** `Package-General`, `Type-ApiDocs`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c12) posted by **cgruber@google.com** on 2012-08-07 at 07:46 PM_

---

I've changed the language and added a note as to the need to declare your own copy of JSR-305 and put it rather repetitively in each section on UsingGuavaInYourBuild, so no one misses it. Note that this should have been the practice already - if you use something, you should directly depend on it.

---

**Status:** `Fixed`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c13) posted by **cgruber@google.com** on 2012-08-07 at 08:05 PM_

---

Docs updated and both open-source bugs relevant to this closed (one with
working as intended, the other (doc centric) as fixed)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c14) posted by **cgruber@google.com** on 2012-08-07 at 08:07 PM_

---

Aah - ignore that last comment - it was meant to be a reply to another
e-mail.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c15) posted by **j...@nwsnet.de** on 2012-08-08 at 10:23 AM_

---

Thanks.

> if you use something, you should directly depend on it.
> I agree, of course. I guess I assumed `Nullable`&nbsp;became available with Java 7 or something, and its package name (`javax.annotation`) is rather misleading.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c16) posted by **cgruber@google.com** on 2012-08-08 at 03:15 PM_

---

No problem.

What's probably more misleading is that we got this from Findbugs, since we did that only because Findbugs had the only stable packaged maven artifact for JSR-305 around. It would be much nicer if the core JCP output API jars were all added as first-class maven artifacts. 

Christian.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1096#c17) posted by **toellrich** on 2012-08-08 at 07:09 PM_

---

What about the fact that JSR 305 is dead in the water? Now that Eclipse 3.8/4.2 supports null analysis annotations I would love to start using them as well and I would love to use the same ones as Guava for obvious reasons (The Eclipse compiler would handle Guavas code as if it was my own telling me which parameters can be null and which nonnull). Unfortunately, because JSR 305 is not going anywhere I will never be able to convince the powers that be at my company to allow usage of those annotations (these guys are very obsessed about not using any "beta" or "alpha" software). Also, one of the annotations (ParametersNonNullByDefault) is a bit of a misnomer since it covers return values as well. Does anybody at Google know what the status of this JSR really is?
 
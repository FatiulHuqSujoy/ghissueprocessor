Guava for Android and Guava for the JRE need separate Maven namespaces Guava for Android is lacking classes that are provided in Guava for the JRE (notably around support for Java 8 in the immutable collections). The two are not interchangeable. Maven's dependency resolution looks at group and project IDs, and then attempts to find the most recent version. Because the only difference between the android and JRE versions of guava is the version number, this can result in Java 8 projects getting the Android version of Guava (and, presumably, vice versa). For reference, at the time of writing [search.maven.org](http://search.maven.org/#search%7Cga%7C1%7Cguava) believes the most recent guava release is the one for Android. ![guava-oops](https://user-images.githubusercontent.com/28229/29334655-14dc5d96-8200-11e7-8276-ebdd616ef661.png) The correct thing to do is give the Android version a different project ID.  Seems like a classifier would be far more appropriate. That would also work, so long as the JRE version retained the default classifier (which I think is `jar`). See #2839, as well. #2839 could also be resolved by adding a classifier. Expecting just the version string to work won't work. Can we please get a 23.1 release that just adds the classifiers and stops projects breaking? Our understanding was that classifiers do not solve the problem. I'll see if I can dig up past conversations or test it out. Despite what [Maven's docs](https://maven.apache.org/pom.html) say about `classifier`...

> As a motivation for this element, consider for example a project that offers an artifact targeting JRE 1.5 but at the same time also an artifact that still supports JRE 1.4. The first artifact could be equipped with the classifier jdk15 and the second one with jdk14 such that clients can choose which one to use.

...we found that Maven will happily permit a project to depend on both `com.google.guava:guava:jar:22.0` and `com.google.guava:guava:jar:someclassifier:22.0`.

As best we can tell, the only reliable solution is for users to manually request `<max-version-needed>` or, if they're running on Android/Java7, `<max-version-needed>-android`.

If there's another scheme that could work, we'd be happy to hear about it. Then pick a different project ID :) The problem right now is that more and more projects are potentially being broken. A different project ID has the same effect as a different classifier, as far as we can tell. Using the right terminology now :)

So the maven namespace is segmented by group id (which can be `com.google.guava`), artifact id (which is currently `guava` in both cases) and version (`23` or `23-android` right now). The correct solution, as pointed out, is to use a classifier, but apparently this is hopelessly broken.

The next best solution is to change the artifact id. This will lead to a situation where people might end up depending on both `guava` and `guava-android`. A group id can have many different artifact ids without them clashing --- if they couldn't then maven as we know it couldn't work. @cpovirk @shs96c I'm not sure I understand what the problem is with a project potentially depending on both `guava` and `guava-android`. Can you elaborate further for me? @jbduncan, it depends on what you're targeting, and what gets put into your classpath:

- An app targeting android might end up with the JRE version of guava. In that case, someone might mistakenly depend on methods that aren't available in the Android SDK (eg. the `Stream` APIs)

- An app targeting desktop guava might not compile properly because they depend on classes that aren't present in the android flavour.

Because the packages are the same, whichever is first on the classpath will be selected. In some cases, this will work as expected. In some cases it won't. Because maven's dependency resolution is "novel" at best, which you get is a bit of a crapshoot. Unless you use version pinning, the version dependency appears to be advisory at best. Our thinking was that it was pretty bad for people to be able to depend on both `guava` and `guava-android`, for the reasons @shs96c lays out.

What I probably need to understand better are the drawbacks of the current versioning scheme. It sounds like you want to be able to tell Maven "depend on the most recent version?" If we could use a version like `23.0-server` to make it "more recent" than `23.0-android`, as discussed in #2839, I think we could solve that problem[*]. Are there other problems?

[*] mostly: It still requires that you have at least one dep on `23.0-server`. If you don't but you do have deps on `22.0-server` and `23.0-android`, Maven is still going to do the wrong thing. Maybe you can ensure that you have that dep (or maybe there's a way to tell Maven "ignore which version everyone specifies, and just give me the max?"). But at that point, I'm wondering if it's easier to just tell Maven to use that specific version directly. @cpovirk yes there's a problem. Doing that will break android projects.
 @cpovirk, I think your understanding of maven dependency resolution isn't quite right. It does a BFS of dependencies, picking the first one that it finds. Thus, even if something like selenium declares a dep on "23.0-server", unless it's "closer" to the build than the one that specifies "19.0", version 19 will be selected. RE: "Doing that will break android projects": Ah, you're saying that, if an Android project tells Maven "use the most recent available version of Guava," it will then get the server version? Good point.

RE: "maven dependency resolution": Thanks, I am aware of the BFS. Above you said: "Maven's dependency resolution looks at group and project IDs, and then attempts to find the most recent version." I was figuring that you were using something like [Gradle's "newest" strategy](https://docs.gradle.org/3.3/userguide/dependency_management.html#sub:version_conflicts) or perhaps some plugin that automatically updates your project to depend on the newest versions (update: like [this](http://www.mojohaus.org/versions-maven-plugin/))? I'm trying to understand the places where it matters which version Maven considers to be "newest." Re: Android. Yes, that's exactly what I'm saying. It's a thorny problem. Re: "guava-server". Not all java implementations are server-side. I think `guava` and `guava-android` are clearer. If you want to really spell out that one version is for the JRE, `guava-jre` seems to more accurately describe the version. Re: "maven dependency resolution" Glad you know about the full horror of how maven selects versions. It's good to have company :) 

I'm just getting reports from users who are using maven's default versioning strategy (not necessarily using gradle). I guess they're using [aether](https://wiki.eclipse.org/Aether/What_Is_Aether).

You know, GH could really do with threading of comments. In case it isn't clear, this particular issue Selenium was having was due to leaving the version as open ended:

 <dependency>
 <groupId>com.google.guava</groupId>
 <artifactId>guava</artifactId>
 <version>[23.0,)</version>
 </dependency>

This allows selenium to specify the minimum required version of guava but anticipating that we will still be compatible with future versions of guava that another project may specify. The problem we encounter is that if another project depends upon us and doesn't specify their version of guava, maven may choose the 'latest' it finds in central... which by timestamp happens to be 23.0-android.

Having users not accidentally include both android and jdk 8 versions is certainly a problem... but solving it in this way feels like bad maven practice. It seems like it's a bug for maven to try to figure out a way for everyone to identify two packages that are incompatible with each other.
 (I'm sure I don't know the _full_ horror of Maven dependencies yet, hopefully ever :))

True, "server" is the wrong word. I think that "jre" may technically mean "Oracle," which isn't necessarily what we mean, either. Hmm. (Update: Admittedly "Android" is itself not the best term, since we advise Java 7 users to use it, too.)

Sounds like Aether may be ["Maven Artifact Resolver"](http://maven.apache.org/resolver/) nowadays? The docs best docs seem to be [this Wiki](http://wiki.eclipse.org/Aether). `jre` just means `Java Runtime Environment`. IBM ships one too, and the OpenJDK has a "jre" package . I _think_ it's an accepted industry term.

Yes, that's the aether version I was referring to. I think that https://github.com/google/guava/issues/2914#issuecomment-322818286 properly describes the problems that guava users are experiencing, and is what we should be trying to resolve. Oops, I think you're right about "jre." Thanks. Oof, I can see that this is not easy to resolve. Just reading this comment thread is enough to do my head in. :stuck_out_tongue_winking_eye:

@lukeis Do you know if the Selenium team ever considered one of the following workarounds?
1. Use a static version of Guava, and update it periodically.
2. "Shade" Guava into Selenium with a tool like the Maven Shade plugin? @jbduncan we are currently working around the issue by setting the version explicitly to 23.0.

Selenium is highly customizable, in particular with our "Grid / Server" product. It would be a bit unkind to those users to shade a dependency that may need upgrading in a timely manner... we don't always timely upgrade our guava reference and it can be many months between our releases when we all get busy. Thanks, @lukeis. That may have been clear to others but was not to me.

That does sound like a good reason for making sure that the jre artifact is newer than the android artifact. (As @shs96c noted, that doesn't help Android users, but I think we would have broken any Android users who used open-ended ranges when we released 21.0, which didn't come with an Android version.)

I'm guessing that there would still be problems -- say, if Selenium depended on something that requests 23.0-android, might Maven still pick that over the "newer" 23.0-jre, since it was requested specifically? And of course there's the problem I mentioned above in which someone wants 24.0-android and someone wants 23.0-jre: The only solution is 24.0-jre, which Maven is never going to figure out on its own.

I will keep thinking about all this. @jbduncan: I'm the selenium lead :)

1. Internally we use a static version of guava, but our maven dependency used to pin to `(23,]` (version 23 or later). Prior to that, we didn't specify a range `23.0`, but this caused problems when we replaced usages of guava's `Function` with java 8's.
2. Shading will bloat the jar, and won't resolve this issue for other users. @cpovirk, yeah, making sure that the jre version is released after the android version will mask this issue for many users. Note that if different coordinates are used, in addition to the classpath ordering issue you could run into same-package conflicts in Java 9 if I understand Jigsaw correctly. If Android later supported modules then a module conflict would occur. If you went down this path then using a different package name would be advisable. The current solution avoids that, though has its own problems.

Per @cpovirk's link to the Maven's Version Plugin for update checks, a similar [plugin](https://github.com/ben-manes/gradle-versions-plugin) for Gradle exists using its dependency resolver. I personally prefer the Maven best practice of not using dynamic versions, optionally locking them down, and excluding when appropriate. In Gradle one can use a `resolutionStrategy` to reject a dependency, e.g. if the version has `-android` in it. I never had to do that in Maven though perhaps it has a similar capability.

Probably the ideal solution is to separate the two variants into their own package and coordinates to avoid any conflicts and work side-by-side. That's not an upgrade friendly approach, which I guess is why it wasn't done. Pushing the handling into the build system seems like the next best option. Sorry for the silence here. I'm going to try to discuss this with some others today.

Do you remember what the problems were when you switched from Guava's `Function` to Java 8's? It would also be good to hear from anyone else who is having a problem with this so that we get as good an idea as we can of the tradeoffs between 23.0-jre and 23.0. (The people I've been talking to have been recommending against version ranges, given that a build that works today might not work tomorrow when the dependency releases a new version. They say that a user can always force a newer version of a dependency when necessary. But I guess the upside of version ranges is that they increase the odds that Maven gives you the right version automatically? (And there's also the point you made about `Function`, which I'd like to understand better.)) Re: https://github.com/google/guava/issues/2914#issuecomment-323814922 In Guava 20, `com.google.common.base.Function` did not extend `java.util.function.Function`. In Guava 21, it did. As a library that people depend on, we switched our API to accept the Java 8 `Function` rather than Guava's.

We upped our guava dependency to version 21. Projects using selenium and guava as a dependencies, however, did not pick up this change --- because of the way that maven dependency resolution works, the project would also need to bump their guava version to 21 or more. 

Fortunately, the selenium project had a way of letting maven know that we actually needed version 21 --- it was more than advisory. We set the version range to be _at least_ version 21, but left the upper bound open so that projects that depend on selenium could use a compatible later version of guava too. Longer writeup here: https://docs.google.com/document/d/1NYGbfz56C0Oh4IGymXjeQUVK4FcRiqDbpc4vGLnDMrY/edit?usp=sharing

Comments welcome. I'm currently thinking that we should switch to the "24.0-jre" pattern, though we'd probably wait for Guava 24. I think the root problem here is the difference between Java 8 and Android as base SDKs. Java 9 is coming in September, and will widen the gap further.

One solution is to shade the Android library into a different package. Libraries that expect to run on Android can make the move to the new package. Those that expect to run on the JRE need do nothing (or make a breaking change, and make _everyone_ make a choice).

On an Android device, it'll be clear that a dependency on the JRE version is a terrible mistake. However, because of the way that the JRE works, if a JRE-based library of application depends on the Android version, all that will happen is that the classpath will be longer, and deployable artefacts will be larger. Disk space is cheap. Also, why not just distribute the Android version of guava as an AAR, and not as a JAR? Given the fact that maven version/artifact resolution is not going to be
sufficient to, in and of itself, note guava-jre and guava-android as
incompatible, can we use some approach like maven-enforcer or whatever to
supply constraints, such that a project won't have both in its deps graph?
The key as I see it is that these are incompatible and there is no way, in
the raw dependency mechanism, to specify that incompatibility.

On Thu, 31 Aug 2017 at 07:53 Simon Stewart <notifications@github.com> wrote:

> Also, why not just distribute the Android version of guava as an AAR, and
> not as a JAR?
>
> —
> You are receiving this because you are subscribed to this thread.
> Reply to this email directly, view it on GitHub
> <https://github.com/google/guava/issues/2914#issuecomment-326320674>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/AAUN4i_jPM3icsPvbg7Fs5UTQ0qyg3dOks5sdsjMgaJpZM4O4ET0>
> .
>
 > I think the root problem here is the difference between Java 8 and Android as base SDKs. Java 9 is coming in September, and will widen the gap further.

(We will likely keep the main artifact on Java 8, but I don't know that we've made that decision for certain. It's similar to how we stayed on Java 6 and (IIRC) upgraded straight to Java 8 without releasing a Java 7 version.)

> One solution is to shade the Android library into a different package. Libraries that expect to run on Android can make the move to the new package. Those that expect to run on the JRE need do nothing (or make a breaking change, and make everyone make a choice).

We're trying to serve the case of cross-platform libraries, too. It's possible that they could fork or preprocess their code, too, of course, but that has its own tradeoffs.

Ideally I should note this in the linked doc someday.

> Also, why not just distribute the Android version of guava as an AAR, and not as a JAR?

We also promise that it's usable as on the JRE (for Java 7 users). (I don't know enough about AAR to say more than that.)

> Given the fact that maven version/artifact resolution is not going to be
> sufficient to, in and of itself, note guava-jre and guava-android as
> incompatible, can we use some approach like maven-enforcer or whatever to
> supply constraints, such that a project won't have both in its deps graph?

My understanding has been that users can opt into such enforcement themselves, but we have no way of doing it for them. If users know enough to take action, the solution is generally simple enough (at least once we switch to `-jre`). The hard part is getting it right automatically most of the time. > We also promise that it's usable as on the JRE (for Java 7 users). (I don't know enough about AAR to say more than that.)

AAR is an Android-specific way of packaging a library. If you've got something that is only meant for an Android app, that's the best way to distribute it.

https://gist.github.com/lopspower/6f62fe1492726d848d6d might be useful. The only advantage would be to bundle the ProGuard rules so they're used
automatically which seems very low value compared to now shipping 3
artifacts (jdk8, jdk6/7-ish, and android).

On Fri, Sep 1, 2017 at 1:43 PM Simon Stewart <notifications@github.com>
wrote:

> We also promise that it's usable as on the JRE (for Java 7 users). (I
> don't know enough about AAR to say more than that.)
>
> AAR is an Android-specific way of packaging a library. If you've got
> something that is only meant for an Android app, that's the best way to
> distribute it.
>
> https://gist.github.com/lopspower/6f62fe1492726d848d6d might be useful.
>
> —
> You are receiving this because you commented.
>
>
> Reply to this email directly, view it on GitHub
> <https://github.com/google/guava/issues/2914#issuecomment-326642112>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/AAEEEWTU-6QCj_ebF-RlcVAaj7MoP0Dhks5seEImgaJpZM4O4ET0>
> .
>
 You'd be shipping two: just guava as a jar (targeting java 8, as now), and guava as an aar, which android projects could depend on. That would then contradict this statement by @cpovirk:

> Update: Admittedly "Android" is itself not the best term, since we advise Java 7 users to use it, too. An aar would require the artifact only notation in Gradle. I think that if the explicit extension and transitively unqualified dependency of guava were in the tree, then both would be retrieved. If so, this would require Android users to manually exclude which would require a fix to the user's build. That doesn't seem clearer than using a resolution strategy / enforcer to restrict versions. @cpovirk If we changed `-android` version suffix to `-a`, [maven version comparison rules](https://stackoverflow.com/a/31482463) would treat it as lesser than vanilla release version, i.e. `24.0` (for java 8+) would be considered newer than `24.0-a` (for android / java 7) by maven.
This way we can keep normal guava version normal (and "latest") while providing android / java 7 specific version, too. @yborovikov Isn't `a` intended for `alpha` releases? It could be misleading. @perceptron8 `-a` is indeed _sorted_ the same way as `-alpha` (and `-b` - as `-beta`). however, the meaning of `-a` doesn't have to translate to "alpha", and, unless we publish alpha releases to maven central, should not create much confusion (at least no more than `-android`).

for example, java `build 1.7.0_80-b15` doesn't mislead us into thinking it's a beta release, does it? :)

i believe a single sentence in the `README.md` would be enough to explain the meaning and purpose of releasing both `guava:24.0a` and `guava:24.0`. Downside of using `-a`: it might be sorted before `rc`, etc. and considered a "pre-release" version by various tools (e.g. [mvnrepository](https://mvnrepository.com/artifact/com.google.guava/guava)).
So we could use `-ga` ("google android") suffix instead - this is considered to be equal to no suffix, so `24.0` and `24.0-ga` would be treated as equally fresh by maven.

Checked these (version `xx-ga` and `xx`) against maven-enforcer-plugin's [requireUpperBoundDeps](https://maven.apache.org/enforcer/enforcer-rules/requireUpperBoundDeps.html) rule - neither is considered newer than the other, both can be used interchangeably. `GA` is sometimes used as `General Availability` in version strings. It looks like some JBoss artifacts used this and then switched to `Final`, so perhaps there was an issue they encountered. I wouldn't redefine known suffixes even if they are non-standard. @ben-manes We're trying to achieve something Maven doesn't have a prescribed solution for here.

You're right, `ga` is one of well-known qualifiers (and stands for `general availability`, as you mentioned), and that's the reason this version qualifier gets special treatment (re version comparison) from Maven.

We would be piggy-backing on this [documented](http://maven.apache.org/ref/3.5.0/maven-artifact/apidocs/org/apache/maven/artifact/versioning/ComparableVersion.html) Maven feature:

> strings are checked for well-known qualifiers and the qualifier ordering is used for version ordering. Well-known qualifiers (case insensitive) are:
> 
> - alpha or a
> - beta or b
> - milestone or m
> - rc or cr
> - snapshot
> - **(the empty string) or ga or final**
> - sp

to provide two Guava versions that can be treated equally from version comparison perspective (at least by Maven itself).

This approach should resolve this very issue of Maven version comparison treating `23.0-android` version as the latest over `23.0` one - as stated by the issue submitter.

<sub>As for why JBoss switched from `ga` version suffix to `Final` - personally I have no idea, as well as why do projects use `-ga` or `.Final` suffixes for their vanilla release artifact versions, to begin with. My guess would be some historical reasons (e.g. JBoss docs circa 2004 said: ["We did not want to depend on a specific build tools naming and ordering rules, because those things tend to change over time. While Maven is the popular choice today ..."](https://developer.jboss.org/wiki/JBossProjectVersioning)).</sub> Copied from [the discussion doc](https://docs.google.com/document/d/1NYGbfz56C0Oh4IGymXjeQUVK4FcRiqDbpc4vGLnDMrY/edit#) shared by @cpovirk:

While it's considered bad practice to just tell Maven "use latest version" (or rely on version ranges), there's another practice of assuring (via `maven-enforcer-plugin`) that a library (transitively shared by multiple dependencies) resolves a version no older than what's required by any of the dependencies.

In [our case](https://github.com/uber/tchannel-java) we try to maintain Java 7 compatibility by using android-flavored guava version, and upgrade to 23.0-android broke the (java 8) builds that depend on the project yet try to enforce the latest versions among shared dependencies - `23.0-android` is considered by Maven to be a newer one and `guava-23.0` fails the check.

Arguably, java 7 / android builds will experience similar issues (e.g., trying to use `24.0-android` while some dependencies use `24.0-jre`).

Ideally, Guava would produce two versions that are considered equally fresh by Maven tooling - and there is such option: use `24.0` for vanilla version and `24.0-ga` for android one.

This would allow a project to pick the flavor it needs while enforcing that the latest (among dependencies) version is being effectively used.

(Of course, android builds would still rely on / check that Java 8 features are not being actually used in the execution path of the libraries - but that's unavoidable anyways.) I replied to the latest comment on the doc a while back. I still need to go back and reply to the remaining comments on the doc. Sorry that I'm taking so long. For the moment, in any case, we're trying `-android` and `-jre`. Please let us know if that causes any new trouble. @cpiotr yes it does, it just did for me and I had to chase around java dependency trees for 3 hours to figure it out! I'm using Google Cloud java SDK libraries, and they sometimes use the `-jre` and sometimes use the `-android` as dependencies and if one of the SDK libraries has a more recent guava android version (e.g. 27-android vs 26-jre), SBT will decide to prefer the more recent `-android` over the `-jre`. Which breaks everything.

This postfix concept, is a misuse that is incompatible with what a version is supposed to be in the maven echo-system.

excerpt from `sbt 19.0}
. 
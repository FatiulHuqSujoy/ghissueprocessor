Post an event to a specific object in an EventBus _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=808) created by **goo...@sa.me.uk** on 2011-11-30 at 12:19 AM_ --- I'd like to be able to post an event to an object immediately before registering it. Currently the only way to do that is to have a separate temporary event bus that's only used to post events. (There are synchronization requirements for this, but that can be handled externally to the EventBus).  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=808#c1) posted by **wasserman.louis** on 2011-11-30 at 04:55 AM_

---

Why can't you just call the object's handler method directly?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=808#c2) posted by **goo...@sa.me.uk** on 2011-11-30 at 07:31 AM_

---

To do that I'd need to know which method(s) subscribe to the event.

getHandlersForEventType() and AnnotatedHandlerFinder aren't visible.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=808#c3) posted by **wasserman.louis** on 2011-11-30 at 08:51 PM_

---

...Do you not know the type of the handler object at compile time?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=808#c4) posted by **goo...@sa.me.uk** on 2011-11-30 at 09:07 PM_

---

Yes but not from the class that should post the initial event... I'd have to re-implement AnnotatedHandlerFinder to find the method to call but the EventBus would be doing that too. It shouldn't be necessary to do that twice on every register().
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=808#c5) posted by **wasserman.louis** on 2011-12-03 at 09:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=808#c6) posted by **fry@google.com** on 2011-12-05 at 07:00 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=808#c7) posted by **fry@google.com** on 2011-12-10 at 04:24 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-EventBus`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=808#c8) posted by **kevinb@google.com** on 2012-01-31 at 07:45 PM_

---

I don't think any new API is justified here. Either arranging things so that you know which methods to call directly, or in a pinch using that temporary eventbus, seem like fine alternatives.

---

**Status:** `WorkingAsIntended`
 
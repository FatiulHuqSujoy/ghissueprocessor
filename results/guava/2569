Not getting "standardAddAll" in CollectionHelpersExplained - how much override needed? https://github.com/google/guava/wiki/CollectionHelpersExplained I read it, but I don't quite get it. . How do you know that `standardAddAll` calls `add`? Do you go read the source code? Is it part of the contract in a doc I missed? The reason I ask is that I'm trying to do the same making a "virtual" table, and I'm not sure in how many places I need to override the calls if I'm creating my own columns. columnMap? columnKeySet? All methods with the word "column" in them?  " Is it part of the contract in a doc I missed?" I think so: https://google.github.io/guava/releases/19.0/api/docs/com/google/common/collect/ForwardingCollection.html#standardAddAll(java.util.Collection)

"A sensible definition of addAll(java.util.Collection<? extends E>) in terms of add(E). If you override add(E), you may wish to override addAll(java.util.Collection<? extends E>) to forward to this implementation."
 Reading this particular paragraph...

"A sensible definition of addAll(java.util.Collection<? extends E>) in terms of add(E). If you override add(E), you may wish to override addAll(java.util.Collection<? extends E>) to forward to **this implementation**."

...makes me realise I'm not so sure what "this implementation" refers to.

I'm reasonably sure it refers to `standardAddAll`, but somehow the wording is throwing me off a bit; it's as if "this implementation" could refer to `add` instead.

Wouldn't it be better to reword the paragraph like so?

"A sensible definition of addAll(java.util.Collection<? extends E>) in terms of add(E). If you override add(E), you may wish to override addAll(java.util.Collection<? extends E>) to forward to `standardAddAll`."

(Or would most people understand the original paragraph, and is it just that my... sense?... of the English language a bit unusual? :wink:)
 I can describe what I was trying to do, and maybe that example will help clarify if the wording needs tuning.

I've got a table that fits in my allowed memory. It has a special column "pixels" that has some poorly encoded image data, as well as other regular columns.

I'd like to "pretend" to all downstream consumers that the table doesn't have a column "pixels", but instead actually has (virtual) columns pixel.1, pixel.2... pixel.484 that map to that cell's specific offset pixel value. (I can read the entire column and find the max pixels to find the 484 limit) In my first naive attempt I blew it out to 484 additional columns, but that hit a memory limit. So I read up on ForwardingTables, and it sounded perfect for these sort of "virtual" columns.

But then I ran into this paragraph, and started wondering how many places I would have to override for the virtual columns to be consistent across all method calls - if I had to do `containsColumn` as well as `column` as well as `columnMap` etc.
 
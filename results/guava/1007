Should it be com.google.common.reflect.Reflection or Reflection_s_? _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1007) created by **ullenboom** on 2012-05-18 at 01:59 PM_ --- Usually all utility classes are in plural form.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1007#c1) posted by **SeanPFloyd** on 2012-05-18 at 07:40 PM_

---

They are in plural form iff the singular form is a class or interface name.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1007#c2) posted by **joe.j.kearney** on 2012-05-21 at 10:44 AM_

---

Slightly more generally, I think the word reflection is used in this sense as an uncountable noun, so the plural doesn't exist. Reflection refers here to the process of inspection, not to the results of that process. Compare to the process of reflection (sing.) of light off a mirror, and the reflections (pl.) that you see having reflected off the mirror.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1007#c3) posted by **wasserman.louis** on 2012-05-21 at 07:03 PM_

---

Even if we wanted to change this (and break all current users of Reflection), "Reflections" feels ugly, awkward, and inappropriate.

---

**Status:** `WorkingAsIntended`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1007#c4) posted by **wasserman.louis** on 2012-05-21 at 07:03 PM_

---

Alternately, think of it this way: Sets deals with sets, Futures deals with futures...but Reflection deals with reflection. You don't pluralize "reflection" there.
 
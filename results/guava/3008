LIFO for cache Hi and thanks a lot for your great library! From what I gather [here](https://github.com/google/guava/issues/1487), Guava [caches](https://github.com/google/guava/wiki/CachesExplained) use a first in first out (FIFO) ordering to queue cache requests. I was wondering whether there's a way to make the cache process those requests first that arrived most recently, i.e., *last* in first out (LIFO). Maybe something similar to [ThreadPoolExecutor](https://docs.oracle.com/javase/9/docs/api/java/util/concurrent/ThreadPoolExecutor.html) that can be constructed using a LIFO data structure, as described [here](https://stackoverflow.com/questions/6107609/executor-service-with-lifo-ordering). Best, Matthias  In that thread I was trying to spark a conversation to see if the performance issues could be addressed, and I didn't want to dictate a solution so I offered alternatives. This are of improvement had been shelved long ago and I hadn't had much luck fixing it. Eventually enough time passed that Java 8 seemed to be a good opportunity to rewrite the cache from scratch.

A stack can be fast by using elimination to avoid a contended reference, which is a problem with the CLQ approach. But it still has allocation overhead which adds GC costs. But having a custom data structure allows one to be lossy, which is the primary advantage. The reasoning being that you don't need to record all operations as the more popular entries will dominate anyway. The stack could be made lossy on contention, among other tricks, so it seemed okay to explore. Basically the last hurdle was to drop the safety net of CLQ+counter, which we used for a simple bootstrap, and focus on performance after the features / correctness was satisfactory.

In the [rewrite](https://github.com/ben-manes/caffeine) I use a striped, lock free ring buffer. The ring buffer means that no allocations are required and it is free to detect when full (no extra counter). The dynamic striping allows additional buffers to be added when contention is detected, but not be wasteful otherwise. In benchmarks, reads scale linearly with the number of CPUs and it offers enough of a performance budget that we could focus on other areas (like write throughput, features, and policy algorithms).

So I think that has all been addressed, but it would be nice to optimize Guava since that code will never be deprecated & removed. Thanks Ben for your answer!

For what it's worth I've created a [custom implementation of my LIFO cache](https://codereview.stackexchange.com/questions/182579/stack-for-asynchronous-processing) (which is not trimmed for performing on huge numbers of inputs though). Feedback is very welcome. I see. The LIFO is for a custom scheduler, which isn’t quite the same as a
cache which should be safe to drop anything. It’s definitely better that
you wrote custom code.

Note that CF ignores the mayInterruptIfRunning flag.

On Tue, Dec 12, 2017 at 2:05 AM Matthias Braun <notifications@github.com>
wrote:

> Thanks Ben for your answer!
>
> For what it's worth I've created a custom implementation of my LIFO cache
> <https://codereview.stackexchange.com/questions/182579/stack-for-asynchronous-processing>
> (which is not trimmed for performing on huge numbers of inputs though).
>
> —
> You are receiving this because you commented.
> Reply to this email directly, view it on GitHub
> <https://github.com/google/guava/issues/3008#issuecomment-351004004>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/AAXG9untcrS-VZ6cRNgw3gCXSJSYkkAJks5s_k_YgaJpZM4Q6_yV>
> .
>
 Thanks a lot for the advice. I'll remove the mayInterruptIfRunning flag from the `CompletableFuture`. 
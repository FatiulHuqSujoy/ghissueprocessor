ImmutableList<E> enumUniverse() _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1022) created by **wasserman.louis** on 2012-06-02 at 07:08 PM_ --- In the JDK, EnumSet and EnumMap use internal magic to get access to a shared array of the "universe" of constants for that enum. I'd like to see something along the lines of &nbsp;&nbsp;ImmutableList&lt;E> Enums.enumConstants(Class&lt;E> clazz) that provides safe, preferably globally cached access to the enum constants for a class, either by using reflection to cheat access to clazz.getEnumConstantsShared(), or by sun.misc.SharedSecrets like EnumSet itself does, or by doing its own caching somehow. (I was attempting to write a DiscreteDomain for enum types, and copying the universe array grated slightly.)  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c1) posted by **neveue** on 2012-06-03 at 01:45 AM_

---

That would be nice. Reminds me of issue 777.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c2) posted by **kevinb@google.com** on 2012-06-04 at 03:28 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Addition`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c3) posted by **j...@nwsnet.de** on 2012-06-05 at 03:23 PM_

---

Sounds nice. Can't it return `ImmutableSortedSet`&nbsp;instead? The enum constants should be both ordered _and_ unique (and immutable).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c4) posted by **wasserman.louis** on 2012-06-05 at 03:49 PM_

---

It could, I guess? I feel like we've had mixed feelings about the Comparable instance of enums before, which doesn't always make contextual sense, and I feel like the get(int) operation is a relatively high priority. We already sort of have immutable enum sets, but they're really just thin wrappers around EnumSet.

The use case I specifically have in mind is, for example, reimplementing EnumMultiset as the ImmutableList&lt;E> universe and an int[] instead of an EnumMap to wrapper objects.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c5) posted by **kevinb@google.com** on 2012-06-19 at 05:47 PM_

---

Set seems correct over List, to me. SortedSet I don't care for, since we've observed that 90% of enums have no meaningful order, and the other 10% seem to have it but don't even bother to specify it.

Is this issue request much different from Sets.immutableEnumSet(EnumSet.allOf(MyEnum.class))?

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c6) posted by **wasserman.louis** on 2012-06-19 at 11:55 PM_

---

Well, for the use cases I had in mind, _indexed_ random access was specifically necessary, meaning there had to be an accessible List. (Perhaps that's better implemented on a package-private basis, though, since I was basically thinking of it as a helper for enum-based collections.)

That's not equivalent to Sets.immutableEnumSet or the like.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c7) posted by **wasserman.louis** on 2012-06-22 at 12:45 PM_

---

Alternative specific example: writing a DiscreteDomain for enum types, and implementing next(E). You need indexed access on the enum type to get the next value in the canonical ordering.

Basically -- indexed access is the one thing that you can't already get from EnumSet.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c8) posted by **SeanPFloyd** on 2012-06-22 at 02:50 PM_

---

Regarding DiscreteDomain.next():

can't that be solved by this existing (admittedly ugly) value.getDeclaringClass().getEnumConstants()[value.ordinal()+1];
}
.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c9) posted by **wasserman.louis** on 2012-06-22 at 02:58 PM_

---

Only at the expense of computing a fresh copy of the array each time. getEnumConstants() can't exactly let you modify the backing copy, so you're paying cost linear in the size of the entire enum every time.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c10) posted by **SeanPFloyd** on 2012-06-22 at 03:12 PM_

---

Ah, I see. I forgot that non-empty arrays can't be made immutable.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c11) posted by **kevinb@google.com** on 2012-06-22 at 06:15 PM_

---

So you store that array in a field, that's all.

This doesn't seem like a general-public use case, but a library-implementors' use case.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c12) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c13) posted by **wasserman.louis** on 2012-06-22 at 06:18 PM_

---

Well, if you're extremely into performance, you can use some tricky hacks to reflectively access the class's internal shared copy of the array -- the array that getEnumConstants() is copied from.

I was hoping that we could factor out those details and provide an API with a safe interface to that array -- thus, an ImmutableList.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c14) posted by **kevinb@google.com** on 2012-06-22 at 06:55 PM_

---

I still don't understand. If I want the enum universe as an ImmutableList, I can already get it with ImmutableList.copyOf(MyEnum.values()). Are you really trying to optimize _that_, or worry about caching that _for_ the user (who can cache it herself)?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c15) posted by **wasserman.louis** on 2012-06-22 at 06:59 PM_

---

Well. Technically, the Class object (you know, MyEnum.class) already does the caching itself internally. I want to provide the magic to safely access _that_ cached value.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c16) posted by **cpovirk@google.com** on 2012-06-25 at 07:17 PM_

---

Pulling in comments from http://codereview.appspot.com/6333058/ ...

"""
Essentially, the goal is to provide a safe, transparent, package-private way to
mirror the tricks used by EnumSet and EnumMap to get access to the shared array
of enum constants held in their Class object. On supported platforms -- that
is, wherever we can get the JavaLangAccess interface to the Java internals --
Enums.getUniverse runs in constant time, independent of the number of enum
values.

Ideally, this can be used to reimplement enum collection magic without going
through EnumSet or EnumMap. For example, EnumMultiset could be implemented with
the ImmutableList&lt;E> and an int[], avoiding the per-element Count object
allocations.
"""

Of course, we could provide the alternative EnumMultiset already, but we'd need to call ImmutableList.copyOf(EnumSet.allOf(MyEnum.class)). That would almost certainly be slower for large enums, but it's not obvious to me that it would be slower, let alone significantly slower, for typical enums. If it is faster, would it pay off over the few dozen users EnumMultiset has? (As always, we can assume that it "should" have more callers than it does, but it's also possible that some of the existing users "should" use ImmutableMultiset.) If any of those users use GWT, will the code-size increase offset any gains?

The fact that EnumSet and EnumMap use this trick is encouraging, but I'm still uncertain about whether it's worth the effort, even with a CL ready.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c17) posted by **wasserman.louis** on 2012-06-27 at 01:43 PM_

---

(Another specific application: a DiscreteDomain for enum types. The "next" function requires being able to look up the element with the next-higher ordinal, which can't be done with EnumSet alone.)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c18) posted by **kevinb@google.com** on 2012-06-28 at 10:22 PM_

---

That was your original example. I still say that DiscreteDomain can just stash values() in a field and voila.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1022#c19) posted by **wasserman.louis** on 2012-07-02 at 09:16 PM_

---

Bleah, I'm really dumb.

---

**Status:** `Duplicate`
**Merged Into:** #777
 
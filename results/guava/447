HashMultimap should choose stingier sizing defaults _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=447) created by **jim.andreou** on 2010-10-11 at 12:06 AM_ --- HashMultimap creates value collections quite pessimistically by default - expecting 8 elements per key. This may increase too much the cost for sparse multimaps. While this setting can be defined by the user, we all know that the vast majority of people use the defaults without thinking. :) I'd suggest starting from a lower point (few doublings of small arrays to reach the current default size wherever needed should be cheap enough), even just 4 instead of 8 (personally I would start even lower). Or one could be more aggressive and create special representations for tiny sets (as for example ImmutableSet), but that would be a lot of work indeed. For illustration, I copy from: http://code.google.com/p/memory-measurer/wiki/ElementCostInDataStructures HashMultimap (Worst) -- Objects = 5.00 Refs = 29.00 Primitives = {float=1.0, int=5.0} HashMultimap (Best) -- Objects = 1.00 Refs = 5.00 Primitives = {int=1.0} HashSet -- Objects = 1.00 Refs = 5.00 Primitives = {int=1.0} This is the (average) cost per user-entry in a default HashMultimap. In the worst case, each entry has a unique key, while in the best case, all entries have the same key (the latter has obviously the exact same cost-per-entry as adding something to a HashSet). In a realistic scenario, the actual cost falls somewhere in between, depending on the sparsity of the multimap. In the worst case, each entry adds 5 objects (_not_ counting the key/value objects of the user) in the multimap, 29 references (most of them are due to the big HashSet allocated), and several primitives, that should be roughly 5*2 + 29 + 6 = 45 words, which seems quite a lot.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c1) posted by **kevinb@google.com** on 2011-07-18 at 03:44 PM_

---

Perhaps with the first value for a key k it should merely put an ImmutableSet in its map? Only at the second go up to a HashSet with table size 4 or 8?

Any new thoughts on this, "Jim"? :-)

---

**Status:** `Accepted`
**Labels:** `Milestone-Release10`, `Type-Performance`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c2) posted by **kevinb@google.com** on 2011-07-18 at 04:15 PM_

---

_(No comment entered for this change.)_
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c3) posted by **jim.andreou** on 2011-07-18 at 06:30 PM_

---

No new thoughts, and more importantly, no data. Wouldn't it be cool if somehow we got such data? E.g. "median size of values", "what % of them are just size==1". I don't know how :(. I think someone recently mentioned internally that initial values were excessively big. 

What would it take to decide for a lower default size?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c4) posted by **fry@google.com** on 2011-07-28 at 05:50 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Milestone-Release10`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c5) posted by **fry@google.com** on 2011-12-10 at 03:51 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c6) posted by **wasserman.louis** on 2012-02-12 at 05:42 PM_

---

We're in a position to get more data on this, no? Now that we have Dimitris' memory-measurement tool?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c7) posted by **jim.andreou** on 2012-02-13 at 09:35 AM_

---

Not really, nobody would want this tool to run in production code :)

But we could make a reasonable decision without additional data. E.g., see what's the relationship between [Linked]HashMap and TreeMap:
HashMap :: Bytes = 32.24
LinkedHashMap :: Bytes = 40.24
TreeMap :: Bytes = 32.00

Wouldn't it be reasonable to expect a similar relationship between [Linked]HashMultimap and TreeMultimap? 

HashMultimap_Worst :: Bytes = 192.24
HashMultimap_Best :: Bytes = 32.24

LinkedHashMultimap_Worst :: Bytes = 328.48
LinkedHashMultimap_Best :: Bytes = 96.48

TreeMultimap_Worst :: Bytes = 128.00
TreeMultimap_Best :: Bytes = 32.00

We could just say that "the average case" (most probably untrue) would be this:

HashMultimap :: Bytes = 112
LinkedHashMultimap :: Bytes = 212
TreeMultimap :: Bytes = 80

And that these numbers should follow about the same relation as in the first case, e.g. HashMultimap should be 80 bytes, and LinkedHashMultimap shoulb be 106 bytes (+20% from the tree case).

By "most probably untrue" I mean that this underestimates the true cost if it happens that most of multimap instances are sparse (thus the cost would approach the worst case, instead of the best). Which is very likely, given that multimaps is the type to do basic, casual graph operations, and that most real world graphs are sparse.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c8) posted by **wasserman.louis** on 2012-02-13 at 04:08 PM_

---

What I meant to suggest was "pick some real-world usage of HashMultimap, attempt to reproduce it in a testing environment, measure the used memory with the current defaults and with changed defaults, see the extent of the improvement."
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c9) posted by **jim.andreou** on 2012-02-13 at 05:35 PM_

---

Too much work just to get a single data point...
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c10) posted by **jim.andreou** on 2012-02-13 at 11:42 PM_

---

Did another test, just using the minimum defaults for HashMultimap and LinkedHashMultimap (i.e. constructing them with create(1,1)). The worst case for HashMultimap is 136 bytes (compared to 192), whereas the worst case for LinkedHashMu

HashMultimap_Worst :: Bytes = 136 (instead of 192)
LinkedHashMultimap_Worst :: Bytes = 280 (instead of 328)

Which would bring the """average""" case down to:
HashMultimap_Avg :: Bytes = 84 (instead of 112), almost as good as TreeMultimap's 80
LinkedHashMultimap_Avg :: Bytes = 188 (instead of 212)

So, HashMultimap could definitely see some adjusting, though LinkedHashMultimap seems like a lost cause. 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c11) posted by **kevinb@google.com** on 2012-03-16 at 09:40 PM_

---

For 12.0 why don't we do nothing but change the default expectedValuesPerKey from 8 to 2?

---

**Labels:** `Milestone-Release12`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c12) posted by **kevinb@google.com** on 2012-03-22 at 05:47 PM_

---

We're changing HashMultimap from 8 to 2, and ArrayListMultimap from 10 to 3.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c13) posted by **cpovirk@google.com** on 2012-03-23 at 07:26 PM_

---

_(No comment entered for this change.)_

---

**Owner:** kevinb9n
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c14) posted by **cpovirk@google.com** on 2012-03-23 at 07:26 PM_

---

_(No comment entered for this change.)_

---

**Owner:** kevinb@google.com
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=447#c15) posted by **kevinb@google.com** on 2012-03-26 at 09:40 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Fixed`
 
Change Retention Policy of @GwtCompatible to RUNTIME. _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1091) created by **chetankumar** on 2012-07-31 at 02:32 PM_ --- I have this ANT typedef that builds fileset of class files based on annotations present. To check if a class is annotated with "GwtCompatible" I use class.isAnnotationPresent(GwtCompatible.class). That didn't work (because GwtCompatible has CLASS retention policy) To work around I now use my own variant of @﻿GwtCompatible that has RetentionPolicy of Runtime. As an enhancement, can RetentionPolicy of @﻿GwtCompatible be changed to RUNTIME ?  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1091#c1) posted by **kurt.kluever** on 2012-08-07 at 05:26 PM_

---

Can you clarify why you are building a fileset of class files?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1091#c2) posted by **daniel.holmes.duke** on 2012-08-07 at 06:26 PM_

---

The fileset is of source files, but the ant AnnotationSelector we build uses the class file to detect the annotation. That seemed simpler than having to parse the source for it.

The is the basic pattern of the selector we had implemented
http://grokbase.com/t/ant/user/08apgzmbw6/aw-building-a-fileset-using-annotations
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1091#c3) posted by **kevinb@google.com** on 2012-08-07 at 08:27 PM_

---

Also posted here http://stackoverflow.com/questions/11657549/can-retention-policy-of-gwtcompatible-be-changed-to-runtime

I still want to see a proper description of what the OP is _really ultimately trying to accomplish_ and see if the SO community really doesn't have a better way to do it, first, before assuming we have to change this. 

---

**Status:** `WontFix`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1091#c4) posted by **daniel.holmes.duke** on 2012-08-07 at 08:59 PM_

---

I have a module that contains both GWT safe and non GWT safe classes. I need to create a jar file that contains only the GWT safe java files from the module to be provided to the eventual UI module that owns the full GWT compile process. That is the requirement I'm trying to satisfy.

In order to do this, it seemed like a good idea to annotate the classes with @﻿GwtCompatible. Then the question became, how during our ant build process could we recognize which classes were GwtCompatible in order to build this jar file. We found the grokbase article that defined exactly a manner to do what we wanted, that it select java files that contain classes with a specific annotation. The problem is that implementation presumes an annotation that has RetentionType of RUNTIME.
From the ant build file perspective, this is a very clear and readable approach that we would like to have and from the AnnotationSelector implementation it is a vary simple implementation given this RUNTIME availability.

Is there a particular concern with the RUNTIME availability in general. While CLASS retention states that the annotation is recorded in the class file, I don't know of a mechanism to obtain that without trying to actually interpret the byte code itself. How could I implement AnnotationSelector in a simple manner with that constraint? I'm not sure I understand the JVM level value provided by the CLASS retention type, what does use case does that really provide that SOURCE or RUNTIME do not?
 
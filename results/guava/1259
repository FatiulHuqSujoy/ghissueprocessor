Document that Ints.tryParse(String st) throws NullPointerException _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1259) created by **yurachud** on 2013-01-16 at 04:20 PM_ --- If we pass null to Ints.tryParse it throws NullPointerException. Method uses AndroidInteger.tryParse(), but the first line there is checkNotNull() method.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1259#c1) posted by **SeanPFloyd** on 2013-01-16 at 04:25 PM_

---

I'm pretty sure that's intended behavior: http://code.google.com/p/guava-libraries/wiki/UsingAndAvoidingNullExplained
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1259#c2) posted by **kak@google.com** on 2013-01-16 at 04:25 PM_

---

That's what it should do, right?
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1259#c3) posted by **kak@google.com** on 2013-01-16 at 04:26 PM_

---

_(No comment entered for this change.)_

---

**Status:** `WorkingAsIntended`
**Labels:** `Package-Primitives`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1259#c4) posted by **yurachud** on 2013-01-16 at 06:56 PM_

---

So, I was confused by JavaDocs.

"<p>Unlike {@﻿link Integer#parseInt(String)}, this method returns
&nbsp;&nbsp;&nbsp;\* {@﻿code null} instead of throwing an exception if parsing fails."

As I understood it means that method doesn't throw any exception and return value or null.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1259#c5) posted by **cpovirk@google.com** on 2013-01-16 at 07:01 PM_

---

True, a couple things intersect to make this confusing:
- Integer.parseInt threw NumberFormatException, rather than NullPointerException, for null inputs, despite the precedent elsewhere in the JDK.
- We don't document "@﻿throws NullPointerException if parameter is null" anywhere in Guava. Instead, we annotate the packages with @﻿ParametersAreNonnullByDefault.

https://google.github.io/guava/apidocs/com/google/common/primitives/package-summary.html

Given these subtleties, I think we should document this better. In fact, we had basically the same discussion about the less confusing UnsignedLongs.parseUnsignedLong, and we decided to make the documentation there clearer:

https://google.github.io/guava/apidocs/com/google/common/primitives/UnsignedLongs.html#parseUnsignedLong%28java.lang.String%29

---

**Status:** `Accepted`
**Labels:** `Type-ApiDocs`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1259#c6) posted by **kak@google.com** on 2013-01-16 at 07:14 PM_

---

In general in Guava, parameters that are not annotated with @﻿Nullable will throw a NPE if passed a null reference.

However, the wording on that particular API could be improved (since we explicitly said it doesn't throw an exception if parsing fails). Thanks for the report!
 Please take a look at pull request And do the same for Longs I'm adding `@throws NullPointerException if {@code string} is {@code null}` to all the `tryParse` methods even though it's always the case for non-`@Nullable` parameters in Guava, to hopefully reduce the chance of confusion. It's still true that the methods do not throw if _parsing_ fails, but `null` isn't even something you can try to parse.

The change should be synced out soon. 
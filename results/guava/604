Wanted in Collections2: Collection<T> filter(Collection<?>, Class<T>) _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=604) created by **jaredjacobs** on 2011-04-14 at 07:26 PM_ --- There is a corollary in Iterables. Was it omitted from Collections2 intentionally? I see the warning in the Javadoc for Collections2.filter(Collection, Predicate), but it's unclear to me why it says the predicate _must_ be consistent with equals. The referenced Javadoc for Predicate.apply specifically says that consistency with equals is _not_ an absolute requirement. I've re-read the Javadoc for Collection and nothing there indicates that it has any "equals" requirements that would justify a different policy for applying predicates than what you have for Iterable. If the omission was intentional, was it because you consider Iterables.filter(Iterable, Class) a mistake? I hope not. It's useful, and a corresponding method in Collections2 would be too. If you decide not to implement the method I'm suggesting, I hope you will at least clarify your Javadoc to make it clear why it's missing. Thanks.  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c1) posted by **kevinb9n** on 2011-04-19 at 02:22 AM_

---

If we drop the consistency requirement, we'd have to drop the optimization in FilteredCollection.contains(). So if you filter 10 million Integers down to the ~5 million that are odd, and then ask that if it contains 2, it will really have to check all those 5 million items.

The main reason there's no Collections2.filter(Collection, Class) is probably that we have so few users of the Iterables version of that method (from what we can tell), and we get a lot of complaints that it is nearly what users want but not quite (they really want something that throws an exception if the wrong type is encountered).
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c2) posted by **neveue** on 2011-04-19 at 11:20 AM_

---

Another datapoint on Iterables.filter():

We _do_ use Iterables.filter() quite often (65 use in our project), but there are indeed many cases were what we really want is something that throws an exception if the wrong type is encountered.

I once had to write the following ugly code...

public void doStuff(List&lt;Foo> foos) {
&nbsp;&nbsp;&nbsp;&nbsp;List&lt;SubFoo> subFoos = ImmutableList.copyOf(Iterables.filter(foos, SubFoo.class));
&nbsp;&nbsp;&nbsp;&nbsp;warnIfFilterGroupsHaveWrongType(foos, subFoos);
&nbsp;&nbsp;&nbsp;&nbsp;// ...
}

private void warnIfFoosHaveWrongType(List&lt;Foo> source, List&lt;SubFoo> dest) {
&nbsp;&nbsp;&nbsp;&nbsp;if (source.size() != dest.size()) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Iterable&lt;Foo> foosOfWrongType = Iterables.filter(source, not(instanceOf(SubFoo.class)));
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;logger.warn(String.format("Following Foos have the wrong type: %s", foosOfWrongType));
&nbsp;&nbsp;&nbsp;&nbsp;}
}

So +1 for some kind of "Iterables.filterAndCast()" method.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c3) posted by **jaredjacobs** on 2011-04-19 at 05:52 PM_

---

Thanks, Kevin, for the explanation. I wasn't aware of the optimization in FilteredCollection.contains(). It's well worth keeping for cases when .contains() in the underlying Collection isn't O(1).

I have a proposition that I hope you will consider. Right now, the Javadoc for Collections2.filter(Collection, Predicate) says:

"Warning: predicate must be consistent with equals, as documented at Predicate.apply. Do not provide a predicate such as Predicates.instanceOf(ArrayList.class), which is inconsistent with equals."

What if you were to provide Collections2.filter(Collection unfiltered, Class type) and say this in its Javadoc:

"Warning: Predicates.instanceOf(type) must be consistent with equals, as documented at Predicate.apply. Do not provide a type such as ArrayList.class."

This would bring Collections2 up to feature parity with Iterables, regarding filtering.

If people want something that narrows the Collection type and throws an exception if the wrong type is encountered, by all means, add it too! What's the harm? I've just filed a new issue for that. Let's put any further discussion of it there:
https://github.com/google/guava/issues/609

Thanks for your consideration.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c4) posted by **jaredjacobs** on 2011-04-21 at 04:31 PM_

---

You might be thinking: "Why aren't you happy just using the existing Collections2.filter with Predicates.instanceof(Class)?"

Don't forget the cast and type warning. Here's the MarshallableManagedSlice.class);
.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c5) posted by **kevinb@google.com** on 2011-07-13 at 07:34 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c6) posted by **fry@google.com** on 2011-12-10 at 04:05 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c7) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c8) posted by **kevinb@google.com** on 2012-05-30 at 07:43 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Addition`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=604#c9) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 There's been no activity on this for four years, and we're not especially inclined to add more functional-ish features at this time. The two main workarounds will have to do
1. Use Iterables.filter(Iterable, Class) and copy
2. Use Collections2.filter(Collection, Predicate) and cast
 
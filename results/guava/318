BufferedIterator _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=318) created by **medotin** on 2010-01-28 at 06:52 PM_ --- As I mean it, a BufferedIterator as an Iterator where the next N elements are eagerly fetched on a background thread. The Guava interface might be: Iterators.buffer(Iterator toBuffer, int bufferSize) or Iterators.buffer(Iterator toBuffer, int bufferSize, ExecutorService executorService) See here for an implementation: http://stackoverflow.com/questions/2149244/bufferediterator-implementation  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c1) posted by **fry@google.com** on 2011-01-26 at 10:39 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Accepted`
**Labels:** `Type-Enhancement`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c2) posted by **kevinb@google.com** on 2011-07-13 at 06:18 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Triaged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c3) posted by **cpovirk@google.com** on 2011-07-13 at 08:40 PM_

---

_(No comment entered for this change.)_
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c4) posted by **fry@google.com** on 2011-12-10 at 03:43 PM_

---

_(No comment entered for this change.)_

---

**Labels:** `Package-Collect`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c5) posted by **brianfromoregon** on 2011-12-11 at 03:21 PM_

---

You might already have this ready. And if you don't, Kevin's g+ points about the fruitlessness of providing an implementation surely apply :)

Still, I implemented this yesterday and you might have interest in comparing notes. I sure do.
- I catch RuntimeException from the source iterator and pass through the buffer to be thrown eventually. I let Error percolate up to user Thread's UncaughtExceptionHandler.
- If internal thread is interrupted, it clears the buffer and signals end of data. If consuming thread is interrupted, the internal thread is interrupted.
- Internal thread started on first use, or manually by user to start buffering before then. It can be destoyed manually.

I stuck mine here: http://code.google.com/p/brianfromoregon/source/browse/trunk/buffering/src/main/java/BufferedIterator.java
With a test here: http://code.google.com/p/brianfromoregon/source/browse/trunk/buffering/src/test/java/BufferedIteratorTest.java
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c6) posted by **jim.andreou** on 2011-12-11 at 06:35 PM_

---

I've toyed with similar things. This in particular seems a little less clear to what the point is. What I've done, for peers asking for this, is constructs like 'take an iterator, apply concurrently a transformation (potentially I/O, thus there should be some potential concurrency), and expose the results as another iterator'. Thus, single producer, single consumer (otherwise iterators don't cut it), with a 'doParallel' transformation in the middle (and the resulting iterator is inan arbitrary order, of course). By the way, accepting an Executor is much preferable to starting a thread.

The interesting thing is how an iterator translates to a blocking queue with an end_of_stream marker, and that queue translates to an iterator which doesn't need the marker since it has hasNext().

Perhaps this is more useful if one exposed the queue, instead of wraping it as an iterator - this allows multiple consumers downstream. And an 'end of stream' can be avoided by using some sort of counter (like the very nice, internal, IncrementableCountDownLatch), and expose a thread-safe 'producerIsDone()', perhaps in a BlockingQueue subtype.

Just ideas for now.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c7) posted by **brianfromoregon** on 2011-12-11 at 10:40 PM_

---

I've found the doParallel construct useful too, I'm using a half baked impl over here http://code.google.com/p/photomosaic/source/browse/trunk/photomosaic/src/main/java/net/bcharris/photomosaic/ThreadedIteratorProcessor.java It's useful standing on its own and a BufferedIterator would be too, and it might be nice to layer them. My current use case is streaming results of serial data store queries, the point being prefetching results to avoid waiting on network I/O for each query. 

Other markers in the queue are exception and null_element, among them end_of_stream might be the easiest to fit to an exposed BlockingQueue. 
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c8) posted by **jim.andreou** on 2011-12-11 at 11:36 PM_

---

I didn't really pay attention to this issue (#﻿318), since the issue explicitly describes a background thread, but still, even for one callback, Executor still makes sense. 

Btw, regarding exceptions, a nice, decoupled way to tackle the issue is to just use Future&lt;V> instead of V. E.g., if the user wants to deal with asynchronous exceptions, he would pass an Iterator&lt;Future&lt;V>> instead. And if you had something like I said, a BlockingQueue of "processed tasks", this could be represented by Future&lt;V>/ListenableFuture&lt;V> - thus no need for something like an "exception" marker as well.

(No time to delve into details right now, sorry)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c9) posted by **fry@google.com** on 2012-02-16 at 07:17 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Acknowledged`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c10) posted by **kevinb@google.com** on 2012-05-30 at 07:43 PM_

---

_(No comment entered for this change.)_

---

**Labels:** -`Type-Enhancement`, `Type-Addition`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c11) posted by **kevinb@google.com** on 2012-06-22 at 06:16 PM_

---

_(No comment entered for this change.)_

---

**Status:** `Research`
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=318#c12) posted by **brianfromoregon** on 2012-07-10 at 10:41 PM_

---

@﻿Jim regarding our discussion earlier about parallel processing an iterator look at this awesome interface Doug put up last week (and how perfect is the name!!) http://gee.cs.oswego.edu/dl/jsr166/dist/jsr166edocs/jsr166e/ConcurrentHashMapV8.Spliterator.html
 We feel that the JDK's extensive collection of Queues are the correct abstraction to use for these use cases.
 
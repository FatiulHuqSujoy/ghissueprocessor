Feature request: clone serializable _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=696) created by **uklance** on 2011-08-24 at 04:16 PM_ --- Any object that implements java.io.Serializable can be cloned as follows: public static Serializable clone(final Serializable obj) throws Exception { &nbsp;&nbsp;&nbsp;ByteArrayOutputStream out = new ByteArrayOutputStream(); &nbsp;&nbsp;&nbsp;ObjectOutputStream oout = new ObjectOutputStream(out); &nbsp;&nbsp;&nbsp;oout.writeObject(obj); &nbsp;&nbsp;&nbsp;ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(out.toByteArray())); &nbsp;&nbsp;&nbsp;return Serializable.class.cast(in.readObject()); } It would be great if guava could have a Serializables.clone(Serializable) method or similar  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=696#c1) posted by **cgdecker** on 2011-08-24 at 06:52 PM_

---

What would you need this for? If it's for testing, guava-testlib has SerializableTester (https://github.com/google/guava/blob/master/guava-testlib/src/com/google/common/testing/SerializableTester.java) for this.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=696#c2) posted by **uklance** on 2011-08-25 at 07:59 AM_

---

I have a production use case where I want to create a bean by cloning a template bean. I will then configure additional properties on the clone and don't want to affect original template.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=696#c3) posted by **cgdecker** on 2011-08-25 at 02:49 PM_

---

It seems to me that you should create a method that creates a copy of your bean and call that directly. Abusing the serialization mechanism just to get a copy of an object doesn't seem like a good idea.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=696#c4) posted by **uklance** on 2011-08-25 at 03:48 PM_

---

The bean in question is in a separate module that is not maintained by my team. We could obviously create a clone() method ourselves but as fields get added to the bean in future, we may miss some properties. To avoid this we could create a clone by reflectively iterating the bean's properties but this is slow.

I have seen the serialize/deserialize used a few times and don't see it as an abuse of serialization. With this technique, I can guarantee that I have a deep clone of an object. Other techniques rely on developers to correctly implement a deep clone.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=696#c5) posted by **uklance** on 2011-08-25 at 04:18 PM_

---

I have attached a test case highlighting the problems with Cloneable
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=696#c6) posted by **kevinb@google.com** on 2011-08-29 at 06:12 PM_

---

Sorry, I don't feel this is a good fit for Guava.

---

**Status:** `WorkingAsIntended`
**Labels:** `Type-Enhancement`
 I had an example use case for this function today: clone org.springframework.security.core.context.SecurityContext and set it as current while executing a background task in a thread pool. This data structure is Serializable, so it can be saved into a servlet session, the concrete classes implementing these interfaces could be different and the data structure could be mutable, so I would like to make a copy, and doing so via serialization looks like the simplest approach.
 The simple approach in the original comment won't always work, for example in an environment with multiple class loaders, such as an app server. I showed how to make it work [here](https://weblogs.java.net/blog/emcmanus/archive/2007/04/cloning_java_ob.html). But it's very expensive and I'm inclined to agree with @kevinb9n that Guava is not a good place for it. Here's what I wrote there:

> I should caution that this technique is not to be used lightly. First of all, serialization is hugely expensive. It could easily be a hundred times more expensive than the clone() method. Secondly, not all objects are serializable. Thirdly, making a class serializable is tricky and not all classes can be relied on to get it right. (You can assume that system classes are correct, though.)
 
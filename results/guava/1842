Guarantee RangeMap iteration order _[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1842) created by **cpovirk@google.com** on 2014-09-04 at 02:34 PM_ --- http://stackoverflow.com/q/25646191/28465  _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1842#c1) posted by **cpovirk@google.com** on 2014-09-04 at 02:35 PM_

---

For comparison, Range_Set_ says:

"The iterators returned by its Iterable#iterator method return the ranges in increasing order of lower bound (equivalently, of upper bound)."
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1842#c2) posted by **cgdecker@google.com** on 2014-09-04 at 03:25 PM_

---

Would it make sense for RangeMap.asMapOfRanges() to return SortedMap&lt;Range&lt;K>, V>? Similarly for RangeSet.asRanges().
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1842#c3) posted by **cpovirk@google.com** on 2014-09-04 at 05:50 PM_

---

Internally, comments on CL 23059491 and CL 23565836 touch on this, as does &lt;http://go/more-on-range-collection-sorted-views>. I feel like there may have been other discussions, too.

In short, there is some weirdness around defining a Comparator that does the right thing. I think that we're required to use RANGE_LEX_ORDERING: We must compare the beginning of the ranges first to get order right, and we must compare the end of the ranges also so that contains() works. But is that Comparator prone to misuse with other methods?

(Part of the discussion was that few users are likely to need the SortedMap/SortedSet features. The one counterexample we'd come across so far was a need for last*() methods.)
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1842#c4) posted by **lowasser@google.com** on 2014-09-04 at 05:53 PM_

---

I'm comfortable guaranteeing the iteration order, but not for returning a SortedMap. As Chris alluded to, I see the reasoning as similar to why SortedMap.entrySet() doesn't return a SortedSet.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1842#c5) posted by **cpovirk@google.com** on 2014-09-04 at 06:05 PM_

---

Oh, right. I forgot that the Map case is especially bad -- definitely bad news for any comparator we might define, since it has to be a Comparator&lt;Entry<...>>, and the value isn't necessarily Comparable.

RangeSet.asRanges() might be less bad than that, since both endpoints of the Range object are Comparable, but it's still potentially error-prone when used with any kind of element-relative method.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1842#c6) posted by **cgdecker@google.com** on 2014-09-04 at 06:06 PM_

---

Hmm, yeah... I was just thinking in terms of the ranges that are in the RangeSet/Map, which are non-overlapping and as such easy to represent as Sorted*. But I can see that it would be confusing with sub/head/tailSet/Map etc.
 _[Original comment](https://code.google.com/p/guava-libraries/issues/detail?id=1842#c7) posted by **lowasser@google.com** on 2014-09-24 at 06:54 PM_

---

Fixed internally; change will be mirrored out soon.

---

**Status:** `Fixed`
 I apologize if I'm badly confused, but Map doesn't appear to define a "iterator()" method as is suggested by the RangeMap docs currently (explicitly in `asMapOfRanges().iterator().remove()` in the subRangeMap doc).

Is iteration in `asMapOfRanges().entrySet().iterator()` guaranteed to be ascending? Or is there some other way to iterate?
 
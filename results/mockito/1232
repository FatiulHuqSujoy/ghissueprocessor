[JUnit5] MockitoSession#initMocks should support multiple test instances  In order to support nested tests in JUnit5 (see #1221) MockitoSession#initMocks should allow to pass more than one test instance or allow multiple calls on initMocks with different test instances. In other words/code: . or .  Related discussion from #1221 
----

@TimvdLippe https://github.com/mockito/mockito/issues/445#issuecomment-357918936
> Regarding #1232: another option would be to not support nesting of test classes. I think we actively have to consider this option, given our public API was apparently coupled to the JUnit4 implementation and we can not ship breaking changes per our policy.


----

@marcphilipp https://github.com/mockito/mockito/issues/445#issuecomment-358106441
> Well, it's @Incubating so you could still change it, right? 😉
> 
> Besides, I think neither of the two proposals would break compatibility, would they? But then again, we should discuss this over at #1232. 🙂

----

@TimvdLippe https://github.com/mockito/mockito/issues/445#issuecomment-358286590
> Hm, looking at it again, it seems that the breaking change is actually in an internal API, namely TestFinishedEvent, which is created in MockitoSession. As such, I think we should be okay actually. I will do a proper investigation this weekend (hopefully, else next week) and update #1221. I hope you are okay with that @ChristianSchwarz ? Once that is done, I can more definitively say the impact on our API and whether we are risking a breaking change. I have good hopes now we might actually dodge that.


----

@marcphilipp https://github.com/mockito/mockito/issues/445#issuecomment-358407562
> `TestFinishedEvent.getTestClassInstance()` and `TestFinishedEvent.getTestMethodName()` are currently only used (twice) like this:
> 
> .
>
> Currently, `DefaultMockitoSession `always uses null for `TestFinishedEvent.getTestMethodName() `which strikes me as odd. I think it would be better if TestFinishedEvent only had two methods: `getFailure()` and getTestName(). Then, MockitoSessionBuilder could get a testName(String) builder method and DefaultMockitoSessionBuilder could pass it to DefaultMockitoSession and so on.
> 
> Alternatively, `MockitoSessionBuilder `could get a `testMethodName()` builder method and pass that on.
> 
> Moreover, I think `MockitoSessionBuilder `should allow to configure a `MockitoLogger`. This way, frameworks like JUnit could pass in a custom implementation. For JUnit Jupiter, a `MockitoLogger `that publishes report entries instead of writing to stdout (which causes problems will parallel execution) comes to mind.
> 
> Thoughts? @TimvdLippe 

> I will do a proper investigation this weekend (hopefully, else next week) and update #1221. I hope you are okay with that @ChristianSchwarz ?

Of course,go ahead!
 @marcphilipp 
>Moreover, I think `MockitoSessionBuilder` should allow to configure a `MockitoLogger`.
>
>Thoughts?

Make sense, should be a new issue.
 Hey guys! What's the next step for closing this issue getting us closer to supporting JUnit5?

@ChristianSchwarz - can you update the ticket and clarify the public API change? Do you suggest adding new "initMock()" method? Do you suggest vararg, or consecutive invocation, or both? I would suggest to go for the simplest compatible API change - keeping the existing method, just making it vararg. Will you be able to work on this?

@marcphilipp - thank you for suggestions! For internal refactorings, we are pretty open - do what you think is good for the codebase. For public API changes/additions, we push back by default and we make sure that the use case is very compelling.

Thanks!!! I would say we can change it to a vararg. This ticket basically addresses https://github.com/mockito/mockito/pull/1221/files#diff-6d0d68d3f3e6a783328fbe723a822668R36 which is unfortunate to do right now. If we change it to vararg, we can transform the `testClassInstances` List to an array and make it work :tada: 
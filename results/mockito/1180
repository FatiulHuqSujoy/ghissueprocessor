Stubbing superclass' method using Mockito inline propagate to the real method call Hi my java env is . OS Version `MacOS Sierra 10.12.6` Mockito version: `mockito-inline:2.9.0` For this simple snippet: . then stubbing the size() method . stringList.size() will always return 0, and size() is acually called in ArrayList instread of the stub the above code works perfectly with mockito-core, for a workaround, StringList can be written in . then size() can be stubbed. I only need mockito inline to mock a single final class in one test case, but mockito-inline took over my entire module's unit tesst, I'm wondering if there's a way to choose mockito-core or inline for each test? and I'm not sure if this is related, if stub in this way ` when(stringList.size()).thenReturn(11);` Mockito reports > org.mockito.exceptions.misusing.MissingMethodInvocationException: > when() requires an argument which has to be 'a method call on a mock'. > For example: > when(mock.getArticles()).thenReturn(articles); > > Also, this error might show up because: > 1. you stub either of: final/private/equals()/hashCode() methods. > Those methods *cannot* be stubbed/verified. > Mocking methods declared on non-public parent classes is not supported. > 2. inside when() you don't call method on mock but on some other object.  If I am not mistaken we explicitly disallow mocking of such Data classes.
If you need to stub a list, then just create a list with the value you
expect. For more information see
https://github.com/mockito/mockito/wiki/How-to-write-good-tests#dont-mock-value-objects

On Wed, 30 Aug 2017, 07:49 Hu Yao <notifications@github.com> wrote:

> Hi my java env is
>
> java version "1.8.0_74"
> Java(TM) SE Runtime Environment (build 1.8.0_74-b02)
> Java HotSpot(TM) 64-Bit Server VM (build 25.74-b02, mixed mode)
>
> OS Version
> MacOS Sierra 10.12.6
>
> Mockito version:
> mockito-inline:2.9.0
>
> For this simple snippet:
>
> public class StringList extends ArrayList<String>{
>
> }
>
> then stubbing the size() method
>
> StringList stringList = mock(StringList.class);
> doReturn(10).when(stringList).size();
>
>
> stringList.size() will always return 0, and size() is acually called in
> ArrayList instread of the stub
> the above code works perfectly with mockito-core, for a workaround,
> StringList can be written in
>
> public class StringList extends ArrayList<String>{
> @Override
> public int size(){
> return 0;
> }
> }
>
> then size() can be stubbed.
> I only need mockito inline to mock a single final class in one test case,
> but mockito-inline took over my entire module's unit tesst, I'm wondering
> if there's a way to choose mockito-core or inline for each test?
>
> and I'm not sure if this is related, if stub in this way
> when(stringList.size()).thenReturn(11);
> Mockito reports
>
> org.mockito.exceptions.misusing.MissingMethodInvocationException:
> when() requires an argument which has to be 'a method call on a mock'.
> For example:
> when(mock.getArticles()).thenReturn(articles);
>
> Also, this error might show up because:
>
> 1. you stub either of: final/private/equals()/hashCode() methods.
> Those methods *cannot* be stubbed/verified.
> Mocking methods declared on non-public parent classes is not supported.
> 2. inside when() you don't call method on mock but on some other
> object.
>
> —
> You are receiving this because you are subscribed to this thread.
> Reply to this email directly, view it on GitHub
> <https://github.com/mockito/mockito/issues/1180>, or mute the thread
> <https://github.com/notifications/unsubscribe-auth/AFrDb0hKkSOylN62U_1YZ8p5KRQWf8-vks5sdXZagaJpZM4PHgWw>
> .
>
 @TimvdLippe sorry I didn't intend to mock List, I happened to use the List as a simple example, this case happens when mocking superclass' method which is not overridden in child class. This seems to be a regression in version 2.9.0 with `mock-maker-inline`. In our code base we hit the issue with some code that mocks Elasticsearch 2.3.5's [`DeleteRequestBuilder`](https://github.com/elastic/elasticsearch/blob/v2.3.5/core/src/main/java/org/elasticsearch/action/delete/DeleteRequestBuilder.java). Simplified mock(DeleteRequestBuilder.class);
assertNull(b.get());
.
This passes with Mockito 2.8.47, but causes an NPE with Mockito 2.9.0, because the "real" method is org.elasticsearch.action.ActionRequestBuilder.get(ActionRequestBuilder.java:64)
.

CC @raphw since the issue only happens when `mock-maker-inline` is enabled. I found the error which lies in the new bridge method sensitive override detection mechanism. I added a test case and hope everything works again in the new version. I fixed this in #1179, please reopen the issue if it is not resolved. Hi @raphw, I just built and installed 1c61f1a91a7d5d8367b3dbddb6270afdd96c8f6a using `./gradlew publishToMavenLocal`. The above issue still reproduces, unfortunately. Darn, I reproduced the issue as a test case but I assume it was insufficient to capture the full problem.

Thanks for double-checking, I will hav another look.

PS: You did build the release 2.x branch? Alternatively, the build should be released by now. Ah, I just double-checked and it seems like the actual fix got lost in git rebase. I will fix this tonight! New try! Thanks! Just built `v2.9.2` locally and I can confirm the issue is fixed. :white_check_mark: 

Dare I ask: since this release resolves a regression, do you plan to publish it on Maven Central? Should be published automatically. @szczepiq? Nah, by default they only go to Bintray. (I can configure our Nexus to proxy to Bintray, but I prefer to keep the number of non-Maven Central hosted dependencies as small as possible. Also, by publishing on Maven Central a larger audience will find the fix using the `versions-maven-plugin`.) >Should be published automatically. @szczepiq?

We currently don't publish automatically to Maven Central based on the feedback from community that there are too many versions of Mockito around :) We might get back to publishing every version as we did before.

I plan to ship 2.10 soon for https://github.com/mockito/mockito/pull/1121 so it will land in Maven Central! 
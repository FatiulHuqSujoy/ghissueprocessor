Coverage report is broken since Gradle 5 We upgraded to Gradle 5 in #1683 to fix our Java 11 build. However, this has now broken our coverage computation because of a breaking change in Gradle 4.10: https://github.com/gradle/gradle/pull/6419 There is no guidance from the Gradle team on how to migrate to a proper configuration. I have spend a couple of hours today to try to fix it, with no result. The TLDR is that Gradle now throws away the `test.exec` after executing the tests in a subproject. Thus, it will only have the coverage from the last subproject it ran. I have looked online and there are other projects that do something similar (https://github.com/grpc/grpc-java/blob/master/all/build.gradle, https://github.com/groovy/groovy-core/blob/master/gradle/jacoco/jacoco.gradle) but none of the approaches worked for us.  Per https://github.com/gradle/gradle/pull/6419#issuecomment-484178277

> Why can't you use the JacocoMerge task and have one execution file per test task?

I suppose we can use this task, but I am not sure how to configure it properly. So you wan't a coverage report which calculates the aggregated test-coverage over all subprojects? That is correct. This was working as intended, but was broken in Gradle 5: https://github.com/gradle/gradle/pull/6419 The appropriate solution is `JacocoMerge`, but I was not able to make that work. I'm looking into it. JacocoMerge alone won't help. You'd also need an additional JacocoReport task.

I can write a grade plugin for this an provide a PR which uses this plugin to solve the problem. > I'm looking into it.

Awesome thanks!

> You'd also need an additional JacocoReport task.

We currently already have a custom task in https://github.com/mockito/mockito/blob/0ad4efbeedac050f416c48f22c9a561e31f3cb05/gradle/root/coverage.gradle, so I don't think a plugin is strictly necessary. You don't need nor merge, nor additional report, nor custom plugin:

https://github.com/gradle/gradle/pull/6419 states

> separate tasks now cannot use the same coverage file

which is about test tasks, however task `JacocoReport` can use data from multiple files.

Was about to open PR after seen https://twitter.com/TimvdLippe/status/1123244630491979776 , however @marcphilipp was faster 😆 :thumbsup: 
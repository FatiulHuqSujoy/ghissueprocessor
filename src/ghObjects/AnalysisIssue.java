package ghObjects;

public class AnalysisIssue {
    private int commentNum, actorNum, assignmentNum, labelNum;
    private int mentionedByNum, referencedByNum, subscribedByNum;
    private int meanCommentLen, cleanedBodyLen;
    private int creatorIssueNum, creatorClosedIssueNum, creatorCommitNum;
    private int actorCommitNum, actorCommitterNum;
    private int projectIssueNum, projectClosedIssueNum, projectCommitNum;
    private int projectIssueTNum, projectClosedIssueTNum, projectCommitTNum;

    public int getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(int commentNum) {
        this.commentNum = commentNum;
    }

    public int getActorNum() {
        return actorNum;
    }

    public void setActorNum(int actorNum) {
        this.actorNum = actorNum;
    }

    public int getAssignmentNum() {
        return assignmentNum;
    }

    public void setAssignmentNum(int assignmentNum) {
        this.assignmentNum = assignmentNum;
    }

    public int getLabelNum() {
        return labelNum;
    }

    public void setLabelNum(int labelNum) {
        this.labelNum = labelNum;
    }

    public int getMentionedByNum() {
        return mentionedByNum;
    }

    public void setMentionedByNum(int mentionedByNum) {
        this.mentionedByNum = mentionedByNum;
    }

    public int getReferencedByNum() {
        return referencedByNum;
    }

    public void setReferencedByNum(int referencedByNum) {
        this.referencedByNum = referencedByNum;
    }

    public int getSubscribedByNum() {
        return subscribedByNum;
    }

    public void setSubscribedByNum(int subscribedByNum) {
        this.subscribedByNum = subscribedByNum;
    }

    public int getMeanCommentLen() {
        return meanCommentLen;
    }

    public void setMeanCommentLen(int meanCommentLen) {
        this.meanCommentLen = meanCommentLen;
    }

    public int getCleanedBodyLen() {
        return cleanedBodyLen;
    }

    public void setCleanedBodyLen(int cleanedBodyLen) {
        this.cleanedBodyLen = cleanedBodyLen;
    }

    public int getCreatorIssueNum() {
        return creatorIssueNum;
    }

    public void setCreatorIssueNum(int creatorIssueNum) {
        this.creatorIssueNum = creatorIssueNum;
    }

    public int getCreatorClosedIssueNum() {
        return creatorClosedIssueNum;
    }

    public void setCreatorClosedIssueNum(int creatorClosedIssueNum) {
        this.creatorClosedIssueNum = creatorClosedIssueNum;
    }

    public int getCreatorCommitNum() {
        return creatorCommitNum;
    }

    public void setCreatorCommitNum(int creatorCommitNum) {
        this.creatorCommitNum = creatorCommitNum;
    }

    public int getActorCommitNum() {
        return actorCommitNum;
    }

    public void setActorCommitNum(int actorCommitNum) {
        this.actorCommitNum = actorCommitNum;
    }

    public int getActorCommitterNum() {
        return actorCommitterNum;
    }

    public void setActorCommitterNum(int actorCommitterNum) {
        this.actorCommitterNum = actorCommitterNum;
    }

    public int getProjectIssueNum() {
        return projectIssueNum;
    }

    public void setProjectIssueNum(int projectIssueNum) {
        this.projectIssueNum = projectIssueNum;
    }

    public int getProjectClosedIssueNum() {
        return projectClosedIssueNum;
    }

    public void setProjectClosedIssueNum(int projectClosedIssueNum) {
        this.projectClosedIssueNum = projectClosedIssueNum;
    }

    public int getProjectCommitNum() {
        return projectCommitNum;
    }

    public void setProjectCommitNum(int projectCommitNum) {
        this.projectCommitNum = projectCommitNum;
    }

    public int getProjectIssueTNum() {
        return projectIssueTNum;
    }

    public void setProjectIssueTNum(int projectIssueTNum) {
        this.projectIssueTNum = projectIssueTNum;
    }

    public int getProjectClosedIssueTNum() {
        return projectClosedIssueTNum;
    }

    public void setProjectClosedIssueTNum(int projectClosedIssueTNum) {
        this.projectClosedIssueTNum = projectClosedIssueTNum;
    }

    public int getProjectCommitTNum() {
        return projectCommitTNum;
    }

    public void setProjectCommitTNum(int projectCommitTNum) {
        this.projectCommitTNum = projectCommitTNum;
    }
}

package ghObjects;

import java.util.*;

public class GHIssue {
    private String id, title, body, json;
    private List<String> commentIds, labels;

    private List<GHComment> comments;

    private Date createdDate, closedDate;
    private Set<String> assignees;
    private Set<String> referencedIssIds, referencedCommitShas;
    private String creator;

    private AnalysisIssue analysisIssue;

    public GHIssue() {
        comments = new ArrayList<>();
        commentIds = new ArrayList<>();
        labels = new ArrayList<>();

        assignees = new HashSet<>();
        referencedIssIds = new HashSet<>();
        referencedCommitShas = new HashSet<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<GHComment> getComments() {
        return comments;
    }

    public void setComments(List<GHComment> comments) {
        this.comments = comments;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

    public String getJson() {
        return json.replace("\n", " ");
    }

    public void setJson(String json) {
        this.json = json;
    }

    public List<String> getCommentIds() {
        return commentIds;
    }

    public void setCommentIds(List<String> commentIds) {
        this.commentIds = commentIds;
    }

    public Set<String> getAssignees() {
        return assignees;
    }

    public void setAssignees(Set<String> assignees) {
        this.assignees = assignees;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        return "GHIssue{" + "\'\n" +
                "id='" + id + "\'\n" +
                "title='" + title + "\'\n" +
                "body='" + body + "\'\n" +
                "createdDate=" + createdDate + "\n" +
                "mergedDate=" + closedDate + "\n" +
                "comments=\n" + comments + "\n\n" +
                "cmnt=\n" + commentIds + "\n\n" +
                "labels=\n" + labels + "\n\n" +
                '}' + "\n";
    }

    public void sortComments() {
        if(!comments.isEmpty())
            Collections.sort(comments);
    }

    public void addCommitReference(String sha) {
        referencedCommitShas.add(sha);
    }

    public void addIssueReference(String id) {
        referencedIssIds.add(id);
    }

    public void addAssignee(String assignee){
        assignees.add(assignee);
    }

    public String getText() {
        return title + " " + body + " " + inlineComments();
    }

    private String inlineComments() {
        String commentText = "";

        for(GHComment comment : comments){
            commentText += comment.getBody();
        }

        return commentText;
    }
}

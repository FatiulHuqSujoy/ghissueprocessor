package ghObjects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GHComment implements Comparable<GHComment> {
    protected String id;
    protected String body;
    protected String date;
    protected String json;

    @Override
    public String toString() {
        return "GHComment{" + "\n" +
                "id='" + id + "\'\n" +
                "body='" + body + "\'\n" +
                "date=" + date + "\n" +
                '}' + "\n";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public Date getCreateDate() {
        try {
            return new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    @Override
    public int compareTo(GHComment o) {
        return getCreateDate().compareTo(o.getCreateDate());
    }
}

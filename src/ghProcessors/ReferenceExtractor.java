package ghProcessors;

import ghObjects.GHComment;
import ghObjects.GHCommit;
import ghObjects.GHIssue;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ReferenceExtractor {
    private Map<String, GHIssue> idToIssMap;

    public ReferenceExtractor(Map<String, GHIssue> idToIssMap) {
        this.idToIssMap = idToIssMap;
    }

    public void extractRefsFromIssues(List<GHIssue> issues){
        for(GHIssue issue : issues){
            Set<String> ids = extractRefs(issue.getTitle());
            ids.addAll(extractRefs(issue.getBody()));
            for(GHComment comment : issue.getComments()){
                ids.addAll(extractRefs(comment.getBody()));
            }

            for(String id : ids){
                GHIssue refIssue = idToIssMap.get(id);
                refIssue.addIssueReference(issue.getId());
            }
        }
    }

    public void extractRefsFromCommits(List<GHCommit> commits){
        for(GHCommit commit : commits){
            Set<String> ids = extractRefs(commit.getText());

            for(String id : ids){
                GHIssue issue = idToIssMap.get(id);
                issue.addCommitReference(commit.getSha());
            }
        }
    }


    private Set<String> extractRefs(String text){
        Set<String> ids = new HashSet<>();

        for(String word : text.split("\\S+")){
            word = word.trim();

            if(isRef(word)){
                ids.add(word
                        .replace("#", "")
                        .replace("GH-", ""));
            }
        }

        return ids;
    }

    private boolean isRef(String word) {
        return word.startsWith("#") || word.startsWith("GH-");
    }
}

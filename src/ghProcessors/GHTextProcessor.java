package ghProcessors;

import ghObjects.GHComment;
import ghObjects.GHIssue;

public class GHTextProcessor {

    public void cleanIssueText(GHIssue issue){
        String body = cleanCodeFromText(issue.getBody());
        issue.setBody(body);
        for(GHComment comment : issue.getComments()){
            comment.setBody(cleanCodeFromText(comment.getBody()));
        }
    }

    private String cleanCodeFromText(String rawText){
        Boolean code = false;
        String newText = "";

        for(String word : rawText.split(" ")){
            if(word.contains("```")){
                code = !code;
            }

            if(!code)
                newText += word + " ";
        }

        return newText
                .replace("```", ".");
//                .replace("\n", ". ");
    }
}

package ghProcessors;

import ghObjects.*;
import utils.FileProcessor;
import utils.JSONConverter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IssueCollector {
    private final String SOURCE_DIR_NAME = "../GHIssueData/";
    private final String ISS_DIR_NAME = "/issues/";
    private final String COMMENT_DIR_NAME = "/comments/";

    private String projectName;
    private List<GHIssue> issues;

    public IssueCollector(String projectName){
        this.projectName = projectName;
        issues = new ArrayList<>();
    }

    public void populateGHGraph() throws FileNotFoundException {
        issues = fetchIssuesFromFiles();

        for(GHIssue issue : issues){
            issue.setComments(fetchComments(issue.getCommentIds(), issue));
            issue.sortComments();

//            System.out.println(issue);
        }
    }

    private List<GHIssue> fetchIssuesFromFiles() throws FileNotFoundException {
        List<GHIssue> issues = new ArrayList<>();
        String pathname = SOURCE_DIR_NAME + projectName + ISS_DIR_NAME;
        File folder = new File(pathname);

        int count = 0;
        for (final File issFile : folder.listFiles()) {
            String issJson = FileProcessor.getFileContent(issFile);
            issJson = mendIssueString(issJson).replace("\n", " ");

//            issJson = processJsonString(issJson);
//            FindingsWriter.printResultInTextFile(issJson, pathname+issFile.getName());

            GHIssue issue = (GHIssue) JSONConverter.jsonFromString(issJson, GHIssue.class);

            issues.add(issue);
            count++;
//            if(count==10)
//                break;
        }

        return issues;
    }

    private String mendIssueString(String issJson){
        issJson = mendQuotations(issJson,"json", "commentIds");
        return issJson;
    }

    private String mendQuotations(String issJson, String start, String end) {
        String startTxt = "\"" + start + "\":\"";
        String endTxt = "\", \"" + end + "\"";

        String problem = issJson.substring(issJson.indexOf(startTxt) + startTxt.length(),
                        issJson.indexOf(endTxt));
        String fix = problem
                .replace("\\\"", "\"")
                .replace("\"", "\\\"");

//        if(fix.length()!=problem.length())
//            System.out.println(issJson + "\n\t" + problem + "\n\t" + fix);
        return issJson.replace(problem, fix);
    }

    private List<GHComment> fetchComments(List<String> commentIds, GHIssue parentPR) throws FileNotFoundException {
        List<GHComment> comments = new ArrayList<>();

        for(String commentId : commentIds){
            GHComment comment = fetchCommentFromFile(commentId);
//            System.out.println("\tCOMMENT: " + commentId + " " + comment.getCreateDate());
            comments.add(comment);
        }

        return comments;
    }

    private GHComment fetchCommentFromFile(String commentId) throws FileNotFoundException {
        String pathname = SOURCE_DIR_NAME + projectName + COMMENT_DIR_NAME + commentId + ".txt";
        File file = new File(pathname);
        String commentJson = FileProcessor.getFileContent(file);
        GHComment comment = (GHComment) JSONConverter.jsonFromString(commentJson, GHComment.class);
        return comment;
    }

    public List<GHIssue> getIssues(){
        return issues;
    }

    public Map<String, GHIssue> getIdIssueMap(){
        Map<String, GHIssue> idIssueMap = new HashMap<>();
        for(GHIssue issue : issues){
            idIssueMap.put(issue.getId(), issue);
        }
        return idIssueMap;
    }
}

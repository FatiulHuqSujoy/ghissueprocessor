package ghProcessors;

import ghObjects.GHCommit;
import ghObjects.GHIssue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskProcessor {
    private String projectPath, projectName;
    private List<GHIssue> issues;
    private Map<String, GHIssue> idIssueMap;
    private Map<String, GHCommit> shaCommitMap;
    private Map<String, List<String>> userCreateMap, userAssignMap;

    public TaskProcessor(String project){
        projectPath = project;
        projectName = project.split("/")[1];
        idIssueMap = new HashMap<>();
        shaCommitMap = new HashMap<>();
        userAssignMap = new HashMap<>();
        userCreateMap = new HashMap<>();
    }

    public void run() throws IOException {
        System.out.println("<Task Run Initiated>");

        collectIssues();

//        extractIssueContent();

        processIssueText();

        printIssues();
    }

    private void printIssues() throws IOException {
        IssueTextWriter issueTextWriter = new IssueTextWriter(projectName);

        for (GHIssue issue : idIssueMap.values()){
            System.out.println(issue.getId());
        }

        for (GHIssue issue : idIssueMap.values()){
            System.out.println("----X--------X------" + "\n" + issue.getId() + ":> ");
            System.out.println(issue.getText() + "\n");
            issueTextWriter.writeIssueTexts(issue);
        }
    }

    private void processIssueText() {
        GHTextProcessor ghTextProcessor = new GHTextProcessor();
        for (GHIssue issue : idIssueMap.values()){
            ghTextProcessor.cleanIssueText(issue);
        }
    }

    private void extractIssueContent() {
        IssueContentExtractor issueContentExtractor = new IssueContentExtractor();
        for (GHIssue issue : idIssueMap.values()){
            issueContentExtractor.extractJsonMap(issue);
        }
    }

    private void collectIssues() throws IOException {
        IssueCollector issueCollector = new IssueCollector(projectName);
        issueCollector.populateGHGraph();
        issues = issueCollector.getIssues();
        idIssueMap = issueCollector.getIdIssueMap();
    }
}

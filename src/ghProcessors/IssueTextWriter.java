package ghProcessors;

import ghObjects.GHIssue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class IssueTextWriter {
    private static final String FOLDER_NAME = "results/";
    private String projectName;

    public IssueTextWriter(String projectName) {
        this.projectName = projectName;
        new File(FOLDER_NAME + projectName).mkdirs();
    }

    public void writeIssueTexts(GHIssue issue) throws IOException {
        File file = new File(FOLDER_NAME + projectName + "/" + issue.getId());

        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(issue.getText());

        writer.close();
    }
}

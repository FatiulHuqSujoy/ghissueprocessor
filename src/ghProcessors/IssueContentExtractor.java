package ghProcessors;

import com.fasterxml.jackson.databind.ObjectMapper;
import ghObjects.GHIssue;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class IssueContentExtractor {
    public void extractIssueContent(List<GHIssue> issues){
        for(GHIssue issue : issues){
            System.out.println("\n" + issue.getId());
            extractJsonMap(issue);
        }
    }

    public void extractJsonMap(GHIssue issue){
        String jsonStr = removeText(issue.getJson());

        ObjectMapper objectMapper = new ObjectMapper();
        try {

            // convert JSON string to Map
            Map<String, Object> map = objectMapper.readValue(jsonStr, Map.class);

            // it works
            //Map<String, String> map = objectMapper.readValue(json, new TypeReference<Map<String, String>>() {});

            Map<String,String> userMap = (Map<String, String>) map.get("user");
            issue.setCreator(userMap.get("login"));

            for(Map<String, String> assigneeMap : (ArrayList<Map>)map.get("assignees")){
                issue.addAssignee(assigneeMap.get("login"));
            }
            issue.setCreatedDate(getIssueDateFromString((String) map.get("created_at")));
            issue.setClosedDate(getIssueDateFromString((String) map.get("closed_at")));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String removeText(String jsonStr) {
        jsonStr = cleanText(jsonStr, "title", "user");
        jsonStr = cleanText(jsonStr, "body", "closed_by");
        return jsonStr;
    }

    private String cleanText(String json, final String start, final String end) {
        String startTxt = "\"" + start + "\":\"";
        String endTxt = "\",\"" + end + "\"";

        String problem = json.substring(json.indexOf(startTxt) + startTxt.length(),
                json.indexOf(endTxt));
        String clean = json.replace(problem, "");

        return clean;
    }

    private Date getIssueDateFromString(String dateStr){
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .parse(dateStr
                    .replace("T", " ")
                    .replace("Z", ""));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }
}

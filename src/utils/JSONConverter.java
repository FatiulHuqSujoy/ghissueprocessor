package utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class JSONConverter {
    public static String jsonResult(Object object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static Object jsonFromString(String jsonString, Class<? extends Object> type){
        Gson gson = new Gson();
        Object json = null;
        try {
            json = gson.fromJson(jsonString, type);
        } catch (JsonSyntaxException e) {
            System.out.println(jsonString);
            e.printStackTrace();
            System.exit(1);
        }
        return json;
    }
}

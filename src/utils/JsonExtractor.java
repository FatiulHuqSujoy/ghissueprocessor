package utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonExtractor {
    public static String getDateFromReview(String json) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();
        String dateStr = jsonObject.getAsJsonPrimitive("created_at").getAsString();
        return dateStr;
    }

    public static String getBodyFromPullRequestJson(String json) {
        String body = "";
//        try{
//            Gson gson = new Gson();
//            JsonReader jsonReader = new JsonReader(new StringReader(json));
//            jsonReader.setLenient(true);
//            JsonObject jsonObject = gson.fromJson(jsonReader, JsonObject.class);
//            body = jsonObject.getAsJsonPrimitive("body").getAsString();
//        } catch (JsonSyntaxException e){
//            System.out.println(">>   " + json);
//            e.printStackTrace();
//        }

        try{
            body = json
                    .substring(json.indexOf("\"body\":"), json.indexOf(",\"created_at\""))
                    .replace("\"body\":\"", "")
                    .replace("\",\"created_at\"", "");
        } catch (StringIndexOutOfBoundsException e){
            System.out.println(">>  " + json);
            e.printStackTrace();
        }


        return body;
    }
}

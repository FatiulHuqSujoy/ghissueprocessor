 package main;

import ghProcessors.ProjectLister;
import ghProcessors.TaskProcessor;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> projects = ProjectLister.listProjects();

        for(String project : projects){
            TaskProcessor taskProcessor = new TaskProcessor(project);
            try {
                taskProcessor.run();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
